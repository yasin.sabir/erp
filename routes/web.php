<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/','ClientController@show_client_login')->name('client.login');
Route::get('/client/login','ClientController@show_client_login')->name('client.login');


//Authentication
//Route::get('/', 'Auth\LoginController@login');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'ClientController@index')->name('clients');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

});


//Only Admin
Route::group(['middleware' => ['role:admin']], function () {

    //User
    Route::get('/users', 'UsersController@index')->name('users.list');
    Route::get('/user/add', 'UsersController@add')->name('user.add');
    Route::post('/user/create', 'UsersController@create')->name('user.create');
    Route::post('/user/{id}/delete', 'UsersController@delete')->name('user.delete');
    Route::get('/user/{id}/edit', 'UsersController@edit')->name('user.edit');
    Route::put('/user/{id}/update', 'UsersController@update')->name('user.update');
    Route::get('/user/{id}/editPassword', 'UsersController@editPassword')->name('user.editPassword');
    Route::put('/user/{id}/updatePassword', 'UsersController@updatePassword')->name('user.updatePassword');

    //Privileges
    Route::get('/privileges', 'PrivilegesController@createPrivileges');
    Route::get('/assign_privileges', 'PrivilegesController@assignPrivileges');
    Route::get('/order/list','OrderController@get_all_orders')->name('order.list');
});

//Admin & Distributor
Route::group(['middleware' => ['role:admin|distributor']], function () {

    //Account
    Route::get('/account', 'AccountController@index')->name('account');
    Route::put('/account/{id}/update', 'AccountController@update')->name('account.update');
    Route::get('/order/list','OrderController@get_all_orders')->name('order.list');

    //--------
    Route::get('/order/list/balanceDelivery' , 'OrderController@get_all_balance_orders')->name('order.list.balanceDelivery');
    Route::get('/order/list/completedDelivery' , 'OrderController@get_all_completed_orders')->name('order.list.completedDelivery');
    Route::get('/order/list/Distributor' , 'OrderController@get_all_distributor_orders')->name('order.list.distributor');
    Route::get('/order/list/Client' , 'OrderController@get_all_clients_order')->name('order.list.clients');
    //--------

    Route::get('/order/detail/{id}','OrderController@get_order')->name('order.detail');
    Route::get('/order/getColor/{id}','OrderController@getColor')->name('order.getColor');
    Route::match(['get', 'post'], 'laravel-send-custom-email', 'EmailController@customEmail');

    //reports
    Route::get('report/stock','ReportController@get_stock')->name('report.stock');
    Route::post('report/stockId','ReportController@get_stock_id')->name('report.getStockId');
    Route::get('report/saleStock','ReportController@get_saleStock')->name('report.saleStock');
    Route::post('report/saleStockcity','ReportController@get_sale_stock')->name('report.getsaleStock');
    Route::get('report/productLegder','ReportController@get_productLedger')->name('report.productLegder');
    Route::get('report/getColorCode/{id}','ReportController@getColorCode')->name('report.getColorCode');
    Route::post('report/productLedgerReport','ReportController@get_productLedgerReport')->name('report.productLedgerReport');
    Route::get('report/deliveryChallan','ReportController@get_deliveryChallen')->name('report.deliveryChallan');
    //--------
    Route::get('report/CreatedeliveryChallan/{id}','ReportController@createView_deliveryChallan')->name('report.createDeliveryChallanView');

    Route::get('report/saleStock/Distributors','ReportController@saleStock_Distributors_View')->name('sales.report.distributorsView');
    Route::post('report/saleStock/Distributors/Records','ReportController@get_saleStock_Distributors_records')->name('sales.report.distributorsRecords');

    Route::get('report/saleStock/Cities','ReportController@saleStock_Cities_View')->name('sales.report.citiesView');
    Route::post('report/saleStock/Cities/Records','ReportController@get_saleStock_Cities_records')->name('sales.report.citiesRecords');

    Route::get('report/saleStock/Colors','ReportController@saleStock_Colors_View')->name('sales.report.colorsView');
    Route::post('report/saleStock/Colors/Records','ReportController@get_saleStock_Colors_records')->name('sales.report.colorsRecords');

    Route::get('report/saleStock/Dates','ReportController@saleStock_Dates_View')->name('sales.report.datesView');
    Route::post('report/saleStock/Dates/Records','ReportController@get_saleStock_Dates_records')->name('sales.report.datesRecords');
    //--------

    Route::post('report/deliveryChallan','ReportController@create_deliveryChallan')->name('report.createDeliveryChallan');
    Route::get('report/viewChallan','ReportController@get_Challans')->name('report.getChallans');
    Route::get('report/viewChallan/{id}','ReportController@get_Challan_detail')->name('report.getChallanDetail');
    Route::get('report/deliveryChallanReport','ReportController@get_delivery_Challan_report')->name('get_delivery_Challan_report');
    Route::get('report/cdeliveryChallanReport','ReportController@get_delivery_Challan_report')->name('get_delivery_Challan_report');

    Route::post('report/getClientOrder','ReportController@getClientOrder')->name('report.getClientOrder');
    Route::post('report/deliveryChallanReport','ReportController@get_Challan_report_detail')->name('report.get_delivery_Challan_report');
    Route::get('report/orderReport','ReportController@orderReport')->name('get_order_report');



    //Category
    Route::get('/category/add', 'CategoryController@add')->name('category.add');
    Route::post('/category/create', 'CategoryController@create')->name('category.create');
    Route::get('/category/{id}/edit', 'CategoryController@edit')->name('category.edit');
    Route::put('/category/{id}/update', 'CategoryController@update')->name('category.update');
//    Route::delete('/category/{id}/delete', 'CategoryController@delete')->name('category.delete');
    Route::post('/category/{id}/delete', 'CategoryController@delete')->name('category.delete');

//    Color
    Route::get('/color/add', 'ColorController@add')->name('color.add');
    Route::post('/color/create', 'ColorController@create')->name('color.create');
    Route::get('/color/{id}/edit', 'ColorController@edit')->name('color.edit');
    Route::put('/color/{id}/update', 'ColorController@update')->name('color.update');
//    Route::delete('/color/{id}/delete', 'ColorController@delete')->name('color.delete');
    Route::post('/color/{id}/delete', 'ColorController@delete')->name('color.delete');


    //Item Types
    Route::get('/item_type/add', 'ItemTypeController@add')->name('item.add');
    Route::post('/item_type/create', 'ItemTypeController@create')->name('item.create');
//    Route::delete('/item_type/{id}/delete', 'ItemTypeController@delete')->name('item.delete');
    Route::post('/item_type/{id}/delete', 'ItemTypeController@delete')->name('item.delete');

    Route::get('/item_type/{id}/edit', 'ItemTypeController@edit')->name('item.edit');
    Route::put('/item_type/{id}/update', 'ItemTypeController@update')->name('item.update');

    //Products
    Route::get('/product/add', 'ProductController@add')->name('product.add');
    Route::post('/product/create', 'ProductController@create')->name('product.create');
//    Route::delete('/product/{id}/delete', 'ProductController@delete')->name('product.delete');
    Route::post('/product/{id}/delete', 'ProductController@delete')->name('product.delete');

    Route::get('/product/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('/product/{id}/update', 'ProductController@update')->name('product.update');

    //Purchases
    Route::get('/purchases/all', 'PurchaseController@index')->name('purchase.all');
    Route::get('/purchases/add', 'PurchaseController@add')->name('purchase.add');
    Route::post('/purchases/create','PurchaseController@create')->name('purchase.create');
    Route::get('/purchases/view/{id}', 'PurchaseController@view')->name('purchase.view');
//    Route::get('/purchases/delete/{id}', 'PurchaseController@delete')->name('purchase.delete');
    Route::post('/purchases/delete/{id}', 'PurchaseController@delete')->name('purchase.delete');



});
Route::group(['middleware' => ['role:distributor|client']], function () {
    //order

    Route::get('/clients', 'ClientController@index')->name('clients');
    Route::get('/cart', 'CartController@cart')->name('cart');
    Route::get('/cart/remove/{id}', 'CartController@removeFromCart')->name('cart.remove');
    Route::post('/cart/update', 'CartController@updateCart')->name('cart.update');
    Route::get('/client/product/{id}', 'ClientController@productdetails')->name('productdetails');
    Route::get('/order/add','OrderController@add')->name('order.add');
    Route::post('/order/create','OrderController@create')->name('order.create');
    Route::post('cart/add','CartController@addToCart')->name('cart.add');
});

Route::group(['middleware' => ['role:client']], function () {
    //Clients

    Route::get('/client/order/report/{status}','ReportController@get_userOrderReport')->name('client.orders');
    Route::get('/client/order/report/detail/{id}','ReportController@get_userOrderReportDetail')->name('client.order.details');
    Route::get('/client/order/challan/detail/{id}','ReportController@get_userChallanDetail')->name('client.order.ChallanDetails');

});

//Admin, Distributor & Warehouse Manager
Route::group(['middleware' => ['role:warehouse_manager|distributor|admin']], function () {

    //Account
    Route::get('/account', 'AccountController@index')->name('account');

    //Category
    Route::get('/categories', 'CategoryController@index')->name('category.list');

    //Colors
    Route::get('/colors', 'ColorController@index')->name('color.list');

    //Item Types
    Route::get('/item_types', 'ItemTypeController@index')->name('items.list');

    //Products
    Route::get('/products', 'ProductController@index')->name('products.list');

    //Challan
    Route::get('report/viewChallan','ReportController@get_Challans')->name('report.getChallans');
    Route::get('report/viewChallan/{id}','ReportController@get_Challan_detail')->name('report.getChallanDetail');
    Route::get('report/deliveryChallan','ReportController@get_deliveryChallen')->name('report.deliveryChallan');
    Route::post('report/deliveryChallan','ReportController@create_deliveryChallan')->name('report.createDeliveryChallan');
    Route::get('report/CreatedeliveryChallan/{id}','ReportController@createView_deliveryChallan')->name('report.createDeliveryChallanView');
    //Route::get('report/CreatedeliveryChallan/{id}','ReportController@createView_deliveryChallan')->name('clients.add_client');



    Route::get('/order/list','OrderController@get_all_orders')->name('order.list');
    Route::put('/order/edit/{id}','OrderController@update')->name('order.edit');
});

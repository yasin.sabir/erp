{{--<!doctype html>--}}
{{--<html lang="en">--}}
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ url('assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('assets/libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    {{--<link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">--}}
    {{--<link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">--}}
    <link rel="stylesheet"
          href="{{ url('assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    {{--<link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">--}}
    <link rel="stylesheet" href="{{ url('assets/vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/b-1.6.0/b-print-1.6.0/r-2.2.3/datatables.min.css"/>


    {{--    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">--}}


    <link rel="stylesheet" href="{{ url('css/multiselect/bootstrap-multiselect.css') }}" type="text/css"/>
    <title>ERP</title>
    <style>
        .navbar-brand {
            display: inline-block;
            margin-right: 1rem;
            line-height: inherit;
            white-space: nowrap;
            padding: 11px 20px;
            font-size: 30px;
            text-transform: uppercase;
            font-weight: 700;
            color: #007bff;
        }

        select.form-control:not([size]):not([multiple]) {
            height: calc(2.25rem + -2px);
        }
    </style>
</head>

{{--<body>--}}

<div class="dashboard-main-wrapper">
    <div class="dashboard-header">
        <nav class="navbar navbar-expand-lg bg-white fixed-top">
            <a class="navbar-brand">ERP</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-right-top">

                    <li class="nav-item dropdown nav-user">
                        <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><img src="{{asset('assets/images/avatar-1.jpg')}}"
                                                                           alt="" class="user-avatar-md rounded-circle"></a>
                        <div class="dropdown-menu dropdown-menu-right nav-user-dropdown"
                             aria-labelledby="navbarDropdownMenuLink2">
                            <div class="nav-user-info">
                                <h5 class="mb-0 text-white nav-user-name"
                                    style="text-transform: capitalize">{{ \Illuminate\Support\Facades\Auth::user()->name }}</h5>
                                <span class="status"></span><span class="ml-2">Available</span>
                            </div>
                            <a class="dropdown-item" href="{{ route('account') }}"><i class="fas fa-user mr-2"></i>Account</a>
                            <a class="dropdown-item" href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i
                                    class="fas fa-power-off mr-2"></i>Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="nav-left-sidebar sidebar-dark">
        <div class="menu-list">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav flex-column">
                        <li class="nav-divider">
                            Menu
                        </li>
                        {{--                        <li class="nav-item ">--}}
                        {{--                            <a class="nav-link @if(Route::current()->getName() == 'dashboard' ) active @endif" href="{{ route('dashboard') }}"><i class="fa fa-fw fa-home"></i>Dashboard <span class="badge badge-success"</span></a>--}}
                        {{--                        </li>--}}
                        @role('admin')
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'users.list' || Route::current()->getName() == 'user.add' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1"
                               aria-controls="submenu-1"><i class="fa fa-fw fa-users"></i>Users <span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-1" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'users.list' ) active @endif"
                                           href="{{ route('users.list') }}">All Users</a>
                                        <a class="nav-link @if(Route::current()->getName() == 'user.add' ) active @endif"
                                           href="{{ route('user.add') }}">Add User</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'category.list' || Route::current()->getName() == 'category.add' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2"
                               aria-controls="submenu-2"><i class="fa fa-fw fa-user-circle"></i>Categories <span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-2" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'category.list' ) active @endif"
                                           href="{{ route('category.list') }}">All Categories</a>
                                        <a class="nav-link @if(Route::current()->getName() == 'category.add' ) active @endif"
                                           href="{{ route('category.add') }}">Add Category</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'items.list' || Route::current()->getName() == 'item.add' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4"
                               aria-controls="submenu-4"><i class="fa fa-fw fa-user-circle"></i>Item Types <span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-4" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'items.list' ) active @endif"
                                           href="{{ route('items.list') }}">All Item Types</a>
                                        <a class="nav-link @if(Route::current()->getName() == 'item.add' ) active @endif"
                                           href="{{ route('item.add') }}">Add Item Type</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'products.list' || Route::current()->getName() == 'product.add' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5"
                               aria-controls="submenu-4"><i class="fa fa-fw fa-user-circle"></i>Products<span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-5" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'products.list' ) active @endif"
                                           href="{{ route('products.list') }}">All Products</a>

                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif"
                                           href="{{ route('product.add') }}">Add Product</a>

                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'color.list' || Route::current()->getName() == 'color.add' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3"
                               aria-controls="submenu-3"><i class="fa fa-fw fa-user-circle"></i>Colors <span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-3" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'color.list' ) active @endif"
                                           href="{{ route('color.list') }}">All Colors</a>
                                        <a class="nav-link @if(Route::current()->getName() == 'color.add' ) active @endif"
                                           href="{{ route('color.add') }}">Add Colors</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'purchase.add' || Route::current()->getName() == 'purchase.all' || Route::current()->getName() == 'report.getChallans' || Route::current()->getName() == 'report.getChallanDetail') active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false"
                               data-target="#submenu-9" aria-controls="submenu-9"><i
                                    class="fa fa-fw fa-user-circle"></i>Purchases <span
                                    class="badge badge-success">9</span></a>
                            <div id="submenu-9" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link @if(Route::current()->getName() == 'purchase.all' ) active @endif"
                                           href="{{ route('purchase.all') }}">View Purchases</a>
                                        <a class="nav-link @if(Route::current()->getName() == 'purchase.add' ) active @endif"
                                           href="{{ route('purchase.add') }}">Add Purchases</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endrole

                        {{--                        <li class="nav-item ">--}}
                        {{--                            <a class="nav-link @if(Route::current()->getName() == 'products.list' || Route::current()->getName() == 'product.add' ) active @endif"--}}
                        {{--                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5"--}}
                        {{--                               aria-controls="submenu-4"><i class="fa fa-fw fa-user-circle"></i>Products<span--}}
                        {{--                                    class="badge badge-success">6</span></a>--}}
                        {{--                            <div id="submenu-5" class="collapse submenu" style="">--}}
                        {{--                                <ul class="nav flex-column">--}}
                        {{--                                    <li class="nav-item">--}}
                        {{--                                        <a class="nav-link @if(Route::current()->getName() == 'products.list' ) active @endif"--}}
                        {{--                                           href="{{ route('products.list') }}">All Products</a>--}}

                        {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif"--}}
                        {{--                                           href="{{ route('product.add') }}">Add Product</a>--}}

                        {{--                                    </li>--}}
                        {{--                                </ul>--}}
                        {{--                            </div>--}}
                        {{--                        </li>--}}

                        @role('admin|distributor')
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'clients') active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-10"
                               aria-controls="submenu-10"><i class="fa fa-fw fa-user-circle"></i>Orders<span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-10" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        @role('admin')
                                        <a class="nav-link @if(Route::current()->getName() == 'clients' ) active @endif"
                                           href="{{ route('order.list') }}">All Order</a>
                                        @endrole
                                        @role('distributor')
                                        <a class="nav-link" href="{{ route('order.list.distributor') }}">Your Order</a>
                                        <a class="nav-link" href="{{ route('order.list.clients')  }}">Clients Order</a>
{{--                                        <a class="nav-link" href="{{ route('order.list.balanceDelivery') }}">Show--}}
{{--                                            Balance</a>--}}
                                        <a class="nav-link" href="{{ route('clients') }}">New Order</a>
                                        @endrole
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endrole


                        @role('admin|distributor')
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'report.stock' || Route::current()->getName() == 'report.saleStock' || Route::current()->getName() == 'report.productLegder' || Route::current()->getName() == 'get_delivery_Challan_report'|| Route::current()->getName() == 'get_order_report' ) active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6"
                               aria-controls="submenu-6"><i class="fa fa-fw fa-user-circle"></i>Reports<span
                                    class="badge badge-success">6</span></a>
                            <div id="submenu-6" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    @role('admin')
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'report.stock' ) active @endif"
                                               href="{{ route('report.stock') }}">Stock Report</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                        <li class="nav-item">

{{--                                            <a class="nav-link @if(Route::current()->getName() == 'report.saleStock' ) active @endif"--}}
{{--                                               href="{{ route('report.saleStock') }}"  data-toggle="collapse" aria-expanded="false"--}}
{{--                                               data-target="#submenu-7" aria-controls="submenu-7">Sale Stock Report</a>--}}


                                            <a class="nav-link" href="#"  data-toggle="collapse" aria-expanded="false"
                                               data-target="#submenu-reports" aria-controls="submenu-reports">Sale Stock Reports</a>
                                            {{--  <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                            <div id="submenu-reports" class="collapse submenu" style="">
                                                <ul class="nav flex-column">
                                                    <li class="nav-item">

                                                        <a class="nav-link @if(Route::current()->getName() == 'sales.report.distributorsView' ) active @endif"
                                                           href="{{route('sales.report.distributorsView')}}">Distributors Report</a>

                                                        <a class="nav-link @if(Route::current()->getName() == 'sales.report.citiesView' ) active @endif"
                                                           href="{{route('sales.report.citiesView')}}">Cities Report</a>

                                                        <a class="nav-link @if(Route::current()->getName() == 'sales.report.colorsView' ) active @endif"
                                                           href="{{route('sales.report.colorsView')}}">Colors Report</a>

                                                        <a class="nav-link @if(Route::current()->getName() == 'sales.report.datesView' ) active @endif"
                                                           href="{{route('sales.report.datesView')}}">Dates Reports</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'report.productLegder' ) active @endif"
                                               href="{{ route('report.productLegder') }}">Product Ledger Report</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'get_delivery_Challan_report' ) active @endif"
                                               href="{{ route('get_delivery_Challan_report') }}">Delivery Challan Report</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'get_order_report' ) active @endif"
                                               href="{{ route('get_order_report') }}">Order Report</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                    @endrole

                                    @role('distributor')
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'order.list.balanceDelivery' ) active @endif"
                                               href="{{ route('order.list.balanceDelivery') }}">Balance Pending Delivery</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if(Route::current()->getName() == 'order.list.completedDelivery' ) active @endif"
                                               href="{{ route('order.list.completedDelivery') }}">Completed Delivery</a>
                                            {{--                                        <a class="nav-link @if(Route::current()->getName() == 'product.add' ) active @endif" href="{{ route('product.add') }}">Add Product</a>--}}
                                        </li>
                                    @endrole
                                </ul>
                            </div>
                        </li>
                        @endrole
                        <li class="nav-item ">
                            <a class="nav-link @if(Route::current()->getName() == 'report.createDeliveryChallan' || Route::current()->getName() == 'report.deliveryChallan' || Route::current()->getName() == 'report.getChallans' || Route::current()->getName() == 'report.getChallanDetail') active @endif"
                               href="#" data-toggle="collapse" aria-expanded="false"
                               data-target="#submenu-7" aria-controls="submenu-7"><i
                                    class="fa fa-fw fa-user-circle"></i>Delivery challan <span
                                    class="badge badge-success">7</span></a>
                            <div id="submenu-7" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        @role('admin|warehouse_manager')
                                        <a class="nav-link @if(Route::current()->getName() == 'report.deliveryChallan' ) active @endif"
                                           href="{{ route('report.deliveryChallan') }}">Create Challan</a>
                                        @endrole
                                        <a class="nav-link @if(Route::current()->getName() == 'report.getChallans' ) active @endif"
                                           href="{{ route('report.getChallans') }}">View Challans</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>
        </div>
    </div>



    <div class="dashboard-wrapper">
{{--    </div>--}}
{{--</div>--}}

@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Product Ledger Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Product Ledger Reports
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Product Ledger Report</h3>
                        </div>
                        <form action="{{route('report.productLedgerReport')}}" id="form" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose the Product for Report</label>
                                            </div>
                                            <div class="col-md-12">
                                                <select class="form-control" id="product" name="product" required="required" >
                                                    <option value="">Choose product</option>
                                                    @foreach($products as $product)
                                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose the Color for Product</label>
                                            </div>
                                            <div class="col-md-12">
                                                <select class="multiselect-ui form-control" id="color" name="color[]" multiple="multiple" required>
{{--                                                    <option value="">Choose color</option>--}}

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Generate Report">
                                    </div>
                                </div>
                            </div>

                            {{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $("#product").change(function () {
            var id = $(this).val();
            var url = '{{route('report.getColorCode',':id')}}';
            var url = url.replace(':id',id);
            if (id != "") {
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: url,
                    success: function (data) {
                        $('#color option').remove();
                        $.each(data, function(i, item){
                            $('#color').append('<option value='+item.id+'>'+ item.color_code +'</option>');
                        });
                        $('.multiselect-ui').multiselect("destroy");
                        $('.multiselect-ui')
                            .multiselect({
                                includeSelectAllOption: true
                            });

                    },
                    error: function () {
                        console.log(data);
                    }
                });
            }
        });
        $(document).ready(function() {
            $('.multiselect-ui')
                .multiselect({
                    includeSelectAllOption: true
                });
            $('.multiselect-ui')
                .multiselect({
                includeSelectAllOption: true
            })
                .change(function(e) {
                    // revalidate the color when it is changed
                    $('#bootstrapSelectForm').bootstrapValidator('revalidateField', 'colors');
                })
                .end()
                .bootstrapValidator({
                    excluded: ':disabled',
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        color: {
                            validators: {
                                callback: {
                                    message: 'Please choose Color for your product',
                                    callback: function(value, validator) {
                                        // Get the selected options
                                        var options = validator.getFieldElements('colors').val();
                                        return (options != null && options.length >= 2 && options.length <= 4);
                                    }
                                }
                            }
                        },
                    }
                });
        });
    </script>
@endsection

@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Stock Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Delivery Challan
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Delivery Challan</h3>
                        </div>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul style="margin: 0">
                                    @foreach($errors->all() as $err)
                                        <li>{{ $err }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (\Session::has('error'))
                            <div class="alert alert-danger">
                                {!! \Session::get('error') !!}
                            </div>
                        @endif




                        <form action="{{route('report.createDeliveryChallan')}}" id="form" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose the Order for Delivery Challan</label>
                                            </div>
                                            <div class="col-md-12">
                                                <select class="form-control" id="order" name="order" required>
                                                {{--    @foreach($orders as $order) --}}
                                                   <option value="{{ $order->id }}"  @if($order->id == $order->id) selected="selected"  @endif>{{ $order->order_no }}</option>
                                                {{--    @endforeach --}}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Vehicle No.</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" type="text" name="vehicle_no" value="{{old('vehicle_no')}}" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Driver Name</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" type="text" name="driver_name" value="{{old('driver_name')}}" required placeholder="Enter driver name...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Driver NIC</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" type="text" id="driver_nic" name="driver_nic" value="{{old('driver_nic')}}"  placeholder="xxxxx-xxxxxxx-x">
                                            </div>
                                            <div class="col-md-12">
                                                <label class="col-form-label">Driver Number</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input class="form-control" type="text" name="driver_number" value="{{old('driver_number')}}" required placeholder="Phone number">
                                            </div>
                                            <div class="col-md-12">
                                                <label class="col-form-label">Created By</label>
                                            </div>
                                            <div class="col-md-12">
                                                <select class="form-control" id="created_by" name="created_by" required>
                                                        @foreach($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                        @endforeach
                                                </select>
                                            </div>

{{--                                            <div class="col-md-12">--}}
{{--                                                <input class="form-control" type="text" name="created_by" value="" required>--}}
{{--                                            </div>--}}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Generate Report">
                                    </div>
                                </div>
                            </div>

                            {{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {

            $('#driver_nic').keydown(function (e) {
                var key = e.charCode || e.keyCode || 0 || 8 || 37 || 38 || 39 || 40 ||46 ;
                $text = $(this);

                // 8 - backspace
                // 37 - left key
                // 38 - up key
                // 39 - right key
                // 40 - down key
                // 46 - delete key

                if (key !== 8 && key !== 9) {
                    if ($text.val().length === 5) {
                        $text.val($text.val() + '-');
                    }  // 42101-2403626-7
                    if ($text.val().length === 13) {
                        $text.val($text.val() + '-');
                    }
                    if ($text.val().length === 15) {
                        return false;
                    }
                }
                //return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
            })

        });
    </script>
@endsection

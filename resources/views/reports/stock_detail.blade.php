@extends('layouts.admin')
@section('content')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Products</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Stock_report</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title center-block" style="text-align: center">ERP</h3>
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center">
                                    Stock Report <br>
                                    DATE:<?php $mytime = Carbon\Carbon::now();
                                    echo $mytime->format('Y-m-d'); ?></h3>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">

                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>STOCK CODE</th>
                                        <th>OPENING BALANCE</th>
                                        {{--                                        <th>Quantity</th>--}}
                                        <th>PURCHASES</th>
                                        <th>STOCK ON FLOOR</th>
                                        <?php $order_id = [];
                                        $i = 0;
                                        ?>
                                        @foreach($product_color as $report)

                                            @foreach($report->orders as $order)

                                                <?php $order_id[$i] = $order->id; ?>
                                                {{--                                                @dd($order)/--}}
                                                <th>Order No. {{$order->order_id}} </br>
                                                    Client name: {{$order->user_name}} </br>
                                                    Date: {{$order->created_at->format('Y-m-d')}}
                                                </th>
                                                @php($i++)
                                            @endforeach
                                        @endforeach

                                        <th style="font-size: 12px ; ">Closing Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0; ?>
                                    @foreach($product_color as $key => $report)
                                        {{--                                        {{dd($report)}}--}}
                                        <tr>
                                            <td>{{$report->code}}</td>
                                            <td>{{$report->quantity}}</td>
                                            <td>{{$report->purchase}}</td>
                                            <td>{{(int)$report->quantity + (int)$report->purchase }}</td>
                                            @for($x = 0;$x < (count($order_id));$x++)
                                                {{--                                                @dd($report)--}}
                                                @if(empty($report->orders))
                                                    <td>N/A</td>
                                                @else
                                                    @if($report->orders[$i]->id == $order_id[$x])
                                                        <td>{{$order['qty']}}</td>

                                                    @endif
                                                    <?php $i++; ?>
                                                @endif
                                            @endfor
                                            <td>{{$report->quantity_in_hand}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        // function myFunction() {
        //      window.print();
        // }
        //
        // function printData() {
        //     var divToPrint = document.getElementById("report");
        //     $(divToPrint).find('.dataTables_length').remove();
        //     $(divToPrint).find('.printBtn').remove();
        //     $(divToPrint).find('#products_filter').remove();
        //     $(divToPrint).find('#products_info').remove();
        //     $(divToPrint).find('#products_paginate').remove();
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.opener.location.reload();
        //     newWin.close();
        //     //  window.print();
        // }
        //
        // $('.printBtn').click(function (e) {
        //     // e.preventDefault();
        //     printData();
        // });


        var top_content = document.getElementById("report-title");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });

    </script>
@endsection

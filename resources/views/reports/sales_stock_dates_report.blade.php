@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Stock Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Sales Stock Reports</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Sales Stock Report</h3>
                        </div>
                        <form action="{{route('sales.report.datesRecords')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label class="col-form-label">Choose the Date for Report</label>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <select class="form-control" id="category"  name="date" placeholder="Choose Date" required>--}}
{{--                                                    @foreach($order_dates as $date)--}}

{{--                                                        <option value="{{ $date }}">{{ $date }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose Starting Date</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="date" class="form-control" name="starting_date" required="required">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose Ending Date</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="date" class="form-control" name="ending_date" required="required">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Generate Report">
                                    </div>
                                </div>
                            </div>

                            {{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

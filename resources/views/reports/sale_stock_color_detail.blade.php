@extends('layouts.admin')
@section('content')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Products</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Sale_Stock_report</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title center-block" style="text-align: center">ERP</h3>
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center"   >
                                    Sales Stock Report Color <br>
                                    Date:<?php $mytime = Carbon\Carbon::now();
                                    echo $mytime->format('Y-m-d'); ?>
                                </h3>
                            </div>

                        </div>

                        <div class="card">
                            <div class="card-body">

                                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th>Product_code</th>
                                        <th>Product_name</th>
                                        {{--                                        <th>Quantity</th>--}}
                                        <th>Opening Qty</th>
                                        <th>Net Purchase Qty</th>
                                        <th>Net sales Qty</th>
                                        <th>Pending Orders</th>
                                        <th>Closing Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($product_color as $key => $report)
                                        {{-- {{dd($report)}}--}}
                                        @foreach($report as $key => $report)
                                            {{--                                            @if($report->code == $report->color_id)--}}
                                            <tr>
                                                <td>{{$report->code}}</td>
                                                <td>{{$report->name}}</td>
                                                <td>{{$report->quantity}}</td>
                                                <td>{{$report->purchase}}</td>
                                                <td>{{$report->net_order}}</td>
                                                <td>{{$report->pending}}</td>
                                                <td>{{(int)$report->quantity_in_hand}}</td>
                                            </tr>
                                            {{--                                            @endif--}}
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        var top_content = document.getElementById("report-title");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });


    </script>
@endsection

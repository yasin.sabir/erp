@extends('layouts.admin')
@section('content')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Delivery Challan Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Delivery challan Report
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title center-block" style="text-align: center">ERP</h3>
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center">
                                    Delivery Challan Report
                                    <br>
                                    <span>
                                     From: {{$date['starting']}} &nbsp; To: {{$date['ending']}}
                                </span>
                                </h3>
                            </div>

                        </div>
                        <div class="card">
                            <div class="card-body">
                                {{--                                <button class="printBtn">Print Report</button>--}}
                                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                    <thead >
                                    <tr>
                                        <th>
                                            Challan No.
                                        </th>
                                        <th>
                                            Order No.
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Client Name
                                        </th>
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">Color Code</th>--}}
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">Driver Details</th>--}}
                                        {{--                                        <th>Quantity</th>--}}
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">Vehicle No.</th>--}}
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">Party Name</th>--}}
                                        <th>
                                            Distributor Name
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Description
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['data'] as $key => $report)
                                        {{--                                        {{dd($report)}}--}}
                                        <tr>
                                            <td>{{$report['id']}}</td>
                                            <td><a
                                                    href="{{ route('order.detail',$report['order_id']) }}">{{$report['order_no']}}</a>
                                            </td>
                                            <td>{{$report['created_at']->format('d-m-Y')}}</td>
                                            <td>{{$report['company_name']}}</td>
                                            {{--                                            <td style="border-right: 1px solid black;">{{$report['item']}}</td>--}}
                                            {{--                                            <td style="border-right: 1px solid black;">{{$report['driver_name']}} <br>--}}
                                            {{--                                                {{$report['driver_cnic']}}--}}
                                            {{--                                            </td>--}}
                                            {{--                                            <td style="border-right: 1px solid black;">{{$report['vehicle_no']}}</td>--}}
                                            {{--                                            <td style="border-right: 1px solid black;">{{$report['party_name']}}</td>--}}
                                            <td>{{$report['agent']}}</td>
                                            <td>{{$report['address']}}</td>
                                            <td><a
                                                    href="{{ route('order.detail',$report['order_id']) }}"> Order
                                                    Quantity: {{$report['order_quantity']}} </br>
                                                    Delivered Quantity: {{$report['delivered_quantity']}} </br>
                                                    Balance Quantity: {{$report['balance']}}</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    {{--                                    @foreach($data as $key => $report)--}}
                                    {{--                                        <tr>--}}
                                    {{--                                            <td style="border-right: 1px solid black;">{{$report->challan_id}}</td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;"><a href="{{ route('order.detail',$report['order_id']) }}">{{$report->order_no}}</a></td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;">{{$report->date}}</td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;">{{$report->client_name}}</td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;">{{$report->distributor_name}}</td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;">{{$report->address}}</td>--}}
                                    {{--                                            <td style="border-right: 1px solid black;"><a--}}
                                    {{--                                                    href="{{ route('order.detail',$report['order_id']) }}"> Order--}}
                                    {{--                                                    Quantity: {{$report->order_qty}} </br>--}}
                                    {{--                                                    Delivered Quantity: {{$report->delivered_qty}} </br>--}}
                                    {{--                                                    Balance Quantity: {{$report->balance_qty}}</a>--}}
                                    {{--                                            </td>--}}
                                    {{--                                        </tr>--}}
                                    {{--                                    @endforeach--}}


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // function printData() {
        //     var divToPrint = document.getElementById("report");
        //     $(divToPrint).find('.dataTables_length').remove();
        //     $(divToPrint).find('.printBtn').remove();
        //     $(divToPrint).find('#products_filter').remove();
        //     $(divToPrint).find('#products_info').remove();
        //     $(divToPrint).find('#products_paginate').remove();
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.opener.location.reload();
        //     newWin.close();
        // }
        //
        // $('.printBtn').click(function (e) {
        //     // e.preventDefault();
        //     printData();
        // });

        var top_content = document.getElementById("basicform");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });


    </script>
@endsection

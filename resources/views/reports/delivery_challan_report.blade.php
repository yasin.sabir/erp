@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Delivery Challan Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Delivery Challan Reports
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Delivery Challan Report</h3>
                        </div>
                        <form action="{{route('report.get_delivery_Challan_report')}}" id="form" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose the Client</label>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" id="client" name="client[]"
                                                        multiple="multiple" required="required">

                                                    @foreach($clients as $client)
                                                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose Starting Date</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="date" class="form-control" name="starting_date" required="required">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="col-form-label">Choose Ending Date</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="date" class="form-control" name="ending_date" required="required">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 5%">
                                        <div class="col-md-12" >
                                            <label class="col-form-label">Choose Items</label>
                                        </div>
                                        <div class="col-md-2" >
                                            <select class="form-control" id="item" name="item[]" multiple="multiple" required="required">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Generate Report">
                                    </div>
                                </div>
                            </div>

                            {{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $("#client").change(function () {
            var id = $(this).val();
            var url = '{{route('report.getClientOrder')}}';
            if (id != "") {
                var data = {};
                data['ids'] = id;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    }
                });
                $.ajax({
                    type: 'POST', //THIS NEEDS TO BE GET
                    data: data,
                    url: url,
                    success: function (data) {
                        var items =[];
                        $.each(data, function (i, item) {
                            var obj = {label:item,value:i};
                            items.push(obj);

                        });
                        $('#item').multiselect('dataprovider', items);
                    },
                    error: function () {
                        console.log(data);
                    }
                });
            }
        });
        $(document).ready(function () {
            $('#client').multiselect();
            $('#item').multiselect();
            $('#form')
                .find('[name="color"]')
                .selectpicker()
                .change(function (e) {
                    // revalidate the color when it is changed
                    $('#bootstrapSelectForm').bootstrapValidator('revalidateField', 'colors');
                })
                .end()
                .bootstrapValidator({
                    excluded: ':disabled',
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        color: {
                            validators: {
                                callback: {
                                    message: 'Please choose Color for your product',
                                    callback: function (value, validator) {
                                        // Get the selected options
                                        var options = validator.getFieldElements('colors').val();
                                        return (options != null && options.length >= 2 && options.length <= 4);
                                    }
                                }
                            }
                        },
                    }
                });
        });
    </script>
@endsection

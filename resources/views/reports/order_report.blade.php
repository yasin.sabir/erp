@extends('layouts.admin')
@section('content')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Orders Wise Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">orders_wise_report</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center">
                                    Orders Wise Report
                                    <br>
                                    Date:<?php
                                    $mytime = Carbon\Carbon::now();
                                    echo $mytime->format('Y-m-d'); ?>
                                </h3>
                            </div>

                        </div>
                        <div class="card">
                            <div class="card-body">


                                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th>
                                            S.No
                                        </th>
                                        <th>
                                            PARTY NAME
                                        </th>
                                        <th>
                                            AGENT
                                        </th>
                                        <th>
                                            ORDER #no & DATE
                                        </th>
                                        <th>
                                            CODE
                                        </th>
                                        <th>
                                            ORDER QTY
                                        </th>
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">QTY DELIVERED</th>--}}
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">PENDING ORDER</th>--}}
                                        <th>
                                            BALANCE QTY
                                        </th>
                                        <th>
                                            APPROVED QTY
                                        </th>
                                        <th>
                                            DECLINED QTY
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($data as $key => $report)
                                        <?php $x = 0; ?>
                                        @foreach($report['codes'] as $order)
                                            {{--@dd($order)--}}
                                            {{--{{dd($report)}}--}}
                                            @if($x == 0)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>
                                                        {{$report['name']}}
                                                      {{--  {{ $report['id'] }}--}}
                                                    </td>
                                                    <td>{{$report['agent']}}</td>
                                                    <td><a
                                                            href="{{ route('order.detail',$report['order_id']) }}"> {{$report['order_no']}} </a></br>
                                                        DATE: {{$report['order_date']}}
                                                        <?php //$mytime = Carbon\Carbon::now();
                                                             //echo $mytime->format('Y-m-d'); ?>
                                                    </td>
                                                    <td>{{$order['color_code']}}</td>
                                                    <td>{{$order['order_quantity']}}</td>
                                                    {{--<td style="border-right: 1px solid black;">{{$order['deliver_quantity']}}</td>--}}
                                                    {{--<td style="border-right: 1px solid black;">{{$order['pending_quantity']}}</td>--}}
                                                    <td>{{$order['balance_quantity']}}</td>
                                                    <td>{{$order['approved_quantity']}}</td>
                                                    <td>{{$order['declined_quantity']}}</td>

                                                </tr>
                                            @else
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{$order['color_code']}}</td>
                                                    <td>{{$order['order_quantity']}}</td>
                                                    {{--<td>{{$order['deliver_quantity']}}</td>--}}
                                                    {{--<td>{{$order['pending_quantity']}}</td>--}}
                                                    <td>{{$order['balance_quantity']}}</td>
                                                    <td>{{$order['approved_quantity']}}</td>
                                                    <td>{{$order['declined_quantity']}}</td>
                                                </tr>
                                            @endif

                                            <?php $x++; ?>
                                        @endforeach
                                            @if($report['total_order'] > 0)
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><strong>TOTAL</strong></td>
                                                    <td>{{$report['total_order']}}</td>
{{--                                                <td>{{$report['total_delivered']}}</td>--}}
{{--                                                <td>{{$report['total_pending']}}</td>--}}
                                                    <td>{{$report['total_balance']}}</td>
                                                    <td>{{$report['total_approved']}}</td>
                                                    <td>{{$report['total_declined']}}</td>
                                                </tr>
                                            @endif
                                        <?php $i++; ?>
                                    @endforeach
{{--                                    @if($report['total_order'] > 0)--}}
{{--                                        <tr>--}}
{{--                                            <td></td>--}}
{{--                                            <td></td>--}}
{{--                                            <td></td>--}}
{{--                                            <td></td>--}}
{{--                                            <td><strong>TOTAL</strong></td>--}}
{{--                                            <td>{{$report['total_order_qty_new']}}</td>--}}
{{--                                            --}}{{--<td>{{$report['total_delivered']}}</td>--}}
{{--                                            --}}{{--<td>{{$report['total_pending']}}</td>--}}
{{--                                            <td>{{$report['total_balance']}}</td>--}}
{{--                                            <td>{{$report['total_approved']}}</td>--}}
{{--                                            <td>{{$report['total_declined']}}</td>--}}
{{--                                        </tr>--}}
{{--                                    @endif--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // function printData() {
        //     var divToPrint = document.getElementById("report");
        //     $(divToPrint).find('#order_report_paginate').remove();
        //     $(divToPrint).find('.printBtn').remove();
        //     $(divToPrint).find('#order_report_length').remove();
        //     $(divToPrint).find('#order_report_filter').remove();
        //     $(divToPrint).find('#order_report_info').remove();
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.opener.location.reload();
        //     newWin.close();
        // }
        //
        // $('.printBtn').click(function (e) {
        //     // e.preventDefault();
        //     printData();
        // });

        var top_content = document.getElementById("report-title");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                "bSort": false,
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });
    </script>
@endsection

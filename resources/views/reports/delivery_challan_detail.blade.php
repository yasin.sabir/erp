@extends('layouts.admin')
@section('content')
    <style>
        .row {
            margin-bottom: 10px;
        }
    </style>
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Delivery Challan</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Delivery Challan</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <div class="row">
                                <div class="col-md-10">
                                    <div id="report-title">
                                        <h3 class="section-title center-block" style="text-align: center">
                                            Delivery
                                            Challan</h3>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    {{--                                    <button class="printBtn">Print Report</button>--}}
                                </div>
                            </div>
                            <div class="report">

                                <div id="challan-detials">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Challan No.</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['challan_no']}}</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Date:</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['date']}}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Party Name</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['party_name']}}</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Order No.</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['order_no']}}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>City</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['city']}}</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Distributor Name</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['distributor']}}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Vehicle No.</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['vehicle_no']}}</p>
                                        </div>
                                        <div class="col-md-3">

                                        </div>

                                        <div class="col-md-3">

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Driver Name</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['driver_name']}}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Driver NIC</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['driver_cnic']}}</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <p style="font-size: 13px;"><strong>Driver Number</strong></p>
                                        </div>
                                        <div class="col-md-3">
                                            <p>{{$data['driver_number']}}</p>
                                        </div>
                                    </div>

                                </div>


                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Product Name
                                                </th>
                                                <th>
                                                    Product code
                                                </th>
                                                <th>
                                                    Quantity
                                                </th>
                                                <th>
                                                    Quantity Delivered
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data['products'] as $product)
                                                <tr>
                                                    <td>{{$product['product_name']}}</td>
                                                    <td>{{$product['product_code']}}</td>
                                                    <td>{{$product['quantity']}}</td>
                                                    <td>{{$product['quantity_delivered']}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <p style="font-size: 13px;"><strong>Total:</strong></p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>{{$data['total']}}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <p style="margin-left: 21px;">{{$data['total_delivered']}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // function printData() {
        //     var divToPrint = document.getElementById("report");
        //     $(divToPrint).find('.dataTables_length').remove();
        //     $(divToPrint).find('.printBtn').remove();
        //     $(divToPrint).find('#products_filter').remove();
        //     $(divToPrint).find('#products_info').remove();
        //     $(divToPrint).find('#products_paginate').remove();
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.opener.location.reload();
        //     newWin.close();
        // }
        //
        // $('.printBtn').click(function (e) {
        //     // e.preventDefault();
        //     printData();
        // });

        var top_content = document.getElementById("report-title");
        var middle_content = document.getElementById("challan-detials");

        $(document).ready(function () {
            // $('#product').DataTable({
            //     "searching": false,
            //     "bPaginate": false,
            //     // "bLengthChange": false,
            //     "bFilter": false,
            //     "bInfo": false,
            // });


            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                "bFilter": false,
                "bInfo": false,
                "searching": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone())
                                .before($(middle_content).clone());

                        }
                    }
                ]
            });






        });
    </script>
@endsection

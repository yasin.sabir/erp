@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Delivery Challan</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">delivery challan</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">All Orders</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <table id="categories" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>S#</th>
                                        <th>Date</th>
                                        <th>Order id</th>
                                        <th>Order no</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;

                                    ?>
                                    @foreach($orders as $order)
                                        <?php $i++ ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td>{{$order->created_at->format('d-m-Y')}}</td>
                                            <td>{{ $order->id }}</td>
                                            <th>
                                                <a href="{{ route('order.detail',$order->id)  }}">{{ $order->order_no }}</a>
                                            </th>
                                            @if($order->status != 'completed')
                                                <td>
                                                    <ul class="actions">
                                                        {{--  report.deliveryChallan--}}
                                                        {{--  createChallan.detail--}}
                                                        {{--  route('report.deliveryChallan', ['id' => $order->id])--}}
                                                        <li>
                                                            <a href="{{route('report.createDeliveryChallanView',$order->id)}}"><span><i
                                                                        class="fa fa-sticky-note"></i></span></a></li>

                                                    </ul>
                                                </td>
                                            @else
                                                <td> Completed </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

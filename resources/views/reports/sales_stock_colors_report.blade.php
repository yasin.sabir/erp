@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Stock Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Sales Stock Reports</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Sales Stock Report</h3>
                        </div>
                        <form action="{{route('sales.report.colorsRecords')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-12">

                                                @if(\Session::has('error'))
                                                    <div class="alert alert-danger">
                                                        {!! \Session::get('error') !!}
                                                    </div>
                                                @endif

                                                <label class="col-form-label">Choose the Color for Report</label>
                                            </div>

{{--                                            <div class="col-md-12">--}}
{{--                                                <select class="form-control" id="category"  name="product_color_id" placeholder="Choose Color" required>--}}
{{--                                                    @foreach($colors as $color)--}}
{{--                                                        <option value="{{ $color->product_color_id }}">{{ $color->code }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}


                                            <br>

                                            <div class="col-md-4">
                                                <select multiple class="multiselect-ui form-control" id="sel2" name="colors_prod_ids[]">
                                                    @foreach($colors as $color)
                                                        <option value="{{ $color->product_color_id }}">{{ $color->code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

{{--                                            <div class="col-md-12">--}}
{{--                                                <select class="multiselect-ui form-control" id="color" name="color[]" multiple="multiple" required>--}}
{{--                                                    --}}{{--                                                    <option value="">Choose color</option>--}}

{{--                                                </select>--}}
{{--                                            </div>--}}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Generate Report">
                                    </div>
                                </div>
                            </div>

                            {{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $("#product").change(function () {
            var id = $(this).val();
            var url = '{{route('report.getColorCode',':id')}}';
            var url = url.replace(':id',id);
            if (id != "") {
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: url,
                    success: function (data) {
                        $('#color option').remove();
                        $.each(data, function(i, item){
                            $('#color').append('<option value='+item.id+'>'+ item.color_code +'</option>');
                        });
                        $('.multiselect-ui').multiselect("destroy");
                        $('.multiselect-ui')
                            .multiselect({
                                includeSelectAllOption: true
                            });

                    },
                    error: function () {
                        console.log(data);
                    }
                });
            }
        });
        $(document).ready(function() {
            $('.multiselect-ui')
                .multiselect({
                    includeSelectAllOption: true
                });
            $('.multiselect-ui')
                .multiselect({
                    includeSelectAllOption: true
                })
                .change(function(e) {
                    // revalidate the color when it is changed
                    $('#bootstrapSelectForm').bootstrapValidator('revalidateField', 'colors');
                })
                .end()
                .bootstrapValidator({
                    excluded: ':disabled',
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        color: {
                            validators: {
                                callback: {
                                    message: 'Please choose Color for your product',
                                    callback: function(value, validator) {
                                        // Get the selected options
                                        var options = validator.getFieldElements('colors').val();
                                        return (options != null && options.length >= 2 && options.length <= 4);
                                    }
                                }
                            }
                        },
                    }
                });
        });
    </script>
@endsection

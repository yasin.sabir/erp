@extends('layouts.admin')
@section('content')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Products</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">reports</li>
                                        <li class="breadcrumb-item active" aria-current="page">Sale_Stock_report</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title center-block" style="text-align: center">ERP</h3>
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center">
                                    Product Ledger Report <br>
                                    <span>
                                        Date: <?php $mytime = Carbon\Carbon::now();
                                        echo $mytime->format('Y-m-d'); ?>
                                    </span>

                                </h3>
                            </div>

                        </div>
                        <div class="card">
                            <div class="card-body">

                                {{--<button class="printBtn" >Print Report</button>--}}
                                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                    <thead>
                                    <tr>
                                        {{--                                        <th style="font-size: 14px ;font-weight: bold;  border-right: 1px solid black;">Date</th>--}}
                                        <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Color Code/Size
                                        </th>
                                        <th>
                                            Initial Qty
                                        </th>
                                        <th>
                                            Received Qty
                                        </th>
                                        <th>
                                            Order Recieved
                                        </th>
                                        <th>
                                            Issued
                                        </th>
                                        <th>
                                            pending order
                                        </th>
                                        <th>
                                            Closing Balance(Stock On Floor)
                                        </th>
                                        <th>
                                            Closing Balance
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--                                    {{ dd($data['orders']) }}--}}
                                    @foreach($data['products'] as $product_key => $colors)
                                        @foreach($colors as $color_key => $report)
                                            {{--@dd($report);--}}
                                            {{-- {{dd($report)}}--}}
                                            <tr>
                                                {{-- <td style="border-right: 1px solid black;">{{$report['date']}}</td>--}}
                                                <td>{{$product_key}}</td>
                                                <td>{{$color_key}}</td>
                                                <td>{{$report['initial']}}</td>
                                                <td>{{$report['purchase']}}</td>
                                                <td>{{$report['order']}}</td>
                                                <td>{{$report['issued']}}</td>
                                                <td>{{$report['pending_order']}}</td>
                                                <td>{{$report['stock']}}</td>
                                                <td>{{$report['balanace']}}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        // function printData() {
        //     var divToPrint = document.getElementById("report");
        //     $(divToPrint).find('.dataTables_length').remove();
        //     $(divToPrint).find('.printBtn').remove();
        //     $(divToPrint).find('#products_filter').remove();
        //     $(divToPrint).find('#products_info').remove();
        //     $(divToPrint).find('#products_paginate').remove();
        //     newWin = window.open("");
        //     newWin.document.write(divToPrint.outerHTML);
        //     newWin.print();
        //     newWin.opener.location.reload();
        //     newWin.close();
        // }
        //
        // $('.printBtn').click(function (e) {
        //     // e.preventDefault();
        //     printData();
        // });


        var top_content = document.getElementById("report-title");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });


    </script>
@endsection

@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Edit Color</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">All Colors</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Edit Color</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Edit Color</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul style="margin: 0">
                                            @foreach($errors->all() as $err)
                                                <li>{{ $err }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form action="{{ route('color.update', ['id' => $color->id]) }}" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Color Name</label>
                                                <input type="text" class="form-control" name="name" value="{{$color->name}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Color Code</label>
                                                <input type="text" class="form-control" name="code" value="{{$color->code}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Product</label>
                                                <select class="form-control" name="product">
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}" @if($product_color->product_id == $product->id) selected="selected" @endif >{{$product->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Initial Quantity</label>
                                                <input type="number" class="form-control" name="quantity" value="{{$product_color->quantity}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Quantity In Hand</label>
                                                <input type="text" class="form-control" name="quantity" value="{{$product_color->quantity_in_hand}}" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Color Image</label>
                                                <input type="file" class="form-control" name="image" value="{{$color->image}}">
                                                <br/>
                                                <p><strong>File Name:</strong> {{$color->image}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <table id="products" class="display" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>S#</th>
                                                <th>Color_Code</th>
                                                {{--                                        <th>Quantity</th>--}}
                                                <th>stock_Added</th>
                                                {{--                                        <th>Image</th>--}}
                                                <th>Added_Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = 0; ?>
                                            @foreach($product_stock as $stock)
{{--                                                @foreach($color_detail->stock as $stock)--}}
                                                    <?php $i++ ?>
                                                    <tr>
                                                        <td>{{ $i }}</td>
                                                        <td>{{ $color->code }}</td>

                                                        {{--                                            <td>{{ $product->qty }}</td>--}}
                                                        <td>{{ $stock->quantity }}</td>
                                                        {{--                                            <td>--}}
                                                        {{--                                                <img src="{{ asset('storage/'. $product->image) }}"--}}
                                                        {{--                                                     class="img-responsive" width="100" height="100"/>--}}
                                                        {{--                                            </td>--}}
                                                        <td>
                                                            {{ $stock->created_at }}
                                                        </td>
                                                    </tr>
                                                @endforeach
{{--                                            @endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="product_color" value="{{$product_color->id}}">
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Edit Color">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

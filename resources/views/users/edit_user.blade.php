@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Edit User</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('users.list') }}"
                                                                       class="breadcrumb-link">All Users</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Edit User</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul style="margin: 0">
                                            @foreach($errors->all() as $err)
                                                <li>{{ $err }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @foreach($user as $u)
                                    <form action="{{ route('user.update', ['id' => $u->id]) }}" method="POST">
                                        @csrf
                                        <input name="_method" type="hidden" value="PUT">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">Full Name</label>
                                                    <input type="text" class="form-control" name="name"
                                                           value="{{ $u->name }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">Phone #</label>
                                                    <input type="text" class="form-control" name="phone"
                                                           placeholder="Phone number"
                                                           value="{{ $u->get_meta('phone') }}">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">Email</label>
                                                    <input type="text" class="form-control" name="email"
                                                           placeholder="example@domain.com" value="{{ $u->email }}">
                                                </div>
                                            </div>


                                            {{--                                        <div class="col-md-6">--}}
                                            {{--                                            <div class="form-group">--}}
                                            {{--                                                <label class="col-form-label">Password</label>--}}
                                            {{--                                                <input type="password" class="form-control" name="password" value="{{ $u->password }}">--}}
                                            {{--                                            </div>--}}
                                            {{--                                        </div>--}}

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">Region</label>
                                                    <select class="form-control" id="region" name="region">
                                                        <option value="sindh"
                                                                @if($u->get_meta('region') == 'sindh') selected @endif>
                                                            Sindh
                                                        </option>
                                                        <option value="punjab"
                                                                @if($u->get_meta('region') == 'punjab') selected @endif>
                                                            Punjab
                                                        </option>
                                                        <option value="kpk"
                                                                @if($u->get_meta('region') == 'kpk') selected @endif>KPK
                                                        </option>
                                                        <option value="balochistan"
                                                                @if($u->get_meta('region') == 'balochistan') selected @endif>
                                                            Balochistan
                                                        </option>
                                                        <option value="gilgit_baltistan"
                                                                @if($u->get_meta('region') == 'gilgit_baltistan') selected @endif>
                                                            Gilgit Baltistan
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-form-label">City</label>
                                                    <input type="hidden" id="selected_city"
                                                           selected_city="{{$u->get_meta('city')}}">


                                                    <select name="city" id="city" class="form-control">
                                                        <option class="option" value="Abbottabad" @if($u->get_meta('city') == 'Abbottabad') selected @endif>Abbottabad</option>
                                                        <option class="option" value="Adezai" @if($u->get_meta('city') == 'Adezai') selected @endif>Adezai</option>
                                                        <option class="option" value="Ali Bandar" @if($u->get_meta('city') == 'Ali Bandar') selected @endif>Ali Bandar</option>
                                                        <option class="option" value="Amir Chah" @if($u->get_meta('city') == 'Amir Chah') selected @endif>Amir Chah</option>
                                                        <option class="option" value="Attock" @if($u->get_meta('city') == 'Attock') selected @endif>Attock</option>
                                                        <option class="option" value="Ayubia" @if($u->get_meta('city') == 'Ayubia') selected @endif>Ayubia</option>
                                                        <option class="option" value="Bahawalpur" @if($u->get_meta('city') == 'Bahawalpur') selected @endif>Bahawalpur</option>
                                                        <option class="option" value="Baden" @if($u->get_meta('city') == 'Baden') selected @endif>Baden</option>
                                                        <option class="option" value="Bagh" @if($u->get_meta('city') == 'Bagh') selected @endif>Bagh</option>
                                                        <option class="option" value="Bahawalnagar" @if($u->get_meta('city') == 'Bahawalnagar') selected @endif>Bahawalnagar</option>
                                                        <option class="option" value="Burewala" @if($u->get_meta('city') == 'Burewala') selected @endif>Burewala</option>
                                                        <option class="option" value="Banda Daud Shah" @if($u->get_meta('city') == 'Banda Daud Shah') selected @endif>Banda Daud Shah</option>
                                                        <option class="option" value="Bannu district|Bannu" @if($u->get_meta('city') == 'Bannu district|Bannu') selected @endif>Bannu</option>
                                                        <option class="option" value="Batagram" @if($u->get_meta('city') == 'Batagram') selected @endif>Batagram</option>
                                                        <option class="option" value="Bazdar" @if($u->get_meta('city') == 'Bazdar') selected @endif>Bazdar</option>
                                                        <option class="option" value="Bela" @if($u->get_meta('city') == 'Bela') selected @endif>Bela</option>
                                                        <option class="option" value="Bellpat" @if($u->get_meta('city') == 'Bellpat') selected @endif>Bellpat</option>
                                                        <option class="option" value="Bhag" @if($u->get_meta('city') == 'Bhag') selected @endif>Bhag</option>
                                                        <option class="option" value="Bhakkar" @if($u->get_meta('city') == 'Bhakkar') selected @endif>Bhakkar</option>
                                                        <option class="option" value="Bhalwal" @if($u->get_meta('city') == 'Bhalwal') selected @endif>Bhalwal</option>
                                                        <option class="option" value="Bhimber" @if($u->get_meta('city') == 'Bhimber') selected @endif>Bhimber</option>
                                                        <option class="option" value="Birote" @if($u->get_meta('city') == 'Birote') selected @endif>Birote</option>
                                                        <option class="option" value="Buner" @if($u->get_meta('city') == 'Buner') selected @endif>Buner</option>
                                                        <option class="option" value="Burj" @if($u->get_meta('city') == 'Burj') selected @endif>Burj</option>
                                                        <option class="option" value="Chiniot" @if($u->get_meta('city') == 'Chiniot') selected @endif>Chiniot</option>
                                                        <option class="option" value="Chachro" @if($u->get_meta('city') == 'Chachro') selected @endif>Chachro</option>
                                                        <option class="option" value="Chagai" @if($u->get_meta('city') == 'Chagai') selected @endif>Chagai</option>
                                                        <option class="option" value="Chah Sandan" @if($u->get_meta('city') == 'Chah Sandan') selected @endif>Chah Sandan</option>
                                                        <option class="option" value="Chailianwala" @if($u->get_meta('city') == 'Chailianwala') selected @endif>Chailianwala</option>
                                                        <option class="option" value="Chakdara" @if($u->get_meta('city') == 'Chakdara') selected @endif>Chakdara</option>
                                                        <option class="option" value="Chakku" @if($u->get_meta('city') == 'Chakku') selected @endif>Chakku</option>
                                                        <option class="option" value="Chakwal" @if($u->get_meta('city') == 'Chakwal') selected @endif>Chakwal</option>
                                                        <option class="option" value="Chaman" @if($u->get_meta('city') == 'Chaman') selected @endif>Chaman</option>
                                                        <option class="option" value="CharsEdita" @if($u->get_meta('city') == 'CharsEdita') selected @endif>CharsEdita</option>
                                                        <option class="option" value="Chhatr" @if($u->get_meta('city') == 'Chhatr') selected @endif>Chhatr</option>
                                                        <option class="option" value="Chichawatni" @if($u->get_meta('city') == 'Chichawatni') selected @endif>Chichawatni</option>
                                                        <option class="option" value="Chitral" @if($u->get_meta('city') == 'Chitral') selected @endif>Chitral</option>
                                                        <option class="option" value="Dadu" @if($u->get_meta('city') == 'Dadu') selected @endif>Dadu</option>
                                                        <option class="option" value="Dera Ghazi Khan" @if($u->get_meta('city') == 'Dera Ghazi Khan') selected @endif>Dera Ghazi Khan</option>
                                                        <option class="option" value="Dera Ismail Khan" @if($u->get_meta('city') == 'Dera Ismail Khan') selected @endif>Dera Ismail Khan</option>
                                                        <option class="option" value="Dalbandin" @if($u->get_meta('city') == 'Dalbandin') selected @endif>Dalbandin</option>
                                                        <option class="option" value="Dargai" @if($u->get_meta('city') == 'Dargai') selected @endif>Dargai</option>
                                                        <option class="option" value="Darya Khan" @if($u->get_meta('city') == 'Darya Khan') selected @endif>Darya Khan</option>
                                                        <option class="option" value="Daska" @if($u->get_meta('city') == 'Daska') selected @endif>Daska</option>
                                                        <option class="option" value="Dera Bugti" @if($u->get_meta('city') == 'Dera Bugti') selected @endif>Dera Bugti</option>
                                                        <option class="option" value="Dhana Sar" @if($u->get_meta('city') == 'Dhana Sar') selected @endif>Dhana Sar</option>
                                                        <option class="option" value="Digri" @if($u->get_meta('city') == 'Digri') selected @endif>Digri</option>
                                                        <option class="option" value="Dina City|Dina" @if($u->get_meta('city') == 'Dina City|Dina') selected @endif>Dina</option>
                                                        <option class="option" value="Dinga" @if($u->get_meta('city') == 'Dinga') selected @endif>Dinga</option>
                                                        <option class="option" value="Diplo, Pakistan|Diplo" @if($u->get_meta('city') == 'Diplo, Pakistan|Diplo') selected @endif>Diplo</option>
                                                        <option class="option" value="Diwana" @if($u->get_meta('city') == 'Diwana') selected @endif>Diwana</option>
                                                        <option class="option" value="Dokri" @if($u->get_meta('city') == 'Dokri') selected @endif>Dokri</option>
                                                        <option class="option" value="Drosh" @if($u->get_meta('city') == 'Drosh') selected @endif>Drosh</option>
                                                        <option class="option" value="Duki" @if($u->get_meta('city') == 'Duki') selected @endif>Duki</option>
                                                        <option class="option" value="Dushi" @if($u->get_meta('city') == 'Dushi') selected @endif>Dushi</option>
                                                        <option class="option" value="Duzab" @if($u->get_meta('city') == 'Duzab') selected @endif>Duzab</option>
                                                        <option class="option" value="Faisalabad" @if($u->get_meta('city') == 'Faisalabad') selected @endif>Faisalabad</option>
                                                        <option class="option" value="Fateh Jang" @if($u->get_meta('city') == 'Fateh Jang') selected @endif>Fateh Jang</option>
                                                        <option class="option" value="Ghotki" @if($u->get_meta('city') == 'Ghotki') selected @endif>Ghotki</option>
                                                        <option class="option" value="Gwadar" @if($u->get_meta('city') == 'Gwadar') selected @endif>Gwadar</option>
                                                        <option class="option" value="Gujranwala" @if($u->get_meta('city') == 'Gujranwala') selected @endif>Gujranwala</option>
                                                        <option class="option" value="Gujrat" @if($u->get_meta('city') == 'Gujrat') selected @endif>Gujrat</option>
                                                        <option class="option" value="Gadra" @if($u->get_meta('city') == 'Gadra') selected @endif>Gadra</option>
                                                        <option class="option" value="Gajar" @if($u->get_meta('city') == 'Gajar') selected @endif>Gajar</option>
                                                        <option class="option" value="Gandava" @if($u->get_meta('city') == 'Gandava') selected @endif>Gandava</option>
                                                        <option class="option" value="Garhi Khairo" @if($u->get_meta('city') == 'Garhi Khairo') selected @endif>Garhi Khairo</option>
                                                        <option class="option" value="Garruck" @if($u->get_meta('city') == 'Garruck') selected @endif>Garruck</option>
                                                        <option class="option" value="Ghakhar Mandi" @if($u->get_meta('city') == 'Ghakhar Mandi') selected @endif>Ghakhar Mandi</option>
                                                        <option class="option" value="Ghanian" @if($u->get_meta('city') == 'Ghanian') selected @endif>Ghanian</option>
                                                        <option class="option" value="Ghauspur" @if($u->get_meta('city') == 'Ghauspur') selected @endif>Ghauspur</option>
                                                        <option class="option" value="Ghazluna" @if($u->get_meta('city') == 'Ghazluna') selected @endif>Ghazluna</option>
                                                        <option class="option" value="Girdan" @if($u->get_meta('city') == 'Girdan') selected @endif>Girdan</option>
                                                        <option class="option" value="Gulistan" @if($u->get_meta('city') == 'Gulistan') selected @endif>Gulistan</option>
                                                        <option class="option" value="Gwash" @if($u->get_meta('city') == 'Gwash') selected @endif>Gwash</option>
                                                        <option class="option" value="Hyderabad" @if($u->get_meta('city') == 'Hyderabad') selected @endif>Hyderabad</option>
                                                        <option class="option" value="Hala" @if($u->get_meta('city') == 'Hala') selected @endif>Hala</option>
                                                        <option class="option" value="Haripur" @if($u->get_meta('city') == 'Haripur') selected @endif>Haripur</option>
                                                        <option class="option" value="Hab Chauki" @if($u->get_meta('city') == 'Hab Chauki') selected @endif>Hab Chauki</option>
                                                        <option class="option" value="Hafizabad" @if($u->get_meta('city') == 'Hafizabad') selected @endif>Hafizabad</option>
                                                        <option class="option" value="Hameedabad" @if($u->get_meta('city') == 'Hameedabad') selected @endif>Hameedabad</option>
                                                        <option class="option" value="Hangu" @if($u->get_meta('city') == 'Hangu') selected @endif>Hangu</option>
                                                        <option class="option" value="Harnai" @if($u->get_meta('city') == 'Harnai') selected @endif>Harnai</option>
                                                        <option class="option" value="Hasilpur" @if($u->get_meta('city') == 'Hasilpur') selected @endif>Hasilpur</option>
                                                        <option class="option" value="Haveli Lakha" @if($u->get_meta('city') == 'Haveli Lakha') selected @endif>Haveli Lakha</option>
                                                        <option class="option" value="Hinglaj" @if($u->get_meta('city') == 'Hinglaj') selected @endif>Hinglaj</option>
                                                        <option class="option" value="Hoshab" @if($u->get_meta('city') == 'Hoshab') selected @endif>Hoshab</option>
                                                        <option class="option" value="Islamabad" @if($u->get_meta('city') == 'Islamabad') selected @endif>Islamabad</option>
                                                        <option class="option" value="Islamkot" @if($u->get_meta('city') == 'Islamkot') selected @endif>Islamkot</option>
                                                        <option class="option" value="Ispikan" @if($u->get_meta('city') == 'Ispikan') selected @endif>Ispikan</option>
                                                        <option class="option" value="Jacobabad" @if($u->get_meta('city') == 'Jacobabad') selected @endif>Jacobabad</option>
                                                        <option class="option" value="Jamshoro" @if($u->get_meta('city') == 'Jamshoro') selected @endif>Jamshoro</option>
                                                        <option class="option" value="Jhang" @if($u->get_meta('city') == 'Jhang') selected @endif>Jhang</option>
                                                        <option class="option" value="Jhelum" @if($u->get_meta('city') == 'Jhelum') selected @endif>Jhelum</option>
                                                        <option class="option" value="Jamesabad" @if($u->get_meta('city') == 'Jamesabad') selected @endif>Jamesabad</option>
                                                        <option class="option" value="Jampur" @if($u->get_meta('city') == 'Jampur') selected @endif>Jampur</option>
                                                        <option class="option" value="Janghar" @if($u->get_meta('city') == 'Janghar') selected @endif>Janghar</option>
                                                        <option class="option" value="Jati, Jati(Mughalbhin)" @if($u->get_meta('city') == 'Jati, Jati(Mughalbhin)') selected @endif>Jati</option>
                                                        <option class="option" value="Jauharabad" @if($u->get_meta('city') == 'Jauharabad') selected @endif>Jauharabad</option>
                                                        <option class="option" value="Jhal" @if($u->get_meta('city') == 'Jhal') selected @endif>Jhal</option>
                                                        <option class="option" value="Jhal Jhao" @if($u->get_meta('city') == 'Jhal Jhao') selected @endif>Jhal Jhao</option>
                                                        <option class="option" value="Jhatpat" @if($u->get_meta('city') == 'Jhatpat') selected @endif>Jhatpat</option>
                                                        <option class="option" value="Jhudo" @if($u->get_meta('city') == 'Jhudo') selected @endif>Jhudo</option>
                                                        <option class="option" value="Jiwani" @if($u->get_meta('city') == 'Jiwani') selected @endif>Jiwani</option>
                                                        <option class="option" value="Jungshahi" @if($u->get_meta('city') == 'Jungshahi') selected @endif>Jungshahi</option>
                                                        <option class="option" value="Karachi" @if($u->get_meta('city') == 'Karachi') selected @endif>Karachi</option>
                                                        <option class="option" value="Kotri" @if($u->get_meta('city') == 'Kotri') selected @endif>Kotri</option>
                                                        <option class="option" value="Kalam" @if($u->get_meta('city') == 'Kalam') selected @endif>Kalam</option>
                                                        <option class="option" value="Kalandi" @if($u->get_meta('city') == 'Kalandi') selected @endif>Kalandi</option>
                                                        <option class="option" value="Kalat" @if($u->get_meta('city') == 'Kalat') selected @endif>Kalat</option>
                                                        <option class="option" value="Kamalia" @if($u->get_meta('city') == 'Kamalia') selected @endif>Kamalia</option>
                                                        <option class="option" value="Kamararod" @if($u->get_meta('city') == 'Kamararod') selected @endif>Kamararod</option>
                                                        <option class="option" value="Kamber" @if($u->get_meta('city') == 'Kamber') selected @endif>Kamber</option>
                                                        <option class="option" value="Kamokey" @if($u->get_meta('city') == 'Kamokey') selected @endif>Kamokey</option>
                                                        <option class="option" value="Kandhkot" @if($u->get_meta('city') == 'Kandhkot') selected @endif>Kandhkot</option>
                                                        <option class="option" value="Kanak" @if($u->get_meta('city') == 'Kanak') selected @endif>Kanak</option>
                                                        <option class="option" value="Kandi" @if($u->get_meta('city') == 'Kandi') selected @endif>Kandi</option>
                                                        <option class="option" value="Kandiaro" @if($u->get_meta('city') == 'Kandiaro') selected @endif>Kandiaro</option>
                                                        <option class="option" value="Kanpur" @if($u->get_meta('city') == 'Kanpur') selected @endif>Kanpur</option>
                                                        <option class="option" value="Kapip" @if($u->get_meta('city') == 'Kapip') selected @endif>Kapip</option>
                                                        <option class="option" value="Kappar" @if($u->get_meta('city') == 'Kappar') selected @endif>Kappar</option>
                                                        <option class="option" value="Karak City" @if($u->get_meta('city') == 'Karak City') selected @endif>Karak City</option>
                                                        <option class="option" value="Karodi" @if($u->get_meta('city') == 'Karodi') selected @endif>Karodi</option>
                                                        <option class="option" value="Kashmor" @if($u->get_meta('city') == 'Kashmor') selected @endif>Kashmor</option>
                                                        <option class="option" value="Kasur" @if($u->get_meta('city') == 'Kasur') selected @endif>Kasur</option>
                                                        <option class="option" value="Katuri" @if($u->get_meta('city') == 'Katuri') selected @endif>Katuri</option>
                                                        <option class="option" value="Keti Bandar" @if($u->get_meta('city') == 'Keti Bandar') selected @endif>Keti Bandar</option>
                                                        <option class="option" value="Khairpur" @if($u->get_meta('city') == 'Khairpur') selected @endif>Khairpur</option>
                                                        <option class="option" value="Khanaspur" @if($u->get_meta('city') == 'Khanaspur') selected @endif>Khanaspur</option>
                                                        <option class="option" value="Khanewal"@if($u->get_meta('city') == 'Khanewal') selected @endif>Khanewal</option>
                                                        <option class="option" value="Kharan" @if($u->get_meta('city') == 'Kharan') selected @endif>Kharan</option>
                                                        <option class="option" value="kharian" @if($u->get_meta('city') == 'kharian') selected @endif>kharian</option>
                                                        <option class="option" value="Khokhropur" @if($u->get_meta('city') == 'Khokhropur') selected @endif>Khokhropur</option>
                                                        <option class="option" value="Khora" @if($u->get_meta('city') == 'Khora') selected @endif>Khora</option>
                                                        <option class="option" value="Khushab" @if($u->get_meta('city') == 'Khushab') selected @endif>Khushab</option>
                                                        <option class="option" value="Khuzdar" @if($u->get_meta('city') == 'Khuzdar') selected @endif>Khuzdar</option>
                                                        <option class="option" value="Kikki" @if($u->get_meta('city') == 'Kikki') selected @endif>Kikki</option>
                                                        <option class="option" value="Klupro" @if($u->get_meta('city') == 'Klupro') selected @endif>Klupro</option>
                                                        <option class="option" value="Kohan" @if($u->get_meta('city') == 'Kohan') selected @endif>Kohan</option>
                                                        <option class="option" value="Kohat" @if($u->get_meta('city') == 'Kohat') selected @endif>Kohat</option>
                                                        <option class="option" value="Kohistan" @if($u->get_meta('city') == 'Kohistan') selected @endif>Kohistan</option>
                                                        <option class="option" value="Kohlu" @if($u->get_meta('city') == 'Kohlu') selected @endif>Kohlu</option>
                                                        <option class="option" value="Korak" @if($u->get_meta('city') == 'Korak') selected @endif>Korak</option>
                                                        <option class="option" value="Korangi" @if($u->get_meta('city') == 'Korangi') selected @endif>Korangi</option>
                                                        <option class="option" value="Kot Sarae" @if($u->get_meta('city') == 'Kot Sarae') selected @endif>Kot Sarae</option>
                                                        <option class="option" value="Kotli" @if($u->get_meta('city') == 'Kotli') selected @endif>Kotli</option>
                                                        <option class="option" value="Lahore" @if($u->get_meta('city') == 'Lahore') selected @endif>Lahore</option>
                                                        <option class="option" value="Larkana" @if($u->get_meta('city') == 'Larkana') selected @endif>Larkana</option>
                                                        <option class="option" value="Lahri" @if($u->get_meta('city') == 'Lahri') selected @endif>Lahri</option>
                                                        <option class="option" value="Lakki Marwat" @if($u->get_meta('city') == 'Lakki Marwat') selected @endif>Lakki Marwat</option>
                                                        <option class="option" value="Lasbela" @if($u->get_meta('city') == 'Lasbela') selected @endif>Lasbela</option>
                                                        <option class="option" value="Latamber" @if($u->get_meta('city') == 'Latamber') selected @endif>Latamber</option>
                                                        <option class="option" value="Layyah" @if($u->get_meta('city') == 'Layyah') selected @endif>Layyah</option>
                                                        <option class="option" value="Leiah" @if($u->get_meta('city') == 'Leiah') selected @endif>Leiah</option>
                                                        <option class="option" value="Liari" @if($u->get_meta('city') == 'Liari') selected @endif>Liari</option>
                                                        <option class="option" value="Lodhran" @if($u->get_meta('city') == 'Lodhran') selected @endif>Lodhran</option>
                                                        <option class="option" value="Loralai" @if($u->get_meta('city') == 'Loralai') selected @endif>Loralai</option>
                                                        <option class="option" value="Lower Dir" @if($u->get_meta('city') == 'Lower Dir') selected @endif>Lower Dir</option>
                                                        <option class="option" value="Shadan Lund" @if($u->get_meta('city') == 'Shadan Lund') selected @endif>Shadan Lund</option>
                                                        <option class="option" value="Multan" @if($u->get_meta('city') == 'Multan') selected @endif>Multan</option>
                                                        <option class="option" value="Mandi Bahauddin" @if($u->get_meta('city') == 'Mandi Bahauddin') selected @endif>Mandi Bahauddin</option>
                                                        <option class="option" value="Mansehra" @if($u->get_meta('city') == 'Mansehra') selected @endif>Mansehra</option>
                                                        <option class="option" value="Mian Chanu" @if($u->get_meta('city') == 'Mian Chanu') selected @endif>Mian Chanu</option>
                                                        <option class="option" value="Mirpur" @if($u->get_meta('city') == 'Mirpur') selected @endif>Mirpur</option>
                                                        <option class="option" value="Mirpur Mathelo" @if($u->get_meta('city') == 'Mirpur Mathelo') selected @endif>Mirpur Mathelo</option>
                                                        <option class="option" value="Moro, Pakistan|Moro" @if($u->get_meta('city') == 'Moro, Pakistan|Moro') selected @endif>Moro</option>
                                                        <option class="option" value="Mardan" @if($u->get_meta('city') == 'Mardan') selected @endif>Mardan</option>
                                                        <option class="option" value="Mach" @if($u->get_meta('city') == 'Mach') selected @endif>Mach</option>
                                                        <option class="option" value="Madyan" @if($u->get_meta('city') == 'Madyan') selected @endif>Madyan</option>
                                                        <option class="option" value="Malakand" @if($u->get_meta('city') == 'Malakand') selected @endif>Malakand</option>
                                                        <option class="option" value="Mand" @if($u->get_meta('city') == 'Mand') selected @endif>Mand</option>
                                                        <option class="option" value="Manguchar" @if($u->get_meta('city') == 'Manguchar') selected @endif>Manguchar</option>
                                                        <option class="option" value="Mashki Chah" @if($u->get_meta('city') == 'Ayubia') selected @endif>Mashki Chah</option>
                                                        <option class="option" value="Maslti" @if($u->get_meta('city') == 'Maslti') selected @endif>Maslti</option>
                                                        <option class="option" value="Mastuj" @if($u->get_meta('city') == 'Mastuj') selected @endif>Mastuj</option>
                                                        <option class="option" value="Mastung" @if($u->get_meta('city') == 'Mastung') selected @endif>Mastung</option>
                                                        <option class="option" value="Mathi" @if($u->get_meta('city') == 'Mathi') selected @endif>Mathi</option>
                                                        <option class="option" value="Matiari" @if($u->get_meta('city') == 'Matiari') selected @endif>Matiari</option>
                                                        <option class="option" value="Mehar" @if($u->get_meta('city') == 'Mehar') selected @endif>Mehar</option>
                                                        <option class="option" value="Mekhtar" @if($u->get_meta('city') == 'Mekhtar') selected @endif>Mekhtar</option>
                                                        <option class="option" value="Merui" @if($u->get_meta('city') == 'Merui') selected @endif>Merui</option>
                                                        <option class="option" value="Mianwali" @if($u->get_meta('city') == 'Mianwali') selected @endif>Mianwali</option>
                                                        <option class="option" value="Mianez" @if($u->get_meta('city') == 'Mianez') selected @endif>Mianez</option>
                                                        <option class="option" value="Mirpur Batoro" @if($u->get_meta('city') == 'Mirpur Batoro') selected @endif>Mirpur Batoro</option>
                                                        <option class="option" value="Mirpur Khas" @if($u->get_meta('city') == 'Mirpur Khas') selected @endif>Mirpur Khas</option>
                                                        <option class="option" value="Mirpur Sakro" @if($u->get_meta('city') == 'Mirpur Sakro') selected @endif>Mirpur Sakro</option>
                                                        <option class="option" value="Mithi" @if($u->get_meta('city') == 'Mithi') selected @endif>Mithi</option>
                                                        <option class="option" value="Mongora" @if($u->get_meta('city') == 'Mongora') selected @endif>Mongora</option>
                                                        <option class="option" value="Murgha Kibzai" @if($u->get_meta('city') == 'Murgha Kibzai') selected @endif>Murgha Kibzai</option>
                                                        <option class="option" value="Muridke" @if($u->get_meta('city') == 'Muridke') selected @endif>Muridke</option>
                                                        <option class="option" value="Musa Khel Bazar" @if($u->get_meta('city') == 'Musa Khel Bazar') selected @endif>Musa Khel Bazar</option>
                                                        <option class="option" value="Muzaffar Garh" @if($u->get_meta('city') == 'Muzaffar Garh') selected @endif>Muzaffar Garh</option>
                                                        <option class="option" value="Muzaffarabad" @if($u->get_meta('city') == 'Muzaffarabad') selected @endif>Muzaffarabad</option>
                                                        <option class="option" value="Nawabshah" @if($u->get_meta('city') == 'Nawabshah') selected @endif>Nawabshah</option>
                                                        <option class="option" value="Nazimabad" @if($u->get_meta('city') == 'Nazimabad') selected @endif>Nazimabad</option>
                                                        <option class="option" value="Nowshera" @if($u->get_meta('city') == 'Nowshera') selected @endif>Nowshera</option>
                                                        <option class="option" value="Nagar Parkar" @if($u->get_meta('city') == 'Nagar Parkar') selected @endif>Nagar Parkar</option>
                                                        <option class="option" value="Nagha Kalat" @if($u->get_meta('city') == 'Nagha Kalat') selected @endif>Nagha Kalat</option>
                                                        <option class="option" value="Nal" @if($u->get_meta('city') == 'Nal') selected @endif>Nal</option>
                                                        <option class="option" value="Naokot" @if($u->get_meta('city') == 'Naokot') selected @endif>Naokot</option>
                                                        <option class="option" value="Nasirabad" @if($u->get_meta('city') == 'Nasirabad') selected @endif>Nasirabad</option>
                                                        <option class="option" value="Nauroz Kalat" @if($u->get_meta('city') == 'Nauroz Kalat') selected @endif>Nauroz Kalat</option>
                                                        <option class="option" value="Naushara" @if($u->get_meta('city') == 'Naushara') selected @endif>Naushara</option>
                                                        <option class="option" value="Nur Gamma" @if($u->get_meta('city') == 'Nur Gamma') selected @endif>Nur Gamma</option>
                                                        <option class="option" value="Nushki" @if($u->get_meta('city') == 'Nushki') selected @endif>Nushki</option>
                                                        <option class="option" value="Nuttal" @if($u->get_meta('city') == 'Nuttal') selected @endif>Nuttal</option>
                                                        <option class="option" value="Okara" @if($u->get_meta('city') == 'Okara') selected @endif>Okara</option>
                                                        <option class="option" value="Ormara" @if($u->get_meta('city') == 'Ormara') selected @endif>Ormara</option>
                                                        <option class="option" value="Peshawar" @if($u->get_meta('city') == 'Peshawar') selected @endif>Peshawar</option>
                                                        <option class="option" value="Panjgur" @if($u->get_meta('city') == 'Panjgur') selected @endif>Panjgur</option>
                                                        <option class="option" value="Pasni City" @if($u->get_meta('city') == 'Pasni City') selected @endif>Pasni City</option>
                                                        <option class="option" value="Paharpur" @if($u->get_meta('city') == 'Paharpur') selected @endif>Paharpur</option>
                                                        <option class="option" value="Palantuk" @if($u->get_meta('city') == 'Palantuk') selected @endif>Palantuk</option>
                                                        <option class="option" value="Pendoo" @if($u->get_meta('city') == 'Pendoo') selected @endif>Pendoo</option>
                                                        <option class="option" value="Piharak" @if($u->get_meta('city') == 'Piharak') selected @endif>Piharak</option>
                                                        <option class="option" value="Pirmahal" @if($u->get_meta('city') == 'Pirmahal') selected @endif>Pirmahal</option>
                                                        <option class="option" value="Pishin" @if($u->get_meta('city') == 'Pishin') selected @endif>Pishin</option>
                                                        <option class="option" value="Plandri" @if($u->get_meta('city') == 'Plandri') selected @endif>Plandri</option>
                                                        <option class="option" value="Pokran" @if($u->get_meta('city') == 'Pokran') selected @endif>Pokran</option>
                                                        <option class="option" value="Pounch" @if($u->get_meta('city') == 'Pounch') selected @endif>Pounch</option>
                                                        <option class="option" value="Qambar" @if($u->get_meta('city') == 'Qambar') selected @endif>Quetta</option>
                                                        <option class="option" value="Qambar" @if($u->get_meta('city') == 'Qambar') selected @endif>Qambar</option>
                                                        <option class="option" value="Qamruddin Karez" @if($u->get_meta('city') == 'Qamruddin Karez') selected @endif>Qamruddin Karez</option>
                                                        <option class="option" value="Qazi Ahmad" @if($u->get_meta('city') == 'Qazi Ahmad') selected @endif>Qazi Ahmad</option>
                                                        <option class="option" value="Qila Abdullah" @if($u->get_meta('city') == 'Qila Abdullah') selected @endif>Qila Abdullah</option>
                                                        <option class="option" value="Qila Ladgasht" @if($u->get_meta('city') == 'Qila Ladgasht') selected @endif>Qila Ladgasht</option>
                                                        <option class="option" value="Qila Safed" @if($u->get_meta('city') == 'Qila Safed') selected @endif>Qila Safed</option>
                                                        <option class="option" value="Qila Saifullah" @if($u->get_meta('city') == 'Qila Saifullah') selected @endif>Qila Saifullah</option>
                                                        <option class="option" value="Rawalpindi" @if($u->get_meta('city') == 'Rawalpindi') selected @endif>Rawalpindi</option>
                                                        <option class="option" value="Rabwah" @if($u->get_meta('city') == 'Rabwah') selected @endif>Rabwah</option>
                                                        <option class="option" value="Rahim Yar Khan" @if($u->get_meta('city') == 'Rahim Yar Khan') selected @endif>Rahim Yar Khan</option>
                                                        <option class="option" value="Rajan Pur" @if($u->get_meta('city') == 'Rajan Pur') selected @endif>Rajan Pur</option>
                                                        <option class="option" value="Rakhni" @if($u->get_meta('city') == 'Rakhni') selected @endif>Rakhni</option>
                                                        <option class="option" value="Ranipur" @if($u->get_meta('city') == 'Ranipur') selected @endif>Ranipur</option>
                                                        <option class="option" value="Ratodero" @if($u->get_meta('city') == 'Ratodero') selected @endif>Ratodero</option>
                                                        <option class="option" value="Rawalakot"  @if($u->get_meta('city') == 'Rawalakot') selected @endif>Rawalakot</option>
                                                        <option class="option" value="Renala Khurd"  @if($u->get_meta('city') == 'Renala Khurd') selected @endif>Renala Khurd</option>
                                                        <option class="option" value="Robat Thana"  @if($u->get_meta('city') == 'Robat Thana') selected @endif>Robat Thana</option>
                                                        <option class="option" value="Rodkhan"  @if($u->get_meta('city') == 'Rodkhan') selected @endif>Rodkhan</option>
                                                        <option class="option" value="Rohri"  @if($u->get_meta('city') == 'Rohri') selected @endif>Rohri</option>
                                                        <option class="option" value="Sialkot"  @if($u->get_meta('city') == 'Sialkot') selected @endif>Sialkot</option>
                                                        <option class="option" value="Sadiqabad"  @if($u->get_meta('city') == 'Sadiqabad') selected @endif>Sadiqabad</option>
                                                        <option class="option" value="Safdar Abad- (Dhaban Singh)"  @if($u->get_meta('city') == 'Safdar Abad- (Dhaban Singh)') selected @endif>Safdar Abad</option>
                                                        <option class="option" value="Sahiwal"  @if($u->get_meta('city') == 'Sahiwal') selected @endif>Sahiwal</option>
                                                        <option class="option" value="Saidu Sharif"  @if($u->get_meta('city') == 'Saidu Sharif') selected @endif>Saidu Sharif</option>
                                                        <option class="option" value="Saindak"  @if($u->get_meta('city') == 'Saindak') selected @endif>Saindak</option>
                                                        <option class="option" value="Sakrand"  @if($u->get_meta('city') == 'Sakrand') selected @endif>Sakrand</option>
                                                        <option class="option" value="Sanjawi"  @if($u->get_meta('city') == 'Sanjawi') selected @endif>Sanjawi</option>
                                                        <option class="option" value="Sargodha"  @if($u->get_meta('city') == 'Sargodha') selected @endif>Sargodha</option>
                                                        <option class="option" value="Saruna"  @if($u->get_meta('city') == 'Saruna') selected @endif>Saruna</option>
                                                        <option class="option" value="Shabaz Kalat"  @if($u->get_meta('city') == 'Shabaz Kalat') selected @endif>Shabaz Kalat</option>
                                                        <option class="option" value="Shadadkhot"  @if($u->get_meta('city') == 'Shadadkhot') selected @endif>Shadadkhot</option>
                                                        <option class="option" value="Shahbandar"  @if($u->get_meta('city') == 'Shahbandar') selected @endif>Shahbandar</option>
                                                        <option class="option" value="Shahpur"  @if($u->get_meta('city') == 'Shahpur') selected @endif>Shahpur</option>
                                                        <option class="option" value="Shahpur Chakar"  @if($u->get_meta('city') == 'Shahpur Chakar') selected @endif>Shahpur Chakar</option>
                                                        <option class="option" value="Shakargarh"  @if($u->get_meta('city') == 'Shakargarh') selected @endif>Shakargarh</option>
                                                        <option class="option" value="Shangla"  @if($u->get_meta('city') == 'Shangla') selected @endif>Shangla</option>
                                                        <option class="option" value="Sharam Jogizai"  @if($u->get_meta('city') == 'Sharam Jogizai') selected @endif>Sharam Jogizai</option>
                                                        <option class="option" value="Sheikhupura"  @if($u->get_meta('city') == 'Sheikhupura') selected @endif>Sheikhupura</option>
                                                        <option class="option" value="Shikarpur"  @if($u->get_meta('city') == 'Shikarpur') selected @endif>Shikarpur</option>
                                                        <option class="option" value="Shingar"  @if($u->get_meta('city') == 'Shingar') selected @endif>Shingar</option>
                                                        <option class="option" value="Shorap"  @if($u->get_meta('city') == 'Shorap') selected @endif>Shorap</option>
                                                        <option class="option" value="Sibi"  @if($u->get_meta('city') == 'Sibi') selected @endif>Sibi</option>
                                                        <option class="option" value="Sohawa"  @if($u->get_meta('city') == 'Sohawa') selected @endif>Sohawa</option>
                                                        <option class="option" value="Sonmiani"  @if($u->get_meta('city') == 'Sonmiani') selected @endif>Sonmiani</option>
                                                        <option class="option" value="Sooianwala"  @if($u->get_meta('city') == 'Sooianwala') selected @endif>Sooianwala</option>
                                                        <option class="option" value="Spezand" @if($u->get_meta('city') == 'Spezand') selected @endif>Spezand</option>
                                                        <option class="option" value="Spintangi" @if($u->get_meta('city') == 'Spintangi') selected @endif>Spintangi</option>
                                                        <option class="option" value="Sui" @if($u->get_meta('city') == 'Sui') selected @endif>Sui</option>
                                                        <option class="option" value="Sujawal" @if($u->get_meta('city') == 'Sujawal') selected @endif>Sujawal</option>
                                                        <option class="option" value="Sukkur" @if($u->get_meta('city') == 'Sukkur') selected @endif>Sukkur</option>
                                                        <option class="option" value="Suntsar" @if($u->get_meta('city') == 'Suntsar') selected @endif>Suntsar</option>
                                                        <option class="option" value="Surab" @if($u->get_meta('city') == 'Surab') selected @endif>Surab</option>
                                                        <option class="option" value="Swabi" @if($u->get_meta('city') == 'Swabi') selected @endif>Swabi</option>
                                                        <option class="option" value="Swat" @if($u->get_meta('city') == 'Swat') selected @endif>Swat</option>
                                                        <option class="option" value="Tando Adam" @if($u->get_meta('city') == 'Tando Adam') selected @endif>Tando Adam</option>
                                                        <option class="option" value="Tando Bago" @if($u->get_meta('city') == 'Tando Bago') selected @endif>Tando Bago</option>
                                                        <option class="option" value="Tando Allahyar" @if($u->get_meta('city') == 'Tando Allahyar') selected @endif>Tando Allahyar</option>
                                                        <option class="option" value="Tando Muhammad Khan" @if($u->get_meta('city') == 'Tando Muhammad Khan') selected @endif> Tando Muhammad Khan</option>
                                                        <option class="option" value="Tangi" @if($u->get_meta('city') == 'Tangi') selected @endif>Tangi</option>
                                                        <option class="option" value="Tank City" @if($u->get_meta('city') == 'Tank City') selected @endif>Tank City</option>
                                                        <option class="option" value="Tar Ahamd Rind" @if($u->get_meta('city') == 'Tar Ahamd Rind') selected @endif>Tar Ahamd Rind</option>
                                                        <option class="option" value="Thalo" @if($u->get_meta('city') == 'Thalo') selected @endif>Thalo</option>
                                                        <option class="option" value="Thatta" @if($u->get_meta('city') == 'Thatta') selected @endif>Thatta</option>
                                                        <option class="option" value="Toba Tek Singh" @if($u->get_meta('city') == 'Toba Tek Singh') selected @endif>Toba Tek Singh</option>
                                                        <option class="option" value="Tordher" @if($u->get_meta('city') == 'Tordher') selected @endif>Tordher</option>
                                                        <option class="option" value="Tujal" @if($u->get_meta('city') == 'Tujal') selected @endif>Tujal</option>
                                                        <option class="option" value="Tump" @if($u->get_meta('city') == 'Tump') selected @endif>Tump</option>
                                                        <option class="option" value="Turbat" @if($u->get_meta('city') == 'Turbat') selected @endif>Turbat</option>
                                                        <option class="option" value="Umarao" @if($u->get_meta('city') == 'Umarao') selected @endif>Umarao</option>
                                                        <option class="option" value="Umerkot" @if($u->get_meta('city') == 'Umerkot') selected @endif>Umerkot</option>
                                                        <option class="option" value="Upper Dir" @if($u->get_meta('city') == 'Upper Dir') selected @endif>Upper Dir</option>
                                                        <option class="option" value="Uthal" @if($u->get_meta('city') == 'Uthal') selected @endif>Uthal</option>
                                                        <option class="option" value="Vehari" @if($u->get_meta('city') == 'Vehari') selected @endif>Vehari</option>
                                                        <option class="option" value="Veirwaro" @if($u->get_meta('city') == 'Veirwaro') selected @endif>Veirwaro</option>
                                                        <option class="option" value="Vitakri" @if($u->get_meta('city') == 'Vitakri') selected @endif>Vitakri</option>
                                                        <option class="option" value="Wadh" @if($u->get_meta('city') == 'Wadh') selected @endif>Wadh</option>
                                                        <option class="option" value="Wah Cantt" @if($u->get_meta('city') == 'Wah Cantt') selected @endif>Wah Cantt</option>
                                                        <option class="option" value="Warah" @if($u->get_meta('city') == 'Warah') selected @endif>Warah</option>
                                                        <option class="option" value="Washap" @if($u->get_meta('city') == 'Washap') selected @endif>Washap</option>
                                                        <option class="option" value="Wasjuk" @if($u->get_meta('city') == 'Wasjuk') selected @endif>Wasjuk</option>
                                                        <option class="option" value="Wazirabad" @if($u->get_meta('city') == 'Wazirabad') selected @endif>Wazirabad</option>
                                                        <option class="option" value="Yakmach" @if($u->get_meta('city') == 'Yakmach') selected @endif>Yakmach</option>
                                                        <option class="option" value="Zhob" @if($u->get_meta('city') == 'Zhob') selected @endif>Zhob</option>
                                                        <option class="option" value="Other">Other</option>
                                                    </select>

                                                    {{--                                                    <select name="city" id="city" class="form-control"--}}
{{--                                                            value="{{old('city')}}">--}}
{{--                                                        <option class="option" value="Badin"--}}
{{--                                                                @if($u->get_meta('city') == 'Badin') selected @endif>--}}
{{--                                                            Badin--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Dadu"--}}
{{--                                                                @if($u->get_meta('city') == 'Dadu') selected @endif>Dadu--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Daharki">Daharki</option>--}}
{{--                                                        <option class="option" value="Ghotki"--}}
{{--                                                                @if($u->get_meta('city') == 'Ghotki') selected @endif>--}}
{{--                                                            Ghotki--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Hyderabad"--}}
{{--                                                                @if($u->get_meta('city') == 'Hyderabad') selected @endif>--}}
{{--                                                            Hyderabad--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Jacobabad"--}}
{{--                                                                @if($u->get_meta('city') == 'Jacobabad') selected @endif>--}}
{{--                                                            Jacobabad--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Karachi"--}}
{{--                                                                @if($u->get_meta('city') == 'Karachi') selected @endif>--}}
{{--                                                            Karachi--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Kotri"--}}
{{--                                                                @if($u->get_meta('city') == 'Kotri') selected @endif>--}}
{{--                                                            Kotri--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Kamber"--}}
{{--                                                                @if($u->get_meta('city') == 'Kamber') selected @endif>--}}
{{--                                                            Kamber--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Kandhkot"--}}
{{--                                                                @if($u->get_meta('city') == 'Kandhkot') selected @endif>--}}
{{--                                                            Kandhkot--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Khairpur"--}}
{{--                                                                @if($u->get_meta('city') == 'Khairpur') selected @endif>--}}
{{--                                                            Khairpur--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Larkana"--}}
{{--                                                                @if($u->get_meta('city') == 'Larkana') selected @endif>--}}
{{--                                                            Larkana--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Mirpur Khas"--}}
{{--                                                                @if($u->get_meta('city') == 'Mirpur Khas') selected @endif>--}}
{{--                                                            Mirpur Khas--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Mirpur Mathelo"--}}
{{--                                                                @if($u->get_meta('city') == 'Mirpur Mathelo') selected @endif>--}}
{{--                                                            Mirpur Mathelo--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Nawabshah"--}}
{{--                                                                @if($u->get_meta('city') == 'Nawabshah') selected @endif>--}}
{{--                                                            Nawabshah--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Shahdadkot"--}}
{{--                                                                @if($u->get_meta('city') == 'Shahdadkot') selected @endif>--}}
{{--                                                            Shahdadkot--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Shikarpur"--}}
{{--                                                                @if($u->get_meta('city') == 'Shikarpur') selected @endif>--}}
{{--                                                            Shikarpur--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Sukkur"--}}
{{--                                                                @if($u->get_meta('city') == 'Sukkur') selected @endif>--}}
{{--                                                            Sukkur--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Tando Adam"--}}
{{--                                                                @if($u->get_meta('city') == 'Tando Adam') selected @endif>--}}
{{--                                                            Tando Adam--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Tando Allahyar"--}}
{{--                                                                @if($u->get_meta('city') == 'Tando Allahyar') selected @endif>--}}
{{--                                                            Tando Allahyar--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Tando Muhammad Khan"--}}
{{--                                                                @if($u->get_meta('city') == 'Tando Muhammad Khan') selected @endif>--}}
{{--                                                            Tando Muhammad Khan--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Umerkot"--}}
{{--                                                                @if($u->get_meta('city') == 'Umerkot') selected @endif>--}}
{{--                                                            Umerkot--}}
{{--                                                        </option>--}}
{{--                                                        <option class="option" value="Other"--}}
{{--                                                                @if($u->get_meta('city') == 'Other ') selected @endif>--}}
{{--                                                            Other--}}
{{--                                                        </option>--}}
{{--                                                    </select>--}}


                                                </div>
                                            </div>

                                            @if($u->hasRole('client'))
                                                <div class="col-md-12" id="dis_row">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Distributor</label>
                                                        <select class="form-control" name="distributor">
                                                            <option value="">Choose the distributor</option>
                                                            @foreach($distributor as $dis)
                                                                <option value="{{$dis->id}}"
                                                                        @if($u->get_meta('distributor_id') == $dis->id) selected @endif>{{$dis->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Address</label>

                                                    @if($u->get_meta('address', 'N/A') == 'N/A')
                                                        <textarea class="form-control" name="address" placeholder="Address not found enter your address"></textarea>
                                                    @else
                                                        <textarea class="form-control" name="address" placeholder="Address not found enter your address">{{$u->get_meta('address', 'N/A')}}</textarea>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Additional Information (Optional)</label>

                                                    @if($u->get_meta('additional_info', 'N/A') == 'N/A')
                                                        <textarea class="form-control" name="additional_info" placeholder="Additional information not found enter your information"></textarea>
                                                    @else
                                                        <textarea class="form-control" name="additional_info" placeholder="Additional information not found enter your information">{{ $u->get_meta('additional_info') }}</textarea>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="Edit User">
                                        </div>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script>

        var selected_city = $("#selected_city").attr("selected_city");


        $('#region').change(function () {


            if ($("#region").val() === "sindh") {

                $('#city').empty();

                let cities = {
                    'Badin': 'Badin',
                    'Dadu': 'Dadu',
                    'Daharki': 'Daharki',
                    'Ghotki': 'Ghotki',
                    'Hyderabad': 'Hyderabad',
                    'Jacobabad': 'Jacobabad',
                    'Kamber': 'Kamber',
                    'Kandhkot': 'Kandhkot',
                    'Karachi': 'Karachi',
                    'Khairpur': 'Khairpur',
                    'Kotri': 'Kotri',
                    'Larkana': 'Larkana',
                    'Mirpur Khas': 'Mirpur Khas',
                    'Mirpur Mathelo': 'Mirpur Mathelo',
                    'Nawabshah': 'Nawabshah',
                    'Shahdadkot': 'Shahdadkot',
                    'Shikarpur': 'Shikarpur',
                    'Sukkur': 'Sukkur',
                    'Tando Adam': 'Tando Adam',
                    'Tando Allahyar': 'Tando Allahyar',
                    'Tando Muhammad Khan': 'Tando Muhammad Khan',
                    'Umerkot': 'Umerkot',
                    'Other': 'Other'
                };

                for (let c in cities) {

                    $('#city').append($('<option>', {value: c, text: cities[c]})).attr("selected", "selected")
                }

            } else if ($("#region").val() === "punjab") {

                $('#city').empty();

                let cities = {

                    'Ahmedpur East': 'Ahmedpur East',
                    'Arif Wala': 'Arif Wala',
                    'Attock': 'Attock',
                    'Bahawalnagar': 'Bahawalnagar',
                    'Bahawalpur': 'Bahawalpur',
                    'Bhakkar': 'Bhakkar',
                    'Bhalwal': 'Bhalwal',
                    'Burewala': 'Burewala',
                    'Chakwal': 'Chakwal',
                    'Chiniot': 'Chiniot',
                    'Chishtian': 'Chishtian',
                    'Daska': 'Daska',
                    'Dera Ghazi Khan': 'Dera Ghazi Khan',
                    'Faisalabad': 'Faisalabad',
                    'Ferozwala': 'Ferozwala',
                    'Gojra': 'Gojra',
                    'Gujranwala': 'Gujranwala',
                    'Gujrat': 'Gujrat',
                    'Hafizabad': 'Hafizabad',
                    'Haroonabad': 'Haroonabad',
                    'Hasilpur': 'Hasilpur',
                    'Islamabad': 'Islamabad',
                    'Jaranwala': 'Jaranwala',
                    'Jatoi': 'Jatoi',
                    'Jhang': 'Jhang',
                    'Jhelum': 'Jhelum',
                    'Kamalia': 'Kamalia',
                    'Kamoke': 'Kamoke',
                    'Kasur': 'Kasur',
                    'Khanewal': 'Khanewal',
                    'Khanpur': 'Khanpur',
                    'Khushab': 'Khushab',
                    'Kot Abdul Malik': 'Kot Abdul Malik',
                    'Kot Addu': 'Kot Addu',
                    'Lahore': 'Lahore',
                    'Layyah': 'Layyah',
                    'Lodhran': 'Lodhran',
                    'Mandi Bahauddin': 'Mandi Bahauddin',
                    'Mianwali': 'Mianwali',
                    'Multan': 'Multan',
                    'Muridke': 'Muridke',
                    'Muzaffargarh': 'Muzaffargarh',
                    'Narowal': 'Narowal',
                    'Okara': 'Okara',
                    'Pakpattan': 'Pakpattan',
                    'Rahim Yar Khan': 'Rahim Yar Khan',
                    'Rawalpindi': 'Rawalpindi',
                    'Sadiqabad': 'Sadiqabad',
                    'Sahiwal': 'Sahiwal',
                    'Sambrial': 'Sambrial',
                    'Samundri': 'Samundri',
                    'Sargodha': 'Sargodha',
                    'Sialkot': 'Sialkot',
                    'Taxila': 'Taxila',
                    'Vehari': 'Vehari',
                    'Wah Cantonment': 'Wah Cantonment',
                    'Wazirabad': 'Wazirabad',
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'kpk') {
                $('#city').empty();

                let cities = {
                    'Abbotabad': 'Abbotabad',
                    'Charsadda': 'Charsadda',
                    'Dera Ismail Khan': 'Dera Ismail Khan',
                    'Kabal': 'Kabal',
                    'Kohat': 'Kohat',
                    'Mansehra': 'Mansehra',
                    'Mardan': 'Mardan',
                    'Mingora': 'Mingora',
                    'Peshawar': 'Peshawar',
                    'Swabi': 'Swabi',
                    'Wazirabad': 'Wazirabad',
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'balochistan') {

                $('#city').empty();

                let cities = {
                    'Chaman': 'Chaman',
                    'Gwadar': 'Gwadar',
                    'Hub': 'Hub',
                    'Khuzdar': 'Khuzdar',
                    'Quetta': 'Quetta',
                    'Turbat': 'Turbat',
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'gilgit_baltistan') {

                $('#city').empty();

                let cities = {
                    'Askole': 'Askole',
                    'Astore': 'Astore',
                    'Bunji': 'Bunji',
                    'Chillinji': 'Chillinji',
                    'Chiran': 'Chiran',
                    'Gakuch': 'Gakuch',
                    'Ghangche': 'Ghangche',
                    'Ghizer': 'Ghizer',
                    'Gilgit': 'Gilgit',
                    'Dayor': 'Dayor',
                    'Sultanabad': 'Sultanabad',
                    'Oshikhanda': 'Oshikhanda',
                    'Jalalabad': 'Jalalabad',
                    'Jutial Gilgit': 'Jutial Gilgit',
                    'Alyabad Hunza': 'Alyabad Hunza',
                    'Gorikot': 'Gorikot',
                    'Gulmit': 'Gulmit',
                    'Jaglot': 'Jaglot',
                    'Chalt_(Nagar)': 'Chalt (Nagar)',
                    'Thole_(Nagar)': 'Thole (Nagar)',
                    'Nasir Abad': 'Nasir Abad',
                    'Mayoon': 'Mayoon',
                    'Khana Abad': 'Khana Abad',
                    'Hussain Abad': 'Hussain Abad',
                    'Qasimabad_Masoot_(Nagar)': 'Qasimabad Masoot (Nagar)',
                    'Nagar Proper': 'Nagar Proper',
                    'Ghulmat_(Nagar)': 'Ghulmat (Nagar)',
                    'Karimabad_(Hunza)': 'Karimabad (Hunza)',
                    'Ishkoman': 'Ishkoman',
                    'Khaplu': 'Khaplu',
                    'Minimerg': 'Minimerg',
                    'Misgar': 'Misgar',
                    'Passu': 'Passu',
                    'Shimshal': 'Shimshal',
                    'Skardu': 'Skardu',
                    'Sust': 'Sust',
                    'Thowar': 'Thowar',
                    'Kharmang': 'Kharmang',
                    'Roundo': 'Roundo',
                    'Shigar Muhammad Amin': 'Shigar Muhammad Amin',
                    'Shigar Bunpa': 'Shigar Bunpa',
                    'Shigar Kiahong': 'Shigar Kiahong',
                    'Other': 'Other'

                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else {
                $('#city').empty();

                let cities = {
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            }
        });

    </script>

@endsection

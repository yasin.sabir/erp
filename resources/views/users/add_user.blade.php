@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Add User</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('users.list') }}"
                                                                       class="breadcrumb-link">All Users</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Add User</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Add User</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul style="margin: 0">
                                            @foreach($errors->all() as $err)
                                                <li>{{ $err }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="{{ route('user.create') }}" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Full Name</label>
                                                <input type="text" class="form-control" name="name"
                                                       value="{{old('name')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Phone #</label>
                                                <input type="text" class="form-control" value="{{old('phone')}}"
                                                       name="text" placeholder="Phone number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Email</label>
                                                <input type="email" class="form-control" name="email"
                                                       value="{{old('email')}}" placeholder="example@domain.com">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Password</label>
                                                <input type="password" class="form-control" name="password">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="col-form-label">Region</label>
                                                    <select class="form-control" id="region" name="region"
                                                            value="{{old('region')}}">
                                                        <option value="sindh" selected>Sindh</option>
                                                        <option value="punjab">Punjab</option>
                                                        <option value="kpk">KPK</option>
                                                        <option value="balochistan">Balochistan</option>
                                                        <option value="gilgit_baltistan">Gilgit Baltistan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">City</label>
                                                <select name="city" id="city" class="form-control"
                                                        value="{{old('city')}}">
                                                        <option class="option" value="Badin">Badin</option>
                                                        <option class="option" value="Dadu">Dadu</option>
                                                        <option class="option" value="Daharki">Daharki</option>
                                                        <option class="option" value="Ghotki">Ghotki</option>
                                                        <option class="option" value="Hyderabad">Hyderabad</option>
                                                        <option class="option" value="Jacobabad">Jacobabad</option>
                                                        <option class="option" value="Karachi">Karachi</option>
                                                        <option class="option" value="Kotri">Kotri</option>
                                                        <option class="option" value="Kamber">Kamber</option>
                                                        <option class="option" value="Kandhkot">Kandhkot</option>
                                                        <option class="option" value="Khairpur">Khairpur</option>
                                                        <option class="option" value="Larkana">Larkana</option>
                                                        <option class="option" value="Mirpur Khas">Mirpur Khas</option>
                                                        <option class="option" value="Mirpur Mathelo">Mirpur Mathelo</option>
                                                        <option class="option" value="Nawabshah">Nawabshah</option>
                                                        <option class="option" value="Shahdadkot">Shahdadkot</option>
                                                        <option class="option" value="Shikarpur">Shikarpur</option>
                                                        <option class="option" value="Sukkur">Sukkur</option>
                                                        <option class="option" value="Tando Adam">Tando Adam</option>
                                                        <option class="option" value="Tando Allahyar">Tando Allahyar</option>
                                                        <option class="option" value="Tando Muhammad Khan">Tando Muhammad Khan</option>
                                                        <option class="option" value="Umerkot">Umerkot</option>
                                                        <option class="option" value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">User Role</label>
                                                <select class="form-control" name="role" id="role"
                                                        value="{{old('role')}}">
                                                    <option value="admin">Admin</option>
                                                    <option value="distributor">Distributor</option>
                                                    <option value="warehouse_manager">Warehouse Manager</option>
                                                    <option value="client">Client</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="dis_row">
                                            <div class="form-group">
                                                <label class="col-form-label">Distributor</label>
                                                <select class="form-control" name="distributor"
                                                        value="{{old('distributor')}}">
                                                    <option value="">Choose the distributor</option>
                                                    @foreach($distributor as $dis)
                                                        <option value="{{$dis->id}}">{{$dis->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Address</label>
                                                <textarea class="form-control" name="address"
                                                          placeholder="1796  Ashcraft Court">{{old('address')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Additional Information (Optional)</label>
                                                <textarea class="form-control" name="additional_info"
                                                          placeholder="Enter additional info...">{{old('additional_info')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Create User">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $('#dis_row').hide();
        $('#role').change(function (e) {
            if ($(this).val() == 'client') {
                $('#dis_row').show();
            } else {
                $('#dis_row').hide();
            }
        });

        // $("#region option[value=kpk]")

        $('#region').change(function () {

            if ($("#region").val() === "sindh") {

                    $('#city').empty();

                    let cities = {
                        'Badin': 'Badin',
                        'Dadu': 'Dadu',
                        'Daharki': 'Daharki',
                        'Ghotki': 'Ghotki',
                        'Hyderabad': 'Hyderabad',
                        'Jacobabad': 'Jacobabad',
                        'Kamber': 'Kamber',
                        'Kandhkot': 'Kandhkot',
                        'Karachi': 'Karachi',
                        'Khairpur': 'Khairpur',
                        'Kotri': 'Kotri',
                        'Larkana': 'Larkana',
                        'Mirpur Khas': 'Mirpur Khas',
                        'Mirpur Mathelo': 'Mirpur Mathelo',
                        'Nawabshah': 'Nawabshah',
                        'Shahdadkot': 'Shahdadkot',
                        'Shikarpur': 'Shikarpur',
                        'Sukkur': 'Sukkur',
                        'Tando Adam': 'Tando Adam',
                        'Tando Allahyar': 'Tando Allahyar',
                        'Tando Muhammad Khan': 'Tando Muhammad Khan',
                        'Umerkot': 'Umerkot',
                        'Other' : 'Other'
                    };

                    for (let c in cities) {
                        $('#city').append($('<option>', {value: c, text: cities[c]}))
                    }

            } else if ($("#region").val() === "punjab") {

                    $('#city').empty();

                    let cities = {

                        'Ahmedpur East': 'Ahmedpur East',
                        'Arif Wala': 'Arif Wala',
                        'Attock': 'Attock',
                        'Bahawalnagar': 'Bahawalnagar',
                        'Bahawalpur': 'Bahawalpur',
                        'Bhakkar': 'Bhakkar',
                        'Bhalwal': 'Bhalwal',
                        'Burewala': 'Burewala',
                        'Chakwal': 'Chakwal',
                        'Chiniot': 'Chiniot',
                        'Chishtian': 'Chishtian',
                        'Daska': 'Daska',
                        'Dera Ghazi Khan': 'Dera Ghazi Khan',
                        'Faisalabad': 'Faisalabad',
                        'Ferozwala': 'Ferozwala',
                        'Gojra': 'Gojra',
                        'Gujranwala': 'Gujranwala',
                        'Gujrat': 'Gujrat',
                        'Hafizabad': 'Hafizabad',
                        'Haroonabad': 'Haroonabad',
                        'Hasilpur': 'Hasilpur',
                        'Islamabad': 'Islamabad',
                        'Jaranwala': 'Jaranwala',
                        'Jatoi': 'Jatoi',
                        'Jhang': 'Jhang',
                        'Jhelum': 'Jhelum',
                        'Kamalia': 'Kamalia',
                        'Kamoke': 'Kamoke',
                        'Kasur': 'Kasur',
                        'Khanewal': 'Khanewal',
                        'Khanpur': 'Khanpur',
                        'Khushab': 'Khushab',
                        'Kot Abdul Malik': 'Kot Abdul Malik',
                        'Kot Addu': 'Kot Addu',
                        'Lahore': 'Lahore',
                        'Layyah': 'Layyah',
                        'Lodhran': 'Lodhran',
                        'Mandi Bahauddin': 'Mandi Bahauddin',
                        'Mianwali': 'Mianwali',
                        'Multan': 'Multan',
                        'Muridke': 'Muridke',
                        'Muzaffargarh': 'Muzaffargarh',
                        'Narowal': 'Narowal',
                        'Okara': 'Okara',
                        'Pakpattan': 'Pakpattan',
                        'Rahim Yar Khan': 'Rahim Yar Khan',
                        'Rawalpindi': 'Rawalpindi',
                        'Sadiqabad': 'Sadiqabad',
                        'Sahiwal': 'Sahiwal',
                        'Sambrial': 'Sambrial',
                        'Samundri': 'Samundri',
                        'Sargodha': 'Sargodha',
                        'Sialkot': 'Sialkot',
                        'Taxila': 'Taxila',
                        'Vehari': 'Vehari',
                        'Wah Cantonment': 'Wah Cantonment',
                        'Wazirabad': 'Wazirabad',
                        'Other' : 'Other'
                    };

                    for (let c in cities) {
                        $('#city').append($('<option>', {value: c, text: cities[c]}))
                    }

            } else if ($("#region").val() === 'kpk') {
                    $('#city').empty();

                    let cities = {
                        'Abbotabad': 'Abbotabad',
                        'Charsadda': 'Charsadda',
                        'Dera Ismail Khan': 'Dera Ismail Khan',
                        'Kabal': 'Kabal',
                        'Kohat': 'Kohat',
                        'Mansehra': 'Mansehra',
                        'Mardan': 'Mardan',
                        'Mingora': 'Mingora',
                        'Peshawar': 'Peshawar',
                        'Swabi': 'Swabi',
                        'Wazirabad': 'Wazirabad',
                        'Other' : 'Other'
                    };

                    for (let c in cities) {
                        $('#city').append($('<option>', {value: c, text: cities[c]}))
                    }

            } else if ($("#region").val() === 'balochistan') {

                    $('#city').empty();

                    let cities = {
                        'Chaman': 'Chaman',
                        'Gwadar': 'Gwadar',
                        'Hub': 'Hub',
                        'Khuzdar': 'Khuzdar',
                        'Quetta': 'Quetta',
                        'Turbat': 'Turbat',
                        'Other' : 'Other'
                    };

                    for (let c in cities) {
                        $('#city').append($('<option>', {value: c, text: cities[c]}))
                    }

            } else if ($("#region").val() === 'gilgit_baltistan') {

                    $('#city').empty();

                    let cities = {
                        'Askole': 'Askole',
                        'Astore': 'Astore',
                        'Bunji': 'Bunji',
                        'Chillinji': 'Chillinji',
                        'Chiran': 'Chiran',
                        'Gakuch': 'Gakuch',
                        'Ghangche': 'Ghangche',
                        'Ghizer': 'Ghizer',
                        'Gilgit': 'Gilgit',
                        'Dayor': 'Dayor',
                        'Sultanabad': 'Sultanabad',
                        'Oshikhanda': 'Oshikhanda',
                        'Jalalabad': 'Jalalabad',
                        'Jutial Gilgit': 'Jutial Gilgit',
                        'Alyabad Hunza': 'Alyabad Hunza',
                        'Gorikot': 'Gorikot',
                        'Gulmit': 'Gulmit',
                        'Jaglot': 'Jaglot',
                        'Chalt_(Nagar)': 'Chalt (Nagar)',
                        'Thole_(Nagar)': 'Thole (Nagar)',
                        'Nasir Abad': 'Nasir Abad',
                        'Mayoon': 'Mayoon',
                        'Khana Abad': 'Khana Abad',
                        'Hussain Abad': 'Hussain Abad',
                        'Qasimabad_Masoot_(Nagar)': 'Qasimabad Masoot (Nagar)',
                        'Nagar Proper': 'Nagar Proper',
                        'Ghulmat_(Nagar)': 'Ghulmat (Nagar)',
                        'Karimabad_(Hunza)': 'Karimabad (Hunza)',
                        'Ishkoman': 'Ishkoman',
                        'Khaplu': 'Khaplu',
                        'Minimerg': 'Minimerg',
                        'Misgar': 'Misgar',
                        'Passu': 'Passu',
                        'Shimshal': 'Shimshal',
                        'Skardu': 'Skardu',
                        'Sust': 'Sust',
                        'Thowar': 'Thowar',
                        'Kharmang': 'Kharmang',
                        'Roundo': 'Roundo',
                        'Shigar Muhammad Amin': 'Shigar Muhammad Amin',
                        'Shigar Bunpa': 'Shigar Bunpa',
                        'Shigar Kiahong': 'Shigar Kiahong',
                        'Other' : 'Other'

                    };

                    for (let c in cities) {
                        $('#city').append($('<option>', {value: c, text: cities[c]}))
                    }

            } else {
                $('#city').empty();

                let cities = {
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            }
        });


    </script>
@endsection

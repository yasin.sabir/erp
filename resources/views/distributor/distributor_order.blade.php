@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Your Orders</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Order</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Your Orders</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <table id="empTable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>S#</th>
                                        <th>Date</th>
                                        <th>Order id</th>
                                        <th>User Name</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;

                                    ?>
                                    @foreach($orders as $order)
                                        <?php $i++ ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td>{{$order->created_at->format('d-m-Y')}}</td>
                                            <td>{{ $order->id }}</td>
                                            <td>{{$order->user_name}}</td>
                                            <td>{{$order->status}}</td>
                                            <td>
                                                <ul class="actions" style="margin: 5px -32px;">
                                                    <li><a href="{{ route('order.detail', ['id' => $order->id]) }}"><span><i class="fa fa-eye"></i></span></a></li>

                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $("document").ready(function () {

            {{--$("#delete-category").submit(function (e) {--}}
            {{--    e.preventDefault(e);--}}
            {{--});--}}

            {{--$("#delete_btn").submit(function (e) {--}}
            {{--    e.preventDefault(e);--}}
            {{--});--}}

            {{--$(document).on('click', '.delete_link', function () {--}}
            {{--    var id = $(this).attr("user_id");--}}

            {{--    var route = '{{ route('user.delete', ['id' => 'id']) }}';--}}
            {{--    route =  route.replace('id',id);--}}

            {{--    $("#deleteUserModal").modal('show');--}}
            {{--    $("#delete-modal-form").attr("action",route);--}}

            {{--    $('.modal_delete_link').on('click' , function (e) {--}}
            {{--        e.preventDefault();--}}
            {{--        // alert("ds");--}}
            {{--        $("#delete-modal-form").submit();--}}
            {{--    })--}}
            {{--});--}}

            $('#empTable').dataTable({
                "aoColumnDefs": [
                    {
                        "bSortable": false,
                        "aTargets": [ -1 ] // <-- gets last column and turns off sorting
                    }
                ]
            });

        });

    </script>
@endsection

@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Products</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">All Purchases</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Products</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">

                                @if(\Session::has('msg'))
                                    <div class="alert alert-success">
                                        {!! \Session::get('msg') !!}
                                    </div>
                                @endif

                                @if(\Session::has('error'))
                                    <div class="alert alert-danger">
                                        {!! \Session::get('error') !!}
                                    </div>
                                @endif



                                    <table id="empTable" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>S#</th>
                                        <th>File No.</th>
{{--                                        <th>Quantity</th>--}}
                                        <th>Date</th>
{{--                                        <th>Image</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($purchases as $key => $value)
                                        <tr>
                                            <td>{{ $key +1 }}</td>
                                            <td>{{ $value->batch_name}}</td>
                                            
{{--                                            <td>{{ $product->qty }}</td>--}}
                                            <td>{{ $value->date }}</td>
{{--                                            <td>--}}
{{--                                                <img src="{{ asset('storage/'. $product->image) }}"--}}
{{--                                                     class="img-responsive" width="100" height="100"/>--}}
{{--                                            </td>--}}
                                            <td>
                                                <ul class="actions" style="margin: 5px -35px;">
                                                    <li> <a href="{{ route('purchase.view', ['id' => $value->id]) }}"><span><i class="fa fa-eye"></i></span></a>
                                                    </li>
                                                    <li>
                                                        <button class="delete_btn delete_link" purchases_id="{{$value->id}}"> <span><i class="fa fa-trash"></i></span> </button>

                                    {{--                                                        <a href="{{ route('purchase.delete', ['id' => $value->id]) }}"><span><i--}}
{{--                                                                    class="fa fa-trash"></i></span></a>--}}


                                                    </li>


                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        {{--Delete Purchases Modal--}}
                        {{--data-toggle="modal" data-target="#basicModal"--}}
                        <div class="modal fade" id="deletePurchasesModal" tabindex="-1" role="dialog"
                             aria-labelledby="deletePurchasesModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header custom-delete-model">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h5 class="modal-title" id="myModalLabel"><i class="fa fa-bullhorn"></i> Alert
                                        </h5>
                                    </div>
                                    {{--                                    {{ route('user.delete', ['id' => $user->id]) }}--}}
                                    <form method="post" id="delete-modal-form" action="">
                                        <div class="modal-body">
                                            <label class="">You want to sure delete this Purchase ?</label>
                                            <input type="hidden" name="_method" value="POST">
                                            {{csrf_field()}}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger modal_delete_link" data-dismiss="modal">Yes </button>
                                            {{--                                            <a type="submit" class="btn btn-danger modal_delete_link" href="">Yes</a>--}}
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $("document").ready(function () {

            $("#delete-category").submit(function (e) {
                e.preventDefault(e);
            });

            $("#delete_btn").submit(function (e) {
                e.preventDefault(e);
            });

            $(document).on('click', '.delete_link', function () {
                var id = $(this).attr("purchases_id");

                var route = '{{ route('purchase.delete', ['id' => 'id']) }}';
                route =  route.replace('id',id);

                $("#deletePurchasesModal").modal('show');
                $("#delete-modal-form").attr("action",route);

                $('.modal_delete_link').on('click' , function (e) {
                    e.preventDefault();
                    // alert("ds");
                    $("#delete-modal-form").submit();
                })
            });

            $('#empTable').dataTable({
                "aoColumnDefs": [
                    {
                        "bSortable": false,
                        "aTargets": [ -1 ] // <-- gets last column and turns off sorting
                    }
                ]
            });

        });

    </script>
@endsection

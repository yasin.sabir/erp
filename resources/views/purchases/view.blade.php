@extends('layouts.admin')
@section('content')
    <style>
        .card h3 {
            margin-left: 13%;
        }
    </style>
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Purchase Report</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item" aria-current="page">Purchase</li>
                                        <li class="breadcrumb-item active" aria-current="page">Purchase Report
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title center-block" style="text-align: center;">ERP</h3>
                            <div id="report-title">
                                <h3 class="section-title center-block" style="text-align: center;">
                                    Purchase Report
                                </h3>
                                <br>
                                <h3 style="text-align: center;">File No:{{$purchase->batch_name}}</h3>
                                <h3 style="text-align: center;">Date:{{$purchase->date}}</h3>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <table id="example" class="table table-striped table-bordered" style="width:100%;">
                                    <thead>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Color Code</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchase->ProductColorPurchases as $key => $value)
                                        {{--{{dd($report)}}--}}
                                        <tr>
                                            <td>{{$key +1}}</td>
                                            <td>{{$value->color_code}}</td>
                                            <td>{{$value->product_name}}</td>
                                            <td>{{$value->quantity}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var top_content = document.getElementById("report-title");
        $(document).ready(function () {
            // $('#example').DataTable( {
            //     dom: 'Blfrtip',
            //     buttons: [
            //         'print'
            //     ]
            // } );

            $('#example').DataTable({
                dom: 'Blfrtip',
                responsive: true,
                buttons: [
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                            // .prepend(
                            //     '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            // );

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit')
                                .before($(top_content).clone());
                        }
                    }
                ]
            });

        });

    </script>
@endsection

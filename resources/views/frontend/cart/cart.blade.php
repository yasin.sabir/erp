@extends('frontend.index')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
            @if(Session::has('success'))
                <p class="alert alert-info">{{ Session::get('success') }}</p>
            @endif
            <form action="{{route('cart.update')}}" method="post">
                <div class="table-responsive cart_info">
                    <?php $cart_item = Session::get('cart');
                    ?>

                    <table class="table table-condensed">
                        <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description">Name</td>
                            <td class="quantity">Color Name</td>
                            <td class="price">Color Code</td>
                            <td class="quantity">Quantity</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($cart_item))

                            @csrf
                            @foreach($cart_item as $item)
                                <tr>
                                    <td class="cart_product">
                                        <a href=""><img src="{{ asset('storage/'.$item['image'])}}" width="80"
                                                        height="80" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4>{{$item['product_name']}}</h4>
                                        {{--                            <input type="hidden" name="product_id[]" value="{{$item['id']}}">--}}
                                    </td>
                                    <td class="cart_product">
                                        <h4>{{$item['color_name']}}</h4>
                                    </td>
                                    <td class="">
                                        <p>{{$item['color_code']}}</p>
                                        <input type="hidden" name="product_color_id[]"
                                               value="{{$item['product_color_id']}}">
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <a class="cart_quantity_up" href=""> + </a>
                                            <input class="cart_quantity_input" type="text" name="quantity[]"
                                                   value="{{$item['qty']}}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down" href=""> - </a>
                                        </div>
                                    </td>

                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete"
                                           href="{{route('cart.remove',['id' => $item['product_color_id']])}}"><i
                                                class="fa fa-times"></i></a>
                                    </td>
                                </tr>

                            @endforeach

                        @else
                            <tr>
                                <td>There is No Item Cart</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>


                </div>
                <div class="row" style="margin: 20px">
                    @if(!empty($cart_item))
                    <button type="submit" class="btn btn-default check_out pull-right">Update</button>
                    @else
                        <button class="btn btn-default check_out pull-right disabled" style="color: #FE980F;">Update</button>
                    @endif
                    <a class="btn btn-default check_out pull-right" href="{{route('order.add')}}">Check Out</a>
                </div>
            </form>

        </div>
    </section> <!--/#cart_items-->
@endsection

<div class="mainmenu">
    <ul class="nav navbar-nav collapse navbar-collapse nav-ul pull-left">
        @role('distributor')
        {{--	<li><a href="{{route('order.list')}}"><i class="fa fa-shopping-cart"></i> Dashboard</a></li>--}}
        <li class="nav-li"><a href="{{route('order.list.distributor')}}" class="nav-a">Dashboard</a></li>
        @endrole
        <li class="nav-li"><a href="{{route('clients')}}" class="nav-a">Shop</a></li>
        @role('client')
        <li class="nav-li"><a href="{{route('client.orders','processing')}}" class="nav-a">Orders</a></li>
        @endrole
        @role('distributor')
        <li class="nav-li"><a href="{{route('order.list.distributor')}}" class="nav-a">Orders</a></li>
        @endrole
        @role('admin')
        <li class="nav-li"><a href="{{route('order.list')}}" class="nav-a">Orders</a></li>
        @endrole
        <li class="nav-li"><a href="{{route('cart')}}" class="nav-a">Cart</a></li>
    </ul>

</div>

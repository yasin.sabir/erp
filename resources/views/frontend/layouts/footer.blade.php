<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container footer-container-1">
            <div class="row">
                <div class="col-sm-4">
                    <div class="companyinfo">
                        <h2><span>e</span>RP</h2>
                        <p class="companyinfo-p-content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat ipsum elit, sit amet tincidunt justo fringilla nec. Nunc erat turpis, ullamcorper eu mauris ac, elementum mollis massa. Aliquam dapibus fringilla vulputate. Donec consequat magna vel sem gravida, eget tincidunt tellus feugiat.
                        </p>
                    </div>
                </div>

                <div class="col-sm-2">

                </div>

                <div class="col-sm-3">
                    <div class="companyinfo">
                        <h5 class="text-uppercase">Quick Links</h5>
                        <ul class="footer-list-unstyled">
                            <li class="footer-li">
                                <a href="{{route('clients')}}" class="footer-a">Shop</a>
                            </li>
                            <li class="footer-li">
                                <a href="{{route('client.orders','processing')}}" class="footer-a">Orders</a>
                            </li>
                            <li class="footer-li">
                                <a href="{{route('cart')}}" class="footer-a">Cart</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="companyinfo">
                        <h5 class="text-uppercase">Account</h5>
                        <ul class="footer-list-unstyled">
                            <li class="footer-li">
                                <a href="{{ url('/logout') }}" class="footer-a">Logout</a>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>

{{--        <div class="footer-widget">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="copyright-p-content">Copyright © 2019 ERP. All rights reserved.</p>
                    </div>
                    {{--					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">HZtech</a></span></p>--}}
                </div>
            </div>
        </div>

</footer><!--/Footer-->

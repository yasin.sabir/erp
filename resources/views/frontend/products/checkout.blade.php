@extends('frontend.index')
@section('content')
    <section id="cart_items">
        <div class="container">
{{--            <div class="breadcrumbs">--}}
{{--                <ol class="breadcrumb">--}}
{{--                    <li><a href="#">Home</a></li>--}}
{{--                    <li class="active">Check out</li>--}}
{{--                </ol>--}}
{{--            </div><!--/breadcrums-->--}}


            <form action="{{route('order.create') }}" method="POST">
                @csrf
            {{--<div class="shopper-informations">--}}
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <div class="order-message">
                            <p>Billing Order</p>
                            <textarea name="billing"  placeholder="Billing Address" required="true"></textarea>

                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="order-message">
                            <p>Shipping Order</p>
                            <textarea name="shipping"  placeholder="Shipping Address" required="true"></textarea>

                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="order-message">
                            <p>Additional Information</p>
                            <textarea name="additional_notes"  placeholder="Additional Information"></textarea>

                        </div>
                    </div>
                </div>
            {{--</div>--}}
            {{--<div class="review-payment">--}}
                {{--<h2>Review & Payment</h2>--}}
            {{--</div>--}}
                @if(Session::has('success'))
                    <p class="alert alert-info">{{ Session::get('success') }}</p>
                @endif
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description">Name</td>
                        <td class="quantity">Color Name</td>
                        <td class="price">Color Code</td>
                        <td class="quantity">Quantity</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $cart_item = Session::get('cart');
                    ?>
                    @if(!empty($cart_item))

                        @csrf
                        @foreach($cart_item as $item)
                            <tr>
                                <td class="cart_product">
                                    <a href=""><img src="{{ asset('storage/'.$item['image'])}}" width="80"
                                                    height="80" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4>{{$item['product_name']}}</h4>
                                    {{--                            <input type="hidden" name="product_id[]" value="{{$item['id']}}">--}}
                                </td>
                                <td class="cart_product">
                                    <h4>{{$item['color_name']}}</h4>
                                </td>
                                <td class="">
                                    <p>{{$item['color_code']}}</p>

                                </td>
                                <td class="cart_quantity">
                                    <p>{{$item['qty']}}</p>
                                </td>

                                <td class="cart_delete">
                                    <a class="cart_quantity_delete"
                                       href="{{route('cart.remove',['id' => $item['product_color_id']])}}"><i
                                            class="fa fa-times"></i></a>
                                </td>
                            </tr>

                        @endforeach

                    @else
                        <tr>
                            <td>There is No Item Cart</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-12">
                        @if(!empty($cart_item))
                        <button type="submit" class="btn btn-primary" style="float: right" href="{{route('order.create')}}">Get Quotes</button>
                        @else
                            <button type="submit" class="btn btn-primary disabled" style="float: right" href="{{route('order.create')}}">Get Quotes</button>

                        @endif
                    </div>
                </div>
            {{--</div>--}}
            </form>
        </div>

    </section> <!--/#cart_items-->
    @endsection
    @section('script')
        <script>
            $('#update').click(function (e) {
                e.preventDefault();

            });
        </script>
    @endsection

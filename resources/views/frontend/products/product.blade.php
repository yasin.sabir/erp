@extends('frontend.index')
@section('content')
<section>
		<div class="container">
			<div class="row">
                @if(Session::has('success'))
                    <p class="alert alert-info">{{ Session::get('success') }}</p>
                @endif
				
				
				<div class="col-sm-12 padding-right">
					<div class="features_ites"><!--features_items-->
						<h2 class="title text-center">Shop</h2>
                        <ul class="cd-gallery">

						 {{--{{--}}
						{{--dd(count($products))--}}
						{{--}}--}}
						@if(count($products) > 0)
						@foreach($products as $product)
                                    <li>
                                        <a href="{{ route('productdetails',$product->id) }}">
                                            <ul class="cd-item-wrapper">
                                                @foreach($product->color_image as $key=>$image)
                                                <li @if($key == 0) class="selected" @else class="move-right" @endif>
                                                    <img src="{{ asset('storage/'.$image ) }}" alt="Preview image">
                                                </li>
                                                @endforeach

                                            </ul> <!-- cd-item-wrapper -->
                                        </a>

                                        <div class="cd-item-info">
                                            <b><a href="#0">{{$product->name }}</a></b>

{{--                                            <em class="cd-price">$26</em>--}}
                                        </div> <!-- cd-item-info -->
                                    </li>
{{--						<div class="col-sm-4">--}}
{{--							<div class="product-image-wrapper">--}}
{{--								<div class="single-products">--}}
{{--									<div class="productinfo text-center">--}}
{{--										<img src="{{ asset('storage/'.$product->image) }}" alt="" />--}}
{{--										<h2>{{$product->name }}</h2>--}}
{{--										--}}
{{--										<a href="{{ route('productdetails',$product->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--									</div>--}}
{{--									<div class="product-overlay">--}}
{{--										<div class="overlay-content">--}}
{{--											<h2>{{$product->name }}</h2>--}}
{{--											--}}
{{--											<a href="{{ route('productdetails',$product->id) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--										</div>--}}
{{--									</div>--}}
{{--								</div>--}}
{{--								--}}{{-- <div class="choose">--}}
{{--									<ul class="nav nav-pills nav-justified">--}}
{{--										<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>--}}
{{--										<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>--}}
{{--									</ul>--}}
{{--								</div> --}}
{{--							</div>--}}
{{--						</div>--}}
						@endforeach
						@else
							<div class="col-sm-12" style="margin-bottom: 10px">
								<h4>No Product Available</h4>
							</div>
						@endif
						
						
						{{-- <ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul> --}}
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
@endsection

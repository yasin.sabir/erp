@extends('frontend.index')
@section('content')
    <section id="cart_items">
        <div class="container">
{{--            <div class="breadcrumbs">--}}
{{--                <ol class="breadcrumb">--}}
{{--                    <li><a href="#">Home</a></li>--}}
{{--                    <li class="active">Orders</li>--}}
{{--                </ol>--}}
{{--            </div><!--/breadcrums-->--}}
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="section-block" id="basicform">
{{--                        <h3 class="section-title">All Categories</h3>--}}
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-md-3">

                                <h5>Order Status</h5>
                                <select name="status" id="status">
                                    <option value="processing"  @if(Request::is('client/order/report/processing')) selected="selected" @endif>Processing</option>
                                    <option value="partial_approved" @if(Request::is('client/order/report/partial_approved')) selected="selected" @endif >Partial Approved</option>
                                    <option value="completed" @if(Request::is('client/order/report/completed')) selected="selected" @endif>Completed</option>
                                    <option value="approved" @if(Request::is('client/order/report/approved')) selected="selected" @endif>Approved</option>
                                    <option value="declined" @if(Request::is('client/order/report/declined')) selected="selected" @endif>Declined</option>

                                </select>

                            </div>
                            <table id="categories" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>S#</th>
                                    <th>Order id</th>
{{--                                    <th>Product Name</th>--}}
{{--                                    <th>Product Color</th>--}}
                                    <th>Status</th>
                                    <th>Actions</th>
                                    <th>Reports</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 0;

                                ?>
                                @foreach($orders as $order)

                                    <?php $i++ ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td>{{ $order->id }}</td>
                                        <td>
                                            {{ $order->status }}
                                        </td>
                                        <td>
                                            <ul class="actions">
                                                <li><a href="{{ route('client.order.details', ['id' => $order->id]) }}"><span><i class="fa fa-eye"></i></span></a></li>

                                            </ul>
                                        </td>
                                        <td>

                                            <ul class="actions">
                                                @foreach($order->challans as $challan)
                                                <li style="width: auto;display: inline-table;"><a href="{{ route('client.order.ChallanDetails', ['id' => $challan->id]) }}"><span><i class="fa fa-eye"></i></span></a></li>
                                                @endforeach
                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section> <!--/#cart_items-->
    @endsection
    @section('script')
        <script type="text/javascript">
            $('#status').change(function (e) {
                var val = $(this).val();
                var url = '{{ route("client.orders", ":slug") }}';
                url = url.replace(':slug', val);
                window.location.href = url;
            });
        </script>
    @endsection

@extends('frontend.index')
@section('content')
    <style>
        h4{
            font-size: 14px;
            margin-bottom: 3px;
        }
        p{
            margin: 3px;
        }
        .card{
            background: #ffffff;
            padding: 10px;
        }
        table{
            border: 2px solid #000a00;
        }
        table thead tr th{
            font-size: 14px;
            font-weight: bolder;
            /*border-bottom: 2px solid #000a00;*/
            text-align: center;
            padding: 10px;
            /*border-left: 2px solid #000a00;*/
        }
        table tbody tr{
            padding: 5px;
        }
        table tbody tr td{
            text-align: center;
            font-size: 14px;
            font-weight: normal;
            border-bottom: 1px solid #000a00;
            padding: 5px;
            /*border-left: 2px solid #000a00;*/
        }
        .report .row{
            margin-bottom: 10px;
        }
        .report .row div h2{
            text-transform: uppercase;
            font-weight: bold;
        }
        .report .row div p{
            font-size: 14px;
        }
    </style>
    <div class="container">
        <div class="row card">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="report">
                <div class="section-block" id="basicform">
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-md-12">
                            <h1 class="section-title center-block" style="text-align: center;font-size: 18px;font-weight: bolder;text-transform: uppercase;">Delivery
                                Challan</h1>
                        </div>

                    </div>
                    <div class="report">
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Challan No.</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['challan_no']}}</p>
                            </div>
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Date:</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['date']}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Party Name</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['party_name']}}</p>
                            </div>
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Order No.</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['order_no']}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>City</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['city']}}</p>
                            </div>
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Distributor Name</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['distributor']}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Vehicle No.</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['vehicle_no']}}</p>
                            </div>
                            <div class="col-md-3">

                            </div>

                            <div class="col-md-3">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Driver Name</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['driver_name']}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Driver NIC</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['driver_cnic']}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h2 style="font-size: 13px;"><strong>Driver Number</strong></h2>
                            </div>
                            <div class="col-md-3">
                                <p>{{$data['driver_number']}}</p>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <table id="product" class="display" style="width:100%;border:1px solid black">
                                    <thead style="border-bottom: 1px solid black;">
                                    <tr>
                                        <th style="font-size: 14px ;font-weight: bold; border-right: 1px solid black; ">Product Name</th>
                                        <th style="font-size: 14px ;font-weight: bold; border-right: 1px solid black; ">Product code</th>
                                        <th style="font-size: 14px ;font-weight: bold; border-right: 1px solid black; ">Quantity</th>
                                        <th style="font-size: 14px ;font-weight: bold; border-right: 1px solid black; ">Quantity Delivered</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['products'] as $product)
                                        <tr>
                                            <td style="border-right: 1px solid black;">{{$product['product_name']}}</td>
                                            <td style="border-right: 1px solid black;">{{$product['product_code']}}</td>
                                            <td style="border-right: 1px solid black;">{{$product['quantity']}}</td>
                                            <td style="border-right: 1px solid black;">{{$product['quantity_delivered']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p style="font-size: 14px;font-weight: bold;text-align: center"><strong>Total:</strong></p>
                                    </div>
                                    <div class="col-md-4">
                                        <p style="font-size: 14px;text-align:center;margin-right: 11px;">{{$data['total']}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p style="margin-left: 65px;text-align: center;font-size: 14px">{{$data['total_delivered']}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@extends('frontend.index')
@section('content')
    <style>
        h4{
            font-size: 14px;
            margin-bottom: 3px;
        }
        p{
            margin: 3px;
        }
        .card{
            background: #ffffff;
            padding: 10px;
        }
        table{
            border: 2px solid #000a00;
        }
        table thead tr td{
            font-size: 14px;
            font-weight: bolder;
            border-bottom: 2px solid #000a00;
            border-left: 2px solid #000a00;
        }
        table tbody tr td{
            font-size: 14px;
            font-weight: normal;
            /*border-bottom: 2px solid #000a00;*/
            border-left: 2px solid #000a00;
        }
    </style>
    <div class="container">
        <div class="card">
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="margin: 0">
                            @foreach($errors->all() as $err)
                                <li>{{ $err }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--<form action="{{ route('category.update', ['id' => $category->id]) }}" method="POST">--}}
                <div class="form-group">
                    <h4>Order Id</h4>
                    <p>{{$order['id']}}</p>
                </div>

                <div class="form-group">
                    <h4>Billing Address</h4>
                    <p>{{$order['billing']}}</p>
                </div>
                <div class="form-group">
                    <h4>Shipping Address</h4>
                    <p>{{$order['shipping']}}</p>
                </div>
                <div class="form-group">
                    <h4>Order Status</h4>
                    <p>
                        {{$order['status']}}
                    </p>
                </div>

                <div class="table-responsive cart_info">
                    <table class="table table-condensed" cellspacing="0">
                        <thead>
                        <tr class="cart_menu">
                            <td class="description">Name</td>
                            <td class="price">Color</td>
                            <td class="quantity">Quantity</td>
                            {{--<td class="total">Total</td>--}}

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order['products'] as $product)
                        <tr>

                                {{--                        @dd($product);--}}
                                <td class="cart_description">
                                    <h4>{{$product['product_name']}}</h4>
                                </td>
                                <td class="cart_price">
                                    <p>{{$product['product_color']}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        {{--<a class="cart_quantity_up" href=""> + </a>--}}
                                        <p>{{$product['qty']}}</p>
                                        {{--<a class="cart_quantity_down" href=""> - </a>--}}
                                    </div>
                                </td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>


        </div>
    </div>
@endsection

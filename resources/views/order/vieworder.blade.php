@extends('layouts.admin')
@section('content')
    <style>
        table {
            border: 2px solid #000a00 !important;;
            margin-bottom: 20px !important;
        }

        table thead tr td {
            font-size: 14px !important;
            font-weight: bolder !important;
            border-bottom: 2px solid #000a00 !important;;
            border-left: 2px solid #000a00 !important;;
        }

        table tbody tr td {
            font-size: 14px !important;;
            font-weight: normal !important;;
            /*border-bottom: 2px solid #000a00;*/
            border-left: 2px solid #000a00 !important;;
        }
    </style>
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">View Order</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Orders</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">View Orders</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="{{ route('order.edit',['id' => $order['id']]) }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="section-block" id="basicform">
                                <h3 class="section-title">View Order</h3>
                            </div>

                            @if(\Session::has('error'))
                                <div class="alert alert-danger">
                                    {!! \Session::get('error') !!}
                                </div>
                            @endif

                            @if(\Session::has('msg'))
                                <div class="alert alert-success">
                                    {!! \Session::get('msg') !!}
                                </div>
                            @endif


                            <div class="card">
                                <div class="card-body">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul style="margin: 0">
                                                @foreach($errors->all() as $err)
                                                    <li>{{ $err }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {{--<form action="{{ route('category.update', ['id' => $category->id]) }}" method="POST">--}}

                                    @if(Auth::user()->getRoleNames()[0] == 'admin')
                                        <div class="form-group">
                                            <label class="col-form-label">Order Id</label>
                                            <input name="order_no" type="text" class="form-control"
                                                   value="{{$order['order_no']}}" style="width: 30%;" required3>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="col-form-label">Client Name</label>
                                        <p>{{$user->name}}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Phone</label>
                                        <p>{{$user->phone}}</p>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">City</label>
                                        <p>{{$user->city}}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Billing Address</label>
                                        <p>{{$order['billing']}}</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Shipping Address</label>
                                        <p>{{$order['shipping']}}</p>
                                    </div>
                                    @if(Auth::user()->getRoleNames()[0] == 'admin')
                                        <div class="form-group">
                                            <label class="col-form-label">Order Status</label>
                                            <select name="status">
                                                <option value="processing"
                                                        @if ($order['status'] == 'processing') selected="selected" @endif>
                                                    Processing
                                                </option>
                                                <option value="approved"
                                                        @if ($order['status'] == 'approved') selected="selected" @endif>
                                                    Approved
                                                </option>
                                                <option value="completed"
                                                        @if ($order['status'] == 'completed') selected="selected" @endif>
                                                    Completed
                                                </option>
                                                <option value="declined"
                                                        @if ($order['status'] == 'declined') selected="selected" @endif>
                                                    Declined
                                                </option>
                                                <option value="balance"
                                                        @if ($order['status'] == 'balance') selected="selected" @endif>
                                                    Balance
                                                </option>
                                            </select>
                                            <input type="hidden" name="order_id" value="{{$order['id']}}">
                                        </div>
                                    @endif

                                    <div class="table-responsive cart_info">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr class="cart_menu">
                                                <td>Image</td>
                                                <td class="description">Name</td>
                                                <td class="price">Color</td>
                                                <td class="quantity">Quantity</td>
                                                @if($user->getRoleNames()[0] == 'client')
                                                    <td class="quantity">Approved By Distributor</td>
                                                @endif
                                                @if(Auth::user()->getRoleNames()[0] != 'distributor')
                                                    <td class="quantity">Quantity Pending</td>
                                                    <td class="quantity">Quantity Delivered</td>
                                                @endif
                                                {{--<td class="total">Total</td>--}}
                                                {{--    <td></td>--}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order['products'] as $product)
                                                <tr>
                                                    <td class="cart_description">
                                                        <img src="{{asset('storage/'.$product['image'])}}" width="auto"
                                                             height="100" class="product_img" data-toggle="modal"
                                                             data-target="#myModal">
                                                    </td>
                                                    <td class="cart_description">
                                                        <h4>{{$product['product_name']}}</h4>
                                                    </td>
                                                    <td class="cart_price">
                                                        <select class="form-control color" name='color[]'
                                                                data-value="{{$product['product_color_id']}}"
                                                                data-product-id="{{ $product['product_id']}}" required>
                                                            <option val="">Please choose</option>
                                                        </select>
                                                    </td>
                                                    <td class="cart_quantity">
                                                        <input type="number" class="form-control"
                                                               value="{{$product['qty']}}" name="qty[]"
                                                               style="width: 50%" disabled/>
                                                        <input type="hidden" value="{{$product['qty']}}"
                                                               name="actual_qty[]">
                                                        <input type="hidden"
                                                               value="{{$product['order_product_color_id']}}"
                                                               name="order_product_color_id[]">
                                                    </td>
                                                    @if($user->getRoleNames()[0] == 'client')
                                                        @if(!is_null($product['qty_approved']) && Auth::user()->getRoleNames()[0] != 'distributor' && Auth::user()->getRoleNames()[0] != 'admin')
                                                            <td class="cart_quantity">
                                                                <input type="number" class="form-control"
                                                                       value="{{ $product['qty_approved'] }}"
                                                                       name="qty_approved_distributor[]"
                                                                       style="width: 50%" disabled/>
                                                            </td>
                                                        @else
                                                            <td class="cart_quantity">
                                                                <input type="number" class="form-control"
                                                                       value="{{ $product['qty_approved'] }}"
                                                                       name="qty_approved_distributor[]"
                                                                       style="width: 50%"/>
                                                            </td>
                                                        @endif
                                                    @endif
                                                    @if(Auth::user()->getRoleNames()[0] != 'admin' )

                                                        @if(Auth::user()->getRoleNames()[0] != 'distributor')
                                                            <td class="cart_quantity">
                                                                <input type="number" class="form-control"
                                                                       value="{{$product['qty_pending']}}"
                                                                       name="qty_pending[]" style="width: 50%"
                                                                       disabled/>
                                                            </td>
                                                            <td class="cart_quantity">
                                                                <input type="number" class="form-control"
                                                                       value="{{$product['qty_delivered']}}"
                                                                       name="qty_delivered[]" style="width: 50%"
                                                                       disabled/>
                                                            </td>
                                                        @endif

                                                    @else
                                                        <td class="cart_quantity">
                                                            <input type="number" class="form-control"
                                                                   value="{{$product['qty_pending']}}"
                                                                   name="qty_pending[]" style="width: 50%"/>
                                                        </td>
                                                        <td class="cart_quantity">
                                                            <input type="number" class="form-control"
                                                                   value="{{$product['qty_delivered']}}"
                                                                   name="qty_delivered[]" style="width: 50%"/>
                                                        </td>
                                                    @endif


                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Edit Status">
                                    </div>
                                    <!-- The Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>

                                                </div>
                                                <div class="modal-body">
                                                    <img id="img01" src="{{asset('storage/'.$product['image'])}}"
                                                         width="100%" height="auto">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            $('.color').each(function () {
                var $select = $(this);
                var id = $select.data('product-id');
                var product_color_id = $select.data('value');
                var url = '{{route('report.getColorCode',':id')}}';
                var url = url.replace(':id', id);
                // var url = url.replace(':id',id);
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: url,
                    success: function (data) {

                        $.each(data, function (i, item) {
                            var selected = '';
                            if (item.id == product_color_id) {
                                selected = 'selected';
                            } else {
                                selected = 'false'
                            }
                            $select.append('<option value=' + item.id + ' ' + selected + ' >' + item.color_code + '</option>');
                        });

                    },
                    error: function () {
                        console.log(data);
                    }
                });
            });
            $("#myModal").modal('hide');


            $('.product_img').click(function () {
                $('#myModal').modal("show");
                $("#img01").attr('src', this.src);
            });
            // captionText.innerHTML = this.alt;


// Get the <span> element that closes the modal
            var span = $(".close")[0];

// When the user clicks on <span> (x), close the modal
            span.click(function () {
                $('#myModal').modal("show");
            });
        });




    </script>
@endsection

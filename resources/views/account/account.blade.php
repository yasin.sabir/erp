@extends('layouts.admin')
@section('content')
    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-12">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Account</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Account</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="section-block" id="basicform">
                            <h3 class="section-title">Account</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul style="margin: 0">
                                            @foreach($errors->all() as $err)
                                                <li>{{ $err }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                    @if(\Session::has('msg'))
                                        <div class="alert alert-success">
                                            {!! \Session::get('msg') !!}
                                        </div>
                                    @endif

                                    @if(\Session::has('error'))
                                        <div class="alert alert-danger">
                                            {!! \Session::get('error') !!}
                                        </div>
                                    @endif


                                <form action="{{ route('account.update', ['id' => $session->id]) }}" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Full Name</label>
                                                <input type="text" class="form-control" name="name"
                                                       value="{{ $session->name }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Email</label>
                                                <input type="text" class="form-control" name="email"
                                                       value="{{ $session->email }}">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Password</label>
                                                <input type="password" class="form-control" name="password"
                                                       value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Confirm Password</label>
                                                <input type="password" class="form-control" name="confirm_password" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">User Role</label>
                                                <select class="form-control" name="role">
                                                    <option
                                                        <?= ($session->role_name == 'admin') ? 'selected' : ''?> value="admin">
                                                        Admin
                                                    </option>
                                                    <option
                                                        <?= ($session->role_name == 'distributor') ? 'selected' : ''?> value="distributor">
                                                        Distributor
                                                    </option>
                                                    <option
                                                        <?= ($session->role_name == 'warehouse_manager') ? 'selected' : ''?> value="warehouse_manager">
                                                        Warehouse Manager
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Phone #</label>
                                                <input type="text" class="form-control" name="phone"
                                                       value="{{ $session->get_meta('phone') }}">
                                            </div>
                                        </div>

{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label class="col-form-label">Country</label>--}}
{{--                                                <input type="text" class="form-control" name="country"--}}
{{--                                                       value="{{ $session->get_meta('country') }}">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Region</label>
                                                <select class="form-control" id="region" name="region">
                                                    <option value="sindh"
                                                            @if($session->get_meta('region') == 'sindh') selected @endif>
                                                        Sindh
                                                    </option>
                                                    <option value="punjab"
                                                            @if($session->get_meta('region') == 'punjab') selected @endif>
                                                        Punjab
                                                    </option>
                                                    <option value="kpk"
                                                            @if($session->get_meta('region') == 'kpk') selected @endif>KPK
                                                    </option>
                                                    <option value="balochistan"
                                                            @if($session->get_meta('region') == 'balochistan') selected @endif>
                                                        Balochistan
                                                    </option>
                                                    <option value="gilgit_baltistan"
                                                            @if($session->get_meta('region') == 'gilgit_baltistan') selected @endif>
                                                        Gilgit Baltistan
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">City</label>
                                                <select name="city" id="city" class="form-control">
                                                    <option class="option" value="Abbottabad" @if($session->get_meta('city') == 'Abbottabad') selected @endif>Abbottabad</option>
                                                    <option class="option" value="Adezai" @if($session->get_meta('city') == 'Adezai') selected @endif>Adezai</option>
                                                    <option class="option" value="Ali Bandar" @if($session->get_meta('city') == 'Ali Bandar') selected @endif>Ali Bandar</option>
                                                    <option class="option" value="Amir Chah" @if($session->get_meta('city') == 'Amir Chah') selected @endif>Amir Chah</option>
                                                    <option class="option" value="Attock" @if($session->get_meta('city') == 'Attock') selected @endif>Attock</option>
                                                    <option class="option" value="Ayubia" @if($session->get_meta('city') == 'Ayubia') selected @endif>Ayubia</option>
                                                    <option class="option" value="Bahawalpur" @if($session->get_meta('city') == 'Bahawalpur') selected @endif>Bahawalpur</option>
                                                    <option class="option" value="Baden" @if($session->get_meta('city') == 'Baden') selected @endif>Baden</option>
                                                    <option class="option" value="Bagh" @if($session->get_meta('city') == 'Bagh') selected @endif>Bagh</option>
                                                    <option class="option" value="Bahawalnagar" @if($session->get_meta('city') == 'Bahawalnagar') selected @endif>Bahawalnagar</option>
                                                    <option class="option" value="Burewala" @if($session->get_meta('city') == 'Burewala') selected @endif>Burewala</option>
                                                    <option class="option" value="Banda Daud Shah" @if($session->get_meta('city') == 'Banda Daud Shah') selected @endif>Banda Daud Shah</option>
                                                    <option class="option" value="Bannu district|Bannu" @if($session->get_meta('city') == 'Bannu district|Bannu') selected @endif>Bannu</option>
                                                    <option class="option" value="Batagram" @if($session->get_meta('city') == 'Batagram') selected @endif>Batagram</option>
                                                    <option class="option" value="Bazdar" @if($session->get_meta('city') == 'Bazdar') selected @endif>Bazdar</option>
                                                    <option class="option" value="Bela" @if($session->get_meta('city') == 'Bela') selected @endif>Bela</option>
                                                    <option class="option" value="Bellpat" @if($session->get_meta('city') == 'Bellpat') selected @endif>Bellpat</option>
                                                    <option class="option" value="Bhag" @if($session->get_meta('city') == 'Bhag') selected @endif>Bhag</option>
                                                    <option class="option" value="Bhakkar" @if($session->get_meta('city') == 'Bhakkar') selected @endif>Bhakkar</option>
                                                    <option class="option" value="Bhalwal" @if($session->get_meta('city') == 'Bhalwal') selected @endif>Bhalwal</option>
                                                    <option class="option" value="Bhimber" @if($session->get_meta('city') == 'Bhimber') selected @endif>Bhimber</option>
                                                    <option class="option" value="Birote" @if($session->get_meta('city') == 'Birote') selected @endif>Birote</option>
                                                    <option class="option" value="Buner" @if($session->get_meta('city') == 'Buner') selected @endif>Buner</option>
                                                    <option class="option" value="Burj" @if($session->get_meta('city') == 'Burj') selected @endif>Burj</option>
                                                    <option class="option" value="Chiniot" @if($session->get_meta('city') == 'Chiniot') selected @endif>Chiniot</option>
                                                    <option class="option" value="Chachro" @if($session->get_meta('city') == 'Chachro') selected @endif>Chachro</option>
                                                    <option class="option" value="Chagai" @if($session->get_meta('city') == 'Chagai') selected @endif>Chagai</option>
                                                    <option class="option" value="Chah Sandan" @if($session->get_meta('city') == 'Chah Sandan') selected @endif>Chah Sandan</option>
                                                    <option class="option" value="Chailianwala" @if($session->get_meta('city') == 'Chailianwala') selected @endif>Chailianwala</option>
                                                    <option class="option" value="Chakdara" @if($session->get_meta('city') == 'Chakdara') selected @endif>Chakdara</option>
                                                    <option class="option" value="Chakku" @if($session->get_meta('city') == 'Chakku') selected @endif>Chakku</option>
                                                    <option class="option" value="Chakwal" @if($session->get_meta('city') == 'Chakwal') selected @endif>Chakwal</option>
                                                    <option class="option" value="Chaman" @if($session->get_meta('city') == 'Chaman') selected @endif>Chaman</option>
                                                    <option class="option" value="CharsEdita" @if($session->get_meta('city') == 'CharsEdita') selected @endif>CharsEdita</option>
                                                    <option class="option" value="Chhatr" @if($session->get_meta('city') == 'Chhatr') selected @endif>Chhatr</option>
                                                    <option class="option" value="Chichawatni" @if($session->get_meta('city') == 'Chichawatni') selected @endif>Chichawatni</option>
                                                    <option class="option" value="Chitral" @if($session->get_meta('city') == 'Chitral') selected @endif>Chitral</option>
                                                    <option class="option" value="Dadu" @if($session->get_meta('city') == 'Dadu') selected @endif>Dadu</option>
                                                    <option class="option" value="Dera Ghazi Khan" @if($session->get_meta('city') == 'Dera Ghazi Khan') selected @endif>Dera Ghazi Khan</option>
                                                    <option class="option" value="Dera Ismail Khan" @if($session->get_meta('city') == 'Dera Ismail Khan') selected @endif>Dera Ismail Khan</option>
                                                    <option class="option" value="Dalbandin" @if($session->get_meta('city') == 'Dalbandin') selected @endif>Dalbandin</option>
                                                    <option class="option" value="Dargai" @if($session->get_meta('city') == 'Dargai') selected @endif>Dargai</option>
                                                    <option class="option" value="Darya Khan" @if($session->get_meta('city') == 'Darya Khan') selected @endif>Darya Khan</option>
                                                    <option class="option" value="Daska" @if($session->get_meta('city') == 'Daska') selected @endif>Daska</option>
                                                    <option class="option" value="Dera Bugti" @if($session->get_meta('city') == 'Dera Bugti') selected @endif>Dera Bugti</option>
                                                    <option class="option" value="Dhana Sar" @if($session->get_meta('city') == 'Dhana Sar') selected @endif>Dhana Sar</option>
                                                    <option class="option" value="Digri" @if($session->get_meta('city') == 'Digri') selected @endif>Digri</option>
                                                    <option class="option" value="Dina City|Dina" @if($session->get_meta('city') == 'Dina City|Dina') selected @endif>Dina</option>
                                                    <option class="option" value="Dinga" @if($session->get_meta('city') == 'Dinga') selected @endif>Dinga</option>
                                                    <option class="option" value="Diplo, Pakistan|Diplo" @if($session->get_meta('city') == 'Diplo, Pakistan|Diplo') selected @endif>Diplo</option>
                                                    <option class="option" value="Diwana" @if($session->get_meta('city') == 'Diwana') selected @endif>Diwana</option>
                                                    <option class="option" value="Dokri" @if($session->get_meta('city') == 'Dokri') selected @endif>Dokri</option>
                                                    <option class="option" value="Drosh" @if($session->get_meta('city') == 'Drosh') selected @endif>Drosh</option>
                                                    <option class="option" value="Duki" @if($session->get_meta('city') == 'Duki') selected @endif>Duki</option>
                                                    <option class="option" value="Dushi" @if($session->get_meta('city') == 'Dushi') selected @endif>Dushi</option>
                                                    <option class="option" value="Duzab" @if($session->get_meta('city') == 'Duzab') selected @endif>Duzab</option>
                                                    <option class="option" value="Faisalabad" @if($session->get_meta('city') == 'Faisalabad') selected @endif>Faisalabad</option>
                                                    <option class="option" value="Fateh Jang" @if($session->get_meta('city') == 'Fateh Jang') selected @endif>Fateh Jang</option>
                                                    <option class="option" value="Ghotki" @if($session->get_meta('city') == 'Ghotki') selected @endif>Ghotki</option>
                                                    <option class="option" value="Gwadar" @if($session->get_meta('city') == 'Gwadar') selected @endif>Gwadar</option>
                                                    <option class="option" value="Gujranwala" @if($session->get_meta('city') == 'Gujranwala') selected @endif>Gujranwala</option>
                                                    <option class="option" value="Gujrat" @if($session->get_meta('city') == 'Gujrat') selected @endif>Gujrat</option>
                                                    <option class="option" value="Gadra" @if($session->get_meta('city') == 'Gadra') selected @endif>Gadra</option>
                                                    <option class="option" value="Gajar" @if($session->get_meta('city') == 'Gajar') selected @endif>Gajar</option>
                                                    <option class="option" value="Gandava" @if($session->get_meta('city') == 'Gandava') selected @endif>Gandava</option>
                                                    <option class="option" value="Garhi Khairo" @if($session->get_meta('city') == 'Garhi Khairo') selected @endif>Garhi Khairo</option>
                                                    <option class="option" value="Garruck" @if($session->get_meta('city') == 'Garruck') selected @endif>Garruck</option>
                                                    <option class="option" value="Ghakhar Mandi" @if($session->get_meta('city') == 'Ghakhar Mandi') selected @endif>Ghakhar Mandi</option>
                                                    <option class="option" value="Ghanian" @if($session->get_meta('city') == 'Ghanian') selected @endif>Ghanian</option>
                                                    <option class="option" value="Ghauspur" @if($session->get_meta('city') == 'Ghauspur') selected @endif>Ghauspur</option>
                                                    <option class="option" value="Ghazluna" @if($session->get_meta('city') == 'Ghazluna') selected @endif>Ghazluna</option>
                                                    <option class="option" value="Girdan" @if($session->get_meta('city') == 'Girdan') selected @endif>Girdan</option>
                                                    <option class="option" value="Gulistan" @if($session->get_meta('city') == 'Gulistan') selected @endif>Gulistan</option>
                                                    <option class="option" value="Gwash" @if($session->get_meta('city') == 'Gwash') selected @endif>Gwash</option>
                                                    <option class="option" value="Hyderabad" @if($session->get_meta('city') == 'Hyderabad') selected @endif>Hyderabad</option>
                                                    <option class="option" value="Hala" @if($session->get_meta('city') == 'Hala') selected @endif>Hala</option>
                                                    <option class="option" value="Haripur" @if($session->get_meta('city') == 'Haripur') selected @endif>Haripur</option>
                                                    <option class="option" value="Hab Chauki" @if($session->get_meta('city') == 'Hab Chauki') selected @endif>Hab Chauki</option>
                                                    <option class="option" value="Hafizabad" @if($session->get_meta('city') == 'Hafizabad') selected @endif>Hafizabad</option>
                                                    <option class="option" value="Hameedabad" @if($session->get_meta('city') == 'Hameedabad') selected @endif>Hameedabad</option>
                                                    <option class="option" value="Hangu" @if($session->get_meta('city') == 'Hangu') selected @endif>Hangu</option>
                                                    <option class="option" value="Harnai" @if($session->get_meta('city') == 'Harnai') selected @endif>Harnai</option>
                                                    <option class="option" value="Hasilpur" @if($session->get_meta('city') == 'Hasilpur') selected @endif>Hasilpur</option>
                                                    <option class="option" value="Haveli Lakha" @if($session->get_meta('city') == 'Haveli Lakha') selected @endif>Haveli Lakha</option>
                                                    <option class="option" value="Hinglaj" @if($session->get_meta('city') == 'Hinglaj') selected @endif>Hinglaj</option>
                                                    <option class="option" value="Hoshab" @if($session->get_meta('city') == 'Hoshab') selected @endif>Hoshab</option>
                                                    <option class="option" value="Islamabad" @if($session->get_meta('city') == 'Islamabad') selected @endif>Islamabad</option>
                                                    <option class="option" value="Islamkot" @if($session->get_meta('city') == 'Islamkot') selected @endif>Islamkot</option>
                                                    <option class="option" value="Ispikan" @if($session->get_meta('city') == 'Ispikan') selected @endif>Ispikan</option>
                                                    <option class="option" value="Jacobabad" @if($session->get_meta('city') == 'Jacobabad') selected @endif>Jacobabad</option>
                                                    <option class="option" value="Jamshoro" @if($session->get_meta('city') == 'Jamshoro') selected @endif>Jamshoro</option>
                                                    <option class="option" value="Jhang" @if($session->get_meta('city') == 'Jhang') selected @endif>Jhang</option>
                                                    <option class="option" value="Jhelum" @if($session->get_meta('city') == 'Jhelum') selected @endif>Jhelum</option>
                                                    <option class="option" value="Jamesabad" @if($session->get_meta('city') == 'Jamesabad') selected @endif>Jamesabad</option>
                                                    <option class="option" value="Jampur" @if($session->get_meta('city') == 'Jampur') selected @endif>Jampur</option>
                                                    <option class="option" value="Janghar" @if($session->get_meta('city') == 'Janghar') selected @endif>Janghar</option>
                                                    <option class="option" value="Jati, Jati(Mughalbhin)" @if($session->get_meta('city') == 'Jati, Jati(Mughalbhin)') selected @endif>Jati</option>
                                                    <option class="option" value="Jauharabad" @if($session->get_meta('city') == 'Jauharabad') selected @endif>Jauharabad</option>
                                                    <option class="option" value="Jhal" @if($session->get_meta('city') == 'Jhal') selected @endif>Jhal</option>
                                                    <option class="option" value="Jhal Jhao" @if($session->get_meta('city') == 'Jhal Jhao') selected @endif>Jhal Jhao</option>
                                                    <option class="option" value="Jhatpat" @if($session->get_meta('city') == 'Jhatpat') selected @endif>Jhatpat</option>
                                                    <option class="option" value="Jhudo" @if($session->get_meta('city') == 'Jhudo') selected @endif>Jhudo</option>
                                                    <option class="option" value="Jiwani" @if($session->get_meta('city') == 'Jiwani') selected @endif>Jiwani</option>
                                                    <option class="option" value="Jungshahi" @if($session->get_meta('city') == 'Jungshahi') selected @endif>Jungshahi</option>
                                                    <option class="option" value="Karachi" @if($session->get_meta('city') == 'Karachi') selected @endif>Karachi</option>
                                                    <option class="option" value="Kotri" @if($session->get_meta('city') == 'Kotri') selected @endif>Kotri</option>
                                                    <option class="option" value="Kalam" @if($session->get_meta('city') == 'Kalam') selected @endif>Kalam</option>
                                                    <option class="option" value="Kalandi" @if($session->get_meta('city') == 'Kalandi') selected @endif>Kalandi</option>
                                                    <option class="option" value="Kalat" @if($session->get_meta('city') == 'Kalat') selected @endif>Kalat</option>
                                                    <option class="option" value="Kamalia" @if($session->get_meta('city') == 'Kamalia') selected @endif>Kamalia</option>
                                                    <option class="option" value="Kamararod" @if($session->get_meta('city') == 'Kamararod') selected @endif>Kamararod</option>
                                                    <option class="option" value="Kamber" @if($session->get_meta('city') == 'Kamber') selected @endif>Kamber</option>
                                                    <option class="option" value="Kamokey" @if($session->get_meta('city') == 'Kamokey') selected @endif>Kamokey</option>
                                                    <option class="option" value="Kandhkot" @if($session->get_meta('city') == 'Kandhkot') selected @endif>Kandhkot</option>
                                                    <option class="option" value="Kanak" @if($session->get_meta('city') == 'Kanak') selected @endif>Kanak</option>
                                                    <option class="option" value="Kandi" @if($session->get_meta('city') == 'Kandi') selected @endif>Kandi</option>
                                                    <option class="option" value="Kandiaro" @if($session->get_meta('city') == 'Kandiaro') selected @endif>Kandiaro</option>
                                                    <option class="option" value="Kanpur" @if($session->get_meta('city') == 'Kanpur') selected @endif>Kanpur</option>
                                                    <option class="option" value="Kapip" @if($session->get_meta('city') == 'Kapip') selected @endif>Kapip</option>
                                                    <option class="option" value="Kappar" @if($session->get_meta('city') == 'Kappar') selected @endif>Kappar</option>
                                                    <option class="option" value="Karak City" @if($session->get_meta('city') == 'Karak City') selected @endif>Karak City</option>
                                                    <option class="option" value="Karodi" @if($session->get_meta('city') == 'Karodi') selected @endif>Karodi</option>
                                                    <option class="option" value="Kashmor" @if($session->get_meta('city') == 'Kashmor') selected @endif>Kashmor</option>
                                                    <option class="option" value="Kasur" @if($session->get_meta('city') == 'Kasur') selected @endif>Kasur</option>
                                                    <option class="option" value="Katuri" @if($session->get_meta('city') == 'Katuri') selected @endif>Katuri</option>
                                                    <option class="option" value="Keti Bandar" @if($session->get_meta('city') == 'Keti Bandar') selected @endif>Keti Bandar</option>
                                                    <option class="option" value="Khairpur" @if($session->get_meta('city') == 'Khairpur') selected @endif>Khairpur</option>
                                                    <option class="option" value="Khanaspur" @if($session->get_meta('city') == 'Khanaspur') selected @endif>Khanaspur</option>
                                                    <option class="option" value="Khanewal"@if($session->get_meta('city') == 'Khanewal') selected @endif>Khanewal</option>
                                                    <option class="option" value="Kharan" @if($session->get_meta('city') == 'Kharan') selected @endif>Kharan</option>
                                                    <option class="option" value="kharian" @if($session->get_meta('city') == 'kharian') selected @endif>kharian</option>
                                                    <option class="option" value="Khokhropur" @if($session->get_meta('city') == 'Khokhropur') selected @endif>Khokhropur</option>
                                                    <option class="option" value="Khora" @if($session->get_meta('city') == 'Khora') selected @endif>Khora</option>
                                                    <option class="option" value="Khushab" @if($session->get_meta('city') == 'Khushab') selected @endif>Khushab</option>
                                                    <option class="option" value="Khuzdar" @if($session->get_meta('city') == 'Khuzdar') selected @endif>Khuzdar</option>
                                                    <option class="option" value="Kikki" @if($session->get_meta('city') == 'Kikki') selected @endif>Kikki</option>
                                                    <option class="option" value="Klupro" @if($session->get_meta('city') == 'Klupro') selected @endif>Klupro</option>
                                                    <option class="option" value="Kohan" @if($session->get_meta('city') == 'Kohan') selected @endif>Kohan</option>
                                                    <option class="option" value="Kohat" @if($session->get_meta('city') == 'Kohat') selected @endif>Kohat</option>
                                                    <option class="option" value="Kohistan" @if($session->get_meta('city') == 'Kohistan') selected @endif>Kohistan</option>
                                                    <option class="option" value="Kohlu" @if($session->get_meta('city') == 'Kohlu') selected @endif>Kohlu</option>
                                                    <option class="option" value="Korak" @if($session->get_meta('city') == 'Korak') selected @endif>Korak</option>
                                                    <option class="option" value="Korangi" @if($session->get_meta('city') == 'Korangi') selected @endif>Korangi</option>
                                                    <option class="option" value="Kot Sarae" @if($session->get_meta('city') == 'Kot Sarae') selected @endif>Kot Sarae</option>
                                                    <option class="option" value="Kotli" @if($session->get_meta('city') == 'Kotli') selected @endif>Kotli</option>
                                                    <option class="option" value="Lahore" @if($session->get_meta('city') == 'Lahore') selected @endif>Lahore</option>
                                                    <option class="option" value="Larkana" @if($session->get_meta('city') == 'Larkana') selected @endif>Larkana</option>
                                                    <option class="option" value="Lahri" @if($session->get_meta('city') == 'Lahri') selected @endif>Lahri</option>
                                                    <option class="option" value="Lakki Marwat" @if($session->get_meta('city') == 'Lakki Marwat') selected @endif>Lakki Marwat</option>
                                                    <option class="option" value="Lasbela" @if($session->get_meta('city') == 'Lasbela') selected @endif>Lasbela</option>
                                                    <option class="option" value="Latamber" @if($session->get_meta('city') == 'Latamber') selected @endif>Latamber</option>
                                                    <option class="option" value="Layyah" @if($session->get_meta('city') == 'Layyah') selected @endif>Layyah</option>
                                                    <option class="option" value="Leiah" @if($session->get_meta('city') == 'Leiah') selected @endif>Leiah</option>
                                                    <option class="option" value="Liari" @if($session->get_meta('city') == 'Liari') selected @endif>Liari</option>
                                                    <option class="option" value="Lodhran" @if($session->get_meta('city') == 'Lodhran') selected @endif>Lodhran</option>
                                                    <option class="option" value="Loralai" @if($session->get_meta('city') == 'Loralai') selected @endif>Loralai</option>
                                                    <option class="option" value="Lower Dir" @if($session->get_meta('city') == 'Lower Dir') selected @endif>Lower Dir</option>
                                                    <option class="option" value="Shadan Lund" @if($session->get_meta('city') == 'Shadan Lund') selected @endif>Shadan Lund</option>
                                                    <option class="option" value="Multan" @if($session->get_meta('city') == 'Multan') selected @endif>Multan</option>
                                                    <option class="option" value="Mandi Bahauddin" @if($session->get_meta('city') == 'Mandi Bahauddin') selected @endif>Mandi Bahauddin</option>
                                                    <option class="option" value="Mansehra" @if($session->get_meta('city') == 'Mansehra') selected @endif>Mansehra</option>
                                                    <option class="option" value="Mian Chanu" @if($session->get_meta('city') == 'Mian Chanu') selected @endif>Mian Chanu</option>
                                                    <option class="option" value="Mirpur" @if($session->get_meta('city') == 'Mirpur') selected @endif>Mirpur</option>
                                                    <option class="option" value="Mirpur Mathelo" @if($session->get_meta('city') == 'Mirpur Mathelo') selected @endif>Mirpur Mathelo</option>
                                                    <option class="option" value="Moro, Pakistan|Moro" @if($session->get_meta('city') == 'Moro, Pakistan|Moro') selected @endif>Moro</option>
                                                    <option class="option" value="Mardan" @if($session->get_meta('city') == 'Mardan') selected @endif>Mardan</option>
                                                    <option class="option" value="Mach" @if($session->get_meta('city') == 'Mach') selected @endif>Mach</option>
                                                    <option class="option" value="Madyan" @if($session->get_meta('city') == 'Madyan') selected @endif>Madyan</option>
                                                    <option class="option" value="Malakand" @if($session->get_meta('city') == 'Malakand') selected @endif>Malakand</option>
                                                    <option class="option" value="Mand" @if($session->get_meta('city') == 'Mand') selected @endif>Mand</option>
                                                    <option class="option" value="Manguchar" @if($session->get_meta('city') == 'Manguchar') selected @endif>Manguchar</option>
                                                    <option class="option" value="Mashki Chah" @if($session->get_meta('city') == 'Ayubia') selected @endif>Mashki Chah</option>
                                                    <option class="option" value="Maslti" @if($session->get_meta('city') == 'Maslti') selected @endif>Maslti</option>
                                                    <option class="option" value="Mastuj" @if($session->get_meta('city') == 'Mastuj') selected @endif>Mastuj</option>
                                                    <option class="option" value="Mastung" @if($session->get_meta('city') == 'Mastung') selected @endif>Mastung</option>
                                                    <option class="option" value="Mathi" @if($session->get_meta('city') == 'Mathi') selected @endif>Mathi</option>
                                                    <option class="option" value="Matiari" @if($session->get_meta('city') == 'Matiari') selected @endif>Matiari</option>
                                                    <option class="option" value="Mehar" @if($session->get_meta('city') == 'Mehar') selected @endif>Mehar</option>
                                                    <option class="option" value="Mekhtar" @if($session->get_meta('city') == 'Mekhtar') selected @endif>Mekhtar</option>
                                                    <option class="option" value="Merui" @if($session->get_meta('city') == 'Merui') selected @endif>Merui</option>
                                                    <option class="option" value="Mianwali" @if($session->get_meta('city') == 'Mianwali') selected @endif>Mianwali</option>
                                                    <option class="option" value="Mianez" @if($session->get_meta('city') == 'Mianez') selected @endif>Mianez</option>
                                                    <option class="option" value="Mirpur Batoro" @if($session->get_meta('city') == 'Mirpur Batoro') selected @endif>Mirpur Batoro</option>
                                                    <option class="option" value="Mirpur Khas" @if($session->get_meta('city') == 'Mirpur Khas') selected @endif>Mirpur Khas</option>
                                                    <option class="option" value="Mirpur Sakro" @if($session->get_meta('city') == 'Mirpur Sakro') selected @endif>Mirpur Sakro</option>
                                                    <option class="option" value="Mithi" @if($session->get_meta('city') == 'Mithi') selected @endif>Mithi</option>
                                                    <option class="option" value="Mongora" @if($session->get_meta('city') == 'Mongora') selected @endif>Mongora</option>
                                                    <option class="option" value="Murgha Kibzai" @if($session->get_meta('city') == 'Murgha Kibzai') selected @endif>Murgha Kibzai</option>
                                                    <option class="option" value="Muridke" @if($session->get_meta('city') == 'Muridke') selected @endif>Muridke</option>
                                                    <option class="option" value="Musa Khel Bazar" @if($session->get_meta('city') == 'Musa Khel Bazar') selected @endif>Musa Khel Bazar</option>
                                                    <option class="option" value="Muzaffar Garh" @if($session->get_meta('city') == 'Muzaffar Garh') selected @endif>Muzaffar Garh</option>
                                                    <option class="option" value="Muzaffarabad" @if($session->get_meta('city') == 'Muzaffarabad') selected @endif>Muzaffarabad</option>
                                                    <option class="option" value="Nawabshah" @if($session->get_meta('city') == 'Nawabshah') selected @endif>Nawabshah</option>
                                                    <option class="option" value="Nazimabad" @if($session->get_meta('city') == 'Nazimabad') selected @endif>Nazimabad</option>
                                                    <option class="option" value="Nowshera" @if($session->get_meta('city') == 'Nowshera') selected @endif>Nowshera</option>
                                                    <option class="option" value="Nagar Parkar" @if($session->get_meta('city') == 'Nagar Parkar') selected @endif>Nagar Parkar</option>
                                                    <option class="option" value="Nagha Kalat" @if($session->get_meta('city') == 'Nagha Kalat') selected @endif>Nagha Kalat</option>
                                                    <option class="option" value="Nal" @if($session->get_meta('city') == 'Nal') selected @endif>Nal</option>
                                                    <option class="option" value="Naokot" @if($session->get_meta('city') == 'Naokot') selected @endif>Naokot</option>
                                                    <option class="option" value="Nasirabad" @if($session->get_meta('city') == 'Nasirabad') selected @endif>Nasirabad</option>
                                                    <option class="option" value="Nauroz Kalat" @if($session->get_meta('city') == 'Nauroz Kalat') selected @endif>Nauroz Kalat</option>
                                                    <option class="option" value="Naushara" @if($session->get_meta('city') == 'Naushara') selected @endif>Naushara</option>
                                                    <option class="option" value="Nur Gamma" @if($session->get_meta('city') == 'Nur Gamma') selected @endif>Nur Gamma</option>
                                                    <option class="option" value="Nushki" @if($session->get_meta('city') == 'Nushki') selected @endif>Nushki</option>
                                                    <option class="option" value="Nuttal" @if($session->get_meta('city') == 'Nuttal') selected @endif>Nuttal</option>
                                                    <option class="option" value="Okara" @if($session->get_meta('city') == 'Okara') selected @endif>Okara</option>
                                                    <option class="option" value="Ormara" @if($session->get_meta('city') == 'Ormara') selected @endif>Ormara</option>
                                                    <option class="option" value="Peshawar" @if($session->get_meta('city') == 'Peshawar') selected @endif>Peshawar</option>
                                                    <option class="option" value="Panjgur" @if($session->get_meta('city') == 'Panjgur') selected @endif>Panjgur</option>
                                                    <option class="option" value="Pasni City" @if($session->get_meta('city') == 'Pasni City') selected @endif>Pasni City</option>
                                                    <option class="option" value="Paharpur" @if($session->get_meta('city') == 'Paharpur') selected @endif>Paharpur</option>
                                                    <option class="option" value="Palantuk" @if($session->get_meta('city') == 'Palantuk') selected @endif>Palantuk</option>
                                                    <option class="option" value="Pendoo" @if($session->get_meta('city') == 'Pendoo') selected @endif>Pendoo</option>
                                                    <option class="option" value="Piharak" @if($session->get_meta('city') == 'Piharak') selected @endif>Piharak</option>
                                                    <option class="option" value="Pirmahal" @if($session->get_meta('city') == 'Pirmahal') selected @endif>Pirmahal</option>
                                                    <option class="option" value="Pishin" @if($session->get_meta('city') == 'Pishin') selected @endif>Pishin</option>
                                                    <option class="option" value="Plandri" @if($session->get_meta('city') == 'Plandri') selected @endif>Plandri</option>
                                                    <option class="option" value="Pokran" @if($session->get_meta('city') == 'Pokran') selected @endif>Pokran</option>
                                                    <option class="option" value="Pounch" @if($session->get_meta('city') == 'Pounch') selected @endif>Pounch</option>
                                                    <option class="option" value="Qambar" @if($session->get_meta('city') == 'Qambar') selected @endif>Quetta</option>
                                                    <option class="option" value="Qambar" @if($session->get_meta('city') == 'Qambar') selected @endif>Qambar</option>
                                                    <option class="option" value="Qamruddin Karez" @if($session->get_meta('city') == 'Qamruddin Karez') selected @endif>Qamruddin Karez</option>
                                                    <option class="option" value="Qazi Ahmad" @if($session->get_meta('city') == 'Qazi Ahmad') selected @endif>Qazi Ahmad</option>
                                                    <option class="option" value="Qila Abdullah" @if($session->get_meta('city') == 'Qila Abdullah') selected @endif>Qila Abdullah</option>
                                                    <option class="option" value="Qila Ladgasht" @if($session->get_meta('city') == 'Qila Ladgasht') selected @endif>Qila Ladgasht</option>
                                                    <option class="option" value="Qila Safed" @if($session->get_meta('city') == 'Qila Safed') selected @endif>Qila Safed</option>
                                                    <option class="option" value="Qila Saifullah" @if($session->get_meta('city') == 'Qila Saifullah') selected @endif>Qila Saifullah</option>
                                                    <option class="option" value="Rawalpindi" @if($session->get_meta('city') == 'Rawalpindi') selected @endif>Rawalpindi</option>
                                                    <option class="option" value="Rabwah" @if($session->get_meta('city') == 'Rabwah') selected @endif>Rabwah</option>
                                                    <option class="option" value="Rahim Yar Khan" @if($session->get_meta('city') == 'Rahim Yar Khan') selected @endif>Rahim Yar Khan</option>
                                                    <option class="option" value="Rajan Pur" @if($session->get_meta('city') == 'Rajan Pur') selected @endif>Rajan Pur</option>
                                                    <option class="option" value="Rakhni" @if($session->get_meta('city') == 'Rakhni') selected @endif>Rakhni</option>
                                                    <option class="option" value="Ranipur" @if($session->get_meta('city') == 'Ranipur') selected @endif>Ranipur</option>
                                                    <option class="option" value="Ratodero" @if($session->get_meta('city') == 'Ratodero') selected @endif>Ratodero</option>
                                                    <option class="option" value="Rawalakot"  @if($session->get_meta('city') == 'Rawalakot') selected @endif>Rawalakot</option>
                                                    <option class="option" value="Renala Khurd"  @if($session->get_meta('city') == 'Renala Khurd') selected @endif>Renala Khurd</option>
                                                    <option class="option" value="Robat Thana"  @if($session->get_meta('city') == 'Robat Thana') selected @endif>Robat Thana</option>
                                                    <option class="option" value="Rodkhan"  @if($session->get_meta('city') == 'Rodkhan') selected @endif>Rodkhan</option>
                                                    <option class="option" value="Rohri"  @if($session->get_meta('city') == 'Rohri') selected @endif>Rohri</option>
                                                    <option class="option" value="Sialkot"  @if($session->get_meta('city') == 'Sialkot') selected @endif>Sialkot</option>
                                                    <option class="option" value="Sadiqabad"  @if($session->get_meta('city') == 'Sadiqabad') selected @endif>Sadiqabad</option>
                                                    <option class="option" value="Safdar Abad- (Dhaban Singh)"  @if($session->get_meta('city') == 'Safdar Abad- (Dhaban Singh)') selected @endif>Safdar Abad</option>
                                                    <option class="option" value="Sahiwal"  @if($session->get_meta('city') == 'Sahiwal') selected @endif>Sahiwal</option>
                                                    <option class="option" value="Saidu Sharif"  @if($session->get_meta('city') == 'Saidu Sharif') selected @endif>Saidu Sharif</option>
                                                    <option class="option" value="Saindak"  @if($session->get_meta('city') == 'Saindak') selected @endif>Saindak</option>
                                                    <option class="option" value="Sakrand"  @if($session->get_meta('city') == 'Sakrand') selected @endif>Sakrand</option>
                                                    <option class="option" value="Sanjawi"  @if($session->get_meta('city') == 'Sanjawi') selected @endif>Sanjawi</option>
                                                    <option class="option" value="Sargodha"  @if($session->get_meta('city') == 'Sargodha') selected @endif>Sargodha</option>
                                                    <option class="option" value="Saruna"  @if($session->get_meta('city') == 'Saruna') selected @endif>Saruna</option>
                                                    <option class="option" value="Shabaz Kalat"  @if($session->get_meta('city') == 'Shabaz Kalat') selected @endif>Shabaz Kalat</option>
                                                    <option class="option" value="Shadadkhot"  @if($session->get_meta('city') == 'Shadadkhot') selected @endif>Shadadkhot</option>
                                                    <option class="option" value="Shahbandar"  @if($session->get_meta('city') == 'Shahbandar') selected @endif>Shahbandar</option>
                                                    <option class="option" value="Shahpur"  @if($session->get_meta('city') == 'Shahpur') selected @endif>Shahpur</option>
                                                    <option class="option" value="Shahpur Chakar"  @if($session->get_meta('city') == 'Shahpur Chakar') selected @endif>Shahpur Chakar</option>
                                                    <option class="option" value="Shakargarh"  @if($session->get_meta('city') == 'Shakargarh') selected @endif>Shakargarh</option>
                                                    <option class="option" value="Shangla"  @if($session->get_meta('city') == 'Shangla') selected @endif>Shangla</option>
                                                    <option class="option" value="Sharam Jogizai"  @if($session->get_meta('city') == 'Sharam Jogizai') selected @endif>Sharam Jogizai</option>
                                                    <option class="option" value="Sheikhupura"  @if($session->get_meta('city') == 'Sheikhupura') selected @endif>Sheikhupura</option>
                                                    <option class="option" value="Shikarpur"  @if($session->get_meta('city') == 'Shikarpur') selected @endif>Shikarpur</option>
                                                    <option class="option" value="Shingar"  @if($session->get_meta('city') == 'Shingar') selected @endif>Shingar</option>
                                                    <option class="option" value="Shorap"  @if($session->get_meta('city') == 'Shorap') selected @endif>Shorap</option>
                                                    <option class="option" value="Sibi"  @if($session->get_meta('city') == 'Sibi') selected @endif>Sibi</option>
                                                    <option class="option" value="Sohawa"  @if($session->get_meta('city') == 'Sohawa') selected @endif>Sohawa</option>
                                                    <option class="option" value="Sonmiani"  @if($session->get_meta('city') == 'Sonmiani') selected @endif>Sonmiani</option>
                                                    <option class="option" value="Sooianwala"  @if($session->get_meta('city') == 'Sooianwala') selected @endif>Sooianwala</option>
                                                    <option class="option" value="Spezand" @if($session->get_meta('city') == 'Spezand') selected @endif>Spezand</option>
                                                    <option class="option" value="Spintangi" @if($session->get_meta('city') == 'Spintangi') selected @endif>Spintangi</option>
                                                    <option class="option" value="Sui" @if($session->get_meta('city') == 'Sui') selected @endif>Sui</option>
                                                    <option class="option" value="Sujawal" @if($session->get_meta('city') == 'Sujawal') selected @endif>Sujawal</option>
                                                    <option class="option" value="Sukkur" @if($session->get_meta('city') == 'Sukkur') selected @endif>Sukkur</option>
                                                    <option class="option" value="Suntsar" @if($session->get_meta('city') == 'Suntsar') selected @endif>Suntsar</option>
                                                    <option class="option" value="Surab" @if($session->get_meta('city') == 'Surab') selected @endif>Surab</option>
                                                    <option class="option" value="Swabi" @if($session->get_meta('city') == 'Swabi') selected @endif>Swabi</option>
                                                    <option class="option" value="Swat" @if($session->get_meta('city') == 'Swat') selected @endif>Swat</option>
                                                    <option class="option" value="Tando Adam" @if($session->get_meta('city') == 'Tando Adam') selected @endif>Tando Adam</option>
                                                    <option class="option" value="Tando Bago" @if($session->get_meta('city') == 'Tando Bago') selected @endif>Tando Bago</option>
                                                    <option class="option" value="Tando Allahyar" @if($session->get_meta('city') == 'Tando Allahyar') selected @endif>Tando Allahyar</option>
                                                    <option class="option" value="Tando Muhammad Khan" @if($session->get_meta('city') == 'Tando Muhammad Khan') selected @endif> Tando Muhammad Khan</option>
                                                    <option class="option" value="Tangi" @if($session->get_meta('city') == 'Tangi') selected @endif>Tangi</option>
                                                    <option class="option" value="Tank City" @if($session->get_meta('city') == 'Tank City') selected @endif>Tank City</option>
                                                    <option class="option" value="Tar Ahamd Rind" @if($session->get_meta('city') == 'Tar Ahamd Rind') selected @endif>Tar Ahamd Rind</option>
                                                    <option class="option" value="Thalo" @if($session->get_meta('city') == 'Thalo') selected @endif>Thalo</option>
                                                    <option class="option" value="Thatta" @if($session->get_meta('city') == 'Thatta') selected @endif>Thatta</option>
                                                    <option class="option" value="Toba Tek Singh" @if($session->get_meta('city') == 'Toba Tek Singh') selected @endif>Toba Tek Singh</option>
                                                    <option class="option" value="Tordher" @if($session->get_meta('city') == 'Tordher') selected @endif>Tordher</option>
                                                    <option class="option" value="Tujal" @if($session->get_meta('city') == 'Tujal') selected @endif>Tujal</option>
                                                    <option class="option" value="Tump" @if($session->get_meta('city') == 'Tump') selected @endif>Tump</option>
                                                    <option class="option" value="Turbat" @if($session->get_meta('city') == 'Turbat') selected @endif>Turbat</option>
                                                    <option class="option" value="Umarao" @if($session->get_meta('city') == 'Umarao') selected @endif>Umarao</option>
                                                    <option class="option" value="Umerkot" @if($session->get_meta('city') == 'Umerkot') selected @endif>Umerkot</option>
                                                    <option class="option" value="Upper Dir" @if($session->get_meta('city') == 'Upper Dir') selected @endif>Upper Dir</option>
                                                    <option class="option" value="Uthal" @if($session->get_meta('city') == 'Uthal') selected @endif>Uthal</option>
                                                    <option class="option" value="Vehari" @if($session->get_meta('city') == 'Vehari') selected @endif>Vehari</option>
                                                    <option class="option" value="Veirwaro" @if($session->get_meta('city') == 'Veirwaro') selected @endif>Veirwaro</option>
                                                    <option class="option" value="Vitakri" @if($session->get_meta('city') == 'Vitakri') selected @endif>Vitakri</option>
                                                    <option class="option" value="Wadh" @if($session->get_meta('city') == 'Wadh') selected @endif>Wadh</option>
                                                    <option class="option" value="Wah Cantt" @if($session->get_meta('city') == 'Wah Cantt') selected @endif>Wah Cantt</option>
                                                    <option class="option" value="Warah" @if($session->get_meta('city') == 'Warah') selected @endif>Warah</option>
                                                    <option class="option" value="Washap" @if($session->get_meta('city') == 'Washap') selected @endif>Washap</option>
                                                    <option class="option" value="Wasjuk" @if($session->get_meta('city') == 'Wasjuk') selected @endif>Wasjuk</option>
                                                    <option class="option" value="Wazirabad" @if($session->get_meta('city') == 'Wazirabad') selected @endif>Wazirabad</option>
                                                    <option class="option" value="Yakmach" @if($session->get_meta('city') == 'Yakmach') selected @endif>Yakmach</option>
                                                    <option class="option" value="Zhob" @if($session->get_meta('city') == 'Zhob') selected @endif>Zhob</option>
                                                    <option class="option" value="Other">Other</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Address</label>
                                                @if($session->get_meta('address', 'N/A') == 'N/A')
                                                    <textarea class="form-control" name="address" placeholder="Address not found enter your address"></textarea>
                                                @else
                                                    <textarea class="form-control" name="address" placeholder="Address not found enter your address">{{$session->get_meta('address', 'N/A')}}</textarea>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Additional Information (Optional)</label>
                                                @if($session->get_meta('additional_info', 'N/A') == 'N/A')
                                                    <textarea class="form-control" name="additional_info" placeholder="Additional information not found enter your information"></textarea>
                                                @else
                                                    <textarea class="form-control" name="additional_info" placeholder="Additional information not found enter your information">{{ $session->get_meta('additional_info') }}</textarea>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Update Account">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
         // $("#region option[value=kpk]")

        $('#region').change(function () {

            if ($("#region").val() === "sindh") {

                $('#city').empty();

                let cities = {
                    'Badin': 'Badin',
                    'Dadu': 'Dadu',
                    'Daharki': 'Daharki',
                    'Ghotki': 'Ghotki',
                    'Hyderabad': 'Hyderabad',
                    'Jacobabad': 'Jacobabad',
                    'Kamber': 'Kamber',
                    'Kandhkot': 'Kandhkot',
                    'Karachi': 'Karachi',
                    'Khairpur': 'Khairpur',
                    'Kotri': 'Kotri',
                    'Larkana': 'Larkana',
                    'Mirpur Khas': 'Mirpur Khas',
                    'Mirpur Mathelo': 'Mirpur Mathelo',
                    'Nawabshah': 'Nawabshah',
                    'Shahdadkot': 'Shahdadkot',
                    'Shikarpur': 'Shikarpur',
                    'Sukkur': 'Sukkur',
                    'Tando Adam': 'Tando Adam',
                    'Tando Allahyar': 'Tando Allahyar',
                    'Tando Muhammad Khan': 'Tando Muhammad Khan',
                    'Umerkot': 'Umerkot',
                    'Other' : 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === "punjab") {

                $('#city').empty();

                let cities = {

                    'Ahmedpur East': 'Ahmedpur East',
                    'Arif Wala': 'Arif Wala',
                    'Attock': 'Attock',
                    'Bahawalnagar': 'Bahawalnagar',
                    'Bahawalpur': 'Bahawalpur',
                    'Bhakkar': 'Bhakkar',
                    'Bhalwal': 'Bhalwal',
                    'Burewala': 'Burewala',
                    'Chakwal': 'Chakwal',
                    'Chiniot': 'Chiniot',
                    'Chishtian': 'Chishtian',
                    'Daska': 'Daska',
                    'Dera Ghazi Khan': 'Dera Ghazi Khan',
                    'Faisalabad': 'Faisalabad',
                    'Ferozwala': 'Ferozwala',
                    'Gojra': 'Gojra',
                    'Gujranwala': 'Gujranwala',
                    'Gujrat': 'Gujrat',
                    'Hafizabad': 'Hafizabad',
                    'Haroonabad': 'Haroonabad',
                    'Hasilpur': 'Hasilpur',
                    'Islamabad': 'Islamabad',
                    'Jaranwala': 'Jaranwala',
                    'Jatoi': 'Jatoi',
                    'Jhang': 'Jhang',
                    'Jhelum': 'Jhelum',
                    'Kamalia': 'Kamalia',
                    'Kamoke': 'Kamoke',
                    'Kasur': 'Kasur',
                    'Khanewal': 'Khanewal',
                    'Khanpur': 'Khanpur',
                    'Khushab': 'Khushab',
                    'Kot Abdul Malik': 'Kot Abdul Malik',
                    'Kot Addu': 'Kot Addu',
                    'Lahore': 'Lahore',
                    'Layyah': 'Layyah',
                    'Lodhran': 'Lodhran',
                    'Mandi Bahauddin': 'Mandi Bahauddin',
                    'Mianwali': 'Mianwali',
                    'Multan': 'Multan',
                    'Muridke': 'Muridke',
                    'Muzaffargarh': 'Muzaffargarh',
                    'Narowal': 'Narowal',
                    'Okara': 'Okara',
                    'Pakpattan': 'Pakpattan',
                    'Rahim Yar Khan': 'Rahim Yar Khan',
                    'Rawalpindi': 'Rawalpindi',
                    'Sadiqabad': 'Sadiqabad',
                    'Sahiwal': 'Sahiwal',
                    'Sambrial': 'Sambrial',
                    'Samundri': 'Samundri',
                    'Sargodha': 'Sargodha',
                    'Sialkot': 'Sialkot',
                    'Taxila': 'Taxila',
                    'Vehari': 'Vehari',
                    'Wah Cantonment': 'Wah Cantonment',
                    'Wazirabad': 'Wazirabad',
                    'Other' : 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'kpk') {
                $('#city').empty();

                let cities = {
                    'Abbotabad': 'Abbotabad',
                    'Charsadda': 'Charsadda',
                    'Dera Ismail Khan': 'Dera Ismail Khan',
                    'Kabal': 'Kabal',
                    'Kohat': 'Kohat',
                    'Mansehra': 'Mansehra',
                    'Mardan': 'Mardan',
                    'Mingora': 'Mingora',
                    'Peshawar': 'Peshawar',
                    'Swabi': 'Swabi',
                    'Wazirabad': 'Wazirabad',
                    'Other' : 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'balochistan') {

                $('#city').empty();

                let cities = {
                    'Chaman': 'Chaman',
                    'Gwadar': 'Gwadar',
                    'Hub': 'Hub',
                    'Khuzdar': 'Khuzdar',
                    'Quetta': 'Quetta',
                    'Turbat': 'Turbat',
                    'Other' : 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else if ($("#region").val() === 'gilgit_baltistan') {

                $('#city').empty();

                let cities = {
                    'Askole': 'Askole',
                    'Astore': 'Astore',
                    'Bunji': 'Bunji',
                    'Chillinji': 'Chillinji',
                    'Chiran': 'Chiran',
                    'Gakuch': 'Gakuch',
                    'Ghangche': 'Ghangche',
                    'Ghizer': 'Ghizer',
                    'Gilgit': 'Gilgit',
                    'Dayor': 'Dayor',
                    'Sultanabad': 'Sultanabad',
                    'Oshikhanda': 'Oshikhanda',
                    'Jalalabad': 'Jalalabad',
                    'Jutial Gilgit': 'Jutial Gilgit',
                    'Alyabad Hunza': 'Alyabad Hunza',
                    'Gorikot': 'Gorikot',
                    'Gulmit': 'Gulmit',
                    'Jaglot': 'Jaglot',
                    'Chalt_(Nagar)': 'Chalt (Nagar)',
                    'Thole_(Nagar)': 'Thole (Nagar)',
                    'Nasir Abad': 'Nasir Abad',
                    'Mayoon': 'Mayoon',
                    'Khana Abad': 'Khana Abad',
                    'Hussain Abad': 'Hussain Abad',
                    'Qasimabad_Masoot_(Nagar)': 'Qasimabad Masoot (Nagar)',
                    'Nagar Proper': 'Nagar Proper',
                    'Ghulmat_(Nagar)': 'Ghulmat (Nagar)',
                    'Karimabad_(Hunza)': 'Karimabad (Hunza)',
                    'Ishkoman': 'Ishkoman',
                    'Khaplu': 'Khaplu',
                    'Minimerg': 'Minimerg',
                    'Misgar': 'Misgar',
                    'Passu': 'Passu',
                    'Shimshal': 'Shimshal',
                    'Skardu': 'Skardu',
                    'Sust': 'Sust',
                    'Thowar': 'Thowar',
                    'Kharmang': 'Kharmang',
                    'Roundo': 'Roundo',
                    'Shigar Muhammad Amin': 'Shigar Muhammad Amin',
                    'Shigar Bunpa': 'Shigar Bunpa',
                    'Shigar Kiahong': 'Shigar Kiahong',
                    'Other' : 'Other'

                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            } else {
                $('#city').empty();

                let cities = {
                    'Other': 'Other'
                };

                for (let c in cities) {
                    $('#city').append($('<option>', {value: c, text: cities[c]}))
                }

            }
        });


    </script>
@endsection

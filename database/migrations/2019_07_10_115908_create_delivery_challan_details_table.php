<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryChallanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_challan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('challan_id');
            $table->unsignedInteger('order_product_colors_id');
            $table->string('quantity');
            $table->foreign('challan_id')->references('id')
                ->on('delivery_challans')
                ->onDelete('cascade');
            $table->foreign('order_product_colors_id')->references('id')
                ->on('order_product_colors')
                ->onDelete('cascade');
//            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_challan_details');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryChallanReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_challan_report_view', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('challan_id');
            $table->unsignedInteger('order_id');
            $table->string('order_no');
            $table->date('date');
            $table->string('client_name');
            $table->string('distributor_name');
            $table->longText('address');
            $table->string('order_qty');
            $table->string('delivered_qty');
            $table->string('balance_qty');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_challan_report_view');
    }
}

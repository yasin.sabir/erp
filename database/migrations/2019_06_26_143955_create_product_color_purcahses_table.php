<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductColorPurcahsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_color_purcahses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('purchases_id');
            $table->unsignedInteger('product_color_id');
            $table->string('quantity');
            $table
                ->foreign('purchases_id')
                ->references('id')
                ->on('purchases')
                ->onDelete('cascade');
            $table
                ->foreign('product_color_id')
                ->references('id')
                ->on('Product_color')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_color_purcahses');
    }
}

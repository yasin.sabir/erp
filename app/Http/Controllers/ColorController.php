<?php

namespace App\Http\Controllers;

use App\Color;
use App\Product;
use App\ProductColorPurcahses;
use App\ProductColors;
use App\ProductStocks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\storeColor;

class ColorController extends Controller
{
    public function index(){
        $colors = DB::table('colors')->get();
        return view('colors.colors', ['colors' => $colors]);
    }

    public function add(){
        $products = Product::all();
        return view('colors.add_color',['products' => $products]);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'code' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'quantity' => 'required'
        ],[
            'size'    => 'The :attribute must be less than :size.',

        ]);

        if($validator->fails()){
            return redirect('color/add')->withErrors($validator)->withInput();
        }


        $file = $request->file('image');
        $newfile = Storage::put('public/color', $file);
        // dd($newfile);
        $newfile = (explode("/",$newfile));
        $filename = $newfile[1].'/'.$newfile[2];
        // $newfile = $file->move(public_path().'\color'.$request->input('name').'.jpg');
        // dd($filename);
        $color = new Color();
        $color->name = $request->input('name');
        $color->code = $request->input('code');
        $color->image = $filename;
        if($color->save()){
            $product = $request->product;
            $product_color = new ProductColors();
            $product_color->product_id = $product;
            $product_color->color_id = $color->id;
            $product_color->quantity = $request->quantity;
            $product_color->quantity_in_hand = $request->quantity;
            $product_color->save();
            $color->product_color_id = $product_color->id;
            $color->save();
            return redirect('colors')->with("msg","Color added");
        }else{
            $error = [
                'error' => 'An error occurred while saving!',
            ];
            return redirect('color/add')->withErrors($error)->withInput();
        }


    }

    public function edit($id){
        $color = Color::find($id);
        $product_color = ProductColors::where('color_id',$id)->first();
        $products = Product::all();
        $product_stock = ProductColorPurcahses::where('product_color_id', $product_color->id)->get();
        return view('colors.edit_color', ['color' => $color,'product_color' => $product_color,'products' => $products,'product_stock' => $product_stock]);
    }

    public function update($id, Request $request){
        //        $validator = Validator::make($request->all(), [
        //            'name' => 'required|max:255',
        //            'code' => 'required|max:255',
        //            'image' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        //        ],[
        //            'size'    => 'The :attribute must be less than :size.',
        //        ]);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'code' => 'required|max:255',
            'image' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048',
            'quantity' => 'required'
        ],[
            'size'    => 'The :attribute must be less than :size.',
        ]);

        if($validator->fails()){
            return redirect()->route('color.edit', [$id])->withErrors($validator)->withInput();
        }
        $color = Color::find($id);
        if($request->image == '')
        {
            $filename = $color->image;
        }else{
            $file = $request->file('image');
            $newfile = Storage::put('public/color', $file);
            // dd($newfile);
            $newfile = (explode("/",$newfile));
            $filename = $newfile[1].'/'.$newfile[2];
        }
//        $imageName = time().'.'.request()->image->getClientOriginalExtension();
//        request()->image->move(public_path('images'), $imageName);


        $color->name = $request->input('name');
        $color->code = $request->input('code');
        $color->image = $filename;
        if($color->save()){
            $product = $request->product;
            $product_color = ProductColors::find($request->product_color);
            $product_color->product_id = $product;
            $product_color->color_id = $color->id;
            $product_color->quantity = $request->quantity;
            $product_color->save();
            return redirect('colors')->with("msg","Details updated");
        }else{
            $error = [
                'error' => 'An error occurred while saving!',
            ];
            return redirect()->route('color.edit', [$id])->withErrors($error)->withInput();
        }
    }

    public function delete($id){
        $color = Color::find($id);
        $color->delete();
        return redirect()->route('color.list')->with("error","Color successfully deleted!");
    }
}

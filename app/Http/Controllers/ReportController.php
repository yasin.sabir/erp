<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use App\deliveryChallanReport;
use App\ItemType;
use App\Notifications\RegisterEmail;
use App\Order;
use App\OrderMeta;
use App\OrderProductColor;
use App\Product;
use App\ProductCategory;
use App\ProductColorPurcahses;
use App\ProductColors;
use App\ProductStocks;
use App\ProductTypes;
use App\User;
use App\UserMeta;
use Illuminate\Http\Request;
use App\deliveryChallan;
//use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\deliveryChallanDetails;

class ReportController extends Controller
{
    public function get_stock()
    {
        $products = Product::all();
//         dd($products);
        return view('reports.stock', ['products' => $products]);
    }

    public function get_stock_id(Request $request)
    {
//        dd($request);
        $product_id = $request->product;
        $product_color = ProductColors::where('product_id', $product_id)->get();
        $pruchases = 0;

        foreach ($product_color as $product) {
            $purcahses = 0;
            $product['orders'] = [];
            $orders = [];
            $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
            $color = Color::findorfail($product->color_id);
            $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();
            foreach ($stocks as $stock) {
                $purcahses = $purcahses + (int)$stock->quantity;
            }
            $product['purchase'] = $purcahses;
            $product->code = $color->code;
            foreach ($order_product_colors as $order_product_color) {
                $order = Order::find($order_product_color->order_id);
                $order->qty = $order_product_color->quantity;
                $user = User::findorfail($order->user_id);
                $order->user_name = $user->name;
                if ($order->status == 'partial_approved') {

                    $orders[] = $order;
                }
            }
            $product['orders'] = $orders;
        }


        return view('reports.stock_detail', ['product_color' => $product_color]);
    }

    public function get_userOrderReport($status)
    {
        $user_id = Auth::user()->id;
//        dd($user_id);
        $orders = Order::all()->where('user_id', $user_id)->where('status', $status);
        foreach ($orders as $order) {
            $challans = deliveryChallan::where('order_id', $order->id)->get();
            $order->challans = $challans;
        }
        return view('frontend.reports.orderReport', ['orders' => $orders]);
    }

    public function get_userOrderReportDetail($id)
    {
        $order = Order::find($id);
        $client_order = [
            "id" => $order->id,
            "status" => $order->status,
            "created_at" => $order->created_at->format('m-d-Y')
        ];
        $order_metas = OrderMeta::where('order_id', $order->id)->get();
        foreach ($order_metas as $order_meta) {
            $client_order[$order_meta->key] = $order_meta->value;
        }
//            dd($order->id);
        $order_product_colors = OrderProductColor::where('order_id', $order->id)->get();
        $client_order['products'] = [];
        foreach ($order_product_colors as $key => $order_product_color) {
            $product_color = ProductColors::find($order_product_color->product_color_id);
            $product_id = (int)$product_color->product_id;
            $product = Product::find($product_id);

            $client_order['products'][$key]['product_name'] = $product->name;
            $color = Color::find($product_color->color_id);
            $client_order['products'][$key]['product_color'] = $color->code;
            $client_order['products'][$key]['qty'] = $order_product_color->quantity;
        }
//        dd($client_order);
        return view('frontend.reports.orderReportDetail', ['order' => $client_order]);
    }

    public function get_userChallanDetail($id)
    {
        $challan = deliveryChallan::find($id);

        $data = [];
        $order = Order::find($challan->order_id);

        $data['challan_no'] = $challan->id;
        $data['date'] = (new Carbon($challan->created_at))->format('d/m/y');


        $client = User::find($order->user_id);
        $distributor_id = UserMeta::where('user_id', $client->id)->where('key', 'distributor_id')->first();
        if ($distributor_id->value != "") {
            $distributor = User::find($distributor_id->value);
            $distributor_name = $distributor->name;
        } else {
            $distributor_name = "";
        }
        $client_city = UserMeta::where('user_id', $client->id)->where('key', 'city')->first();
        $order_product_colors = OrderProductColor::where('order_id', $order->id)->get();

        $data['party_name'] = $client->name;
        $data['order_no'] = $order->order_no;
        $data['city'] = $client_city->value;
        $data['distributor'] = $distributor_name;
        $data['vehicle_no'] = $challan->vehicle_no;
        $data['driver_name'] = $challan->driver_name;
        $data['driver_cnic'] = $challan->driver_cnic;
        $data['driver_number'] = $challan->driver_number;
        $data['products'] = [];
        $total = 0;
        $total_delivered = 0;
        $delivery_challan_details = deliveryChallanDetails::where('challan_id', $challan->id)->get();
        foreach ($delivery_challan_details as $key => $delivery_challan_detail) {
//                    dd($order_product_color);
            $order_product_color = OrderProductColor::find($delivery_challan_detail->order_product_colors_id);
            $product_color = ProductColors::find($order_product_color->product_color_id);
            $product = Product::find($product_color->product_id);
            $color = Color::find($product_color->color_id);
            $data['products'][$key]['product_name'] = $product->name;
            $data['products'][$key]['product_code'] = $color->code;
            $data['products'][$key]['quantity'] = $order_product_color->quantity;
            $data['products'][$key]['quantity_delivered'] = $delivery_challan_detail->quantity;

            $total += $order_product_color->quantity;
            $total_delivered += $delivery_challan_detail->quantity;
        }

        $data['total'] = $total;
        $data['total_delivered'] = $total_delivered;

        return view('frontend.reports.deliveryChallan', ['data' => $data]);
    }

    public function get_saleStock()
    {
        $users = User::role('client')->get();
        $distributor = User::role('distributor')->get();
        foreach ($distributor as $value) {
            $users[] = $value;
        }
        $city = [];
        foreach ($users as $user) {
            $user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
            $city[] = $user_city->value;

        }
        $city = array_unique($city);
        return view('reports.saleStock', ['cities' => $city]);
    }

    public function get_sale_stock(Request $request)
    {
        $city = $request->city;

        $product_color = ProductColors::all();
        $pruchases = 0;

        foreach ($product_color as $product) {
            $purcahses = 0;
            $orders = [];
            $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
            $color = Color::findorfail($product->color_id);
            $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();
//            dd($product);
            $product_details = Product::find($product->product_id);
//            dd($product_details);
            foreach ($stocks as $stock) {
                $purcahses = $purcahses + (int)$stock->quantity;
            }
            $product['purchase'] = $purcahses;
            $product->code = $color->code;
            $product->name = $product_details->name;
            $product['pending'] = 0;
            $product['net_order'] = 0;
            $product['quantity_in_hand'] = $product->quantity_in_hand;
//            dd($order_product_colors);
            foreach ($order_product_colors as $order_product_color) {
                $order = Order::find($order_product_color->order_id);
                $order->qty = $order_product_color->quantity_delivered;
                $user = User::findorfail($order->user_id);
                $order->user_name = $user->name;
                $user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
//                dd($user_city);
                if ($user_city->value == $city) {
                    $product['net_order'] = $product['net_order'] + $order->qty;
//                    if ($order->status == "processing")
                    $product['pending'] = $product['pending'] + $order_product_color->quantity_pending;
//                }
                    $product['quantity_in_hand'] = $product['quantity_in_hand'];
                }

            }
        }
//        dd($product_color);
        return view('reports.sale_stock_detail', ['product_color' => $product_color]);

    }


    public function saleStock_Distributors_View()
    {
        $distributors = User::role('distributor')->get();
        return view('reports.sales_stock_distributors_report', ['distributors' => $distributors]);
    }

    public function get_saleStock_Distributors_records(Request $request)
    {
        $distributor = $request->distributor;

        $product_color = ProductColors::all();
        $pruchases = 0;

        foreach ($product_color as $product) {
            $purcahses = 0;
            $orders = [];
            $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
            $color = Color::findorfail($product->color_id);
            $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();
//            dd($product);
            $product_details = Product::find($product->product_id);
//            dd($product_details);
            foreach ($stocks as $stock) {
                $purcahses = $purcahses + (int)$stock->quantity;
            }
            $product['purchase'] = $purcahses;
            $product->code = $color->code;
            $product->name = $product_details->name;
            $product['pending'] = 0;
            $product['net_order'] = 0;
            $product['quantity_in_hand'] = $product->quantity_in_hand;
//            dd($order_product_colors);
            foreach ($order_product_colors as $order_product_color) {
                $order = Order::find($order_product_color->order_id);
                $order->qty = $order_product_color->quantity_delivered;
                $user = User::findorfail($order->user_id);
                $order->user_name = $user->name;
                $user_distributor = UserMeta::where('user_id', $user->id)->where('key', 'distributor_id')->first();
//                dd($user_city);
                if ($user_distributor->value == $distributor) {
                    $product['net_order'] = $product['net_order'] + $order->qty;
//                    if ($order->status == "processing")
                    $product['pending'] = $product['pending'] + $order_product_color->quantity_pending;
//                }
                    $product['quantity_in_hand'] = $product['quantity_in_hand'];
                }

            }
        }
//        dd($product_color);
        return view('reports.sale_stock_detail', ['product_color' => $product_color]);

    }


    public function saleStock_Cities_View()
    {
        $users = User::role('client')->get();
        $distributor = User::role('distributor')->get();
        foreach ($distributor as $value) {
            $users[] = $value;
        }
        $city = [];
        foreach ($users as $user) {
            $user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
            $city[] = $user_city->value;

        }
        $city = array_unique($city);
        return view('reports.sales_stock_cities_report', ['cities' => $city]);
    }

    public function get_saleStock_Cities_records(Request $request)
    {
        $city = $request->city;

        $product_color = ProductColors::all();
        $pruchases = 0;

        foreach ($product_color as $product) {
            $purcahses = 0;
            $orders = [];
            $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
            $color = Color::findorfail($product->color_id);
            $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();
//            dd($product);
            $product_details = Product::find($product->product_id);
//            dd($product_details);
            foreach ($stocks as $stock) {
                $purcahses = $purcahses + (int)$stock->quantity;
            }
            $product['purchase'] = $purcahses;
            $product->code = $color->code;
            $product->name = $product_details->name;
            $product['pending'] = 0;
            $product['net_order'] = 0;
            $product['quantity_in_hand'] = $product->quantity_in_hand;
//            dd($order_product_colors);
            foreach ($order_product_colors as $order_product_color) {
                $order = Order::find($order_product_color->order_id);
                $order->qty = $order_product_color->quantity_delivered;
                $user = User::findorfail($order->user_id);
                $order->user_name = $user->name;
                $user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
//                dd($user_city);
                if ($user_city->value == $city) {
                    $product['net_order'] = $product['net_order'] + $order->qty;
//                    if ($order->status == "processing")
                    $product['pending'] = $product['pending'] + $order_product_color->quantity_pending;
//                }
                    $product['quantity_in_hand'] = $product['quantity_in_hand'];
                }

            }
        }
//        dd($product_color);
        return view('reports.sale_stock_detail', ['product_color' => $product_color]);

    }


    public function saleStock_Colors_View()
    {
        $colors = Color::all();
        return view('reports.sales_stock_colors_report', ['colors' => $colors]);
    }

    public function get_saleStock_Colors_records(Request $request)
    {
        //$product_color_id = $request->product_color_id;

        $product_color_ids = $request->input('colors_prod_ids');
        //dd($product_color_ids);

        if (!empty($product_color_ids)) {

            $data = [];

            foreach ($product_color_ids as $product_color_id) {
//            $product_color = ProductColors::all();
                $product_color = ProductColors::where(['id' => $product_color_id])->get();

                $pruchases = 0;

                foreach ($product_color as $product) {


                    $purcahses = 0;
                    $orders = [];
                    $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
                    $color = Color::findorfail($product->color_id);
                    $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();
                    //            dd($product);
                    $product_details = Product::find($product->product_id);
                    //            dd($product_details);
                    foreach ($stocks as $stock) {
                        $purcahses = $purcahses + (int)$stock->quantity;
                    }
                    $product['purchase'] = $purcahses;
                    $product->code = $color->code;
                    $product->name = $product_details->name;
                    $product['pending'] = 0;
                    $product['net_order'] = 0;
                    $product['quantity_in_hand'] = $product->quantity_in_hand;
                    //            dd($order_product_colors);
                    foreach ($order_product_colors as $order_product_color) {

                        $order = Order::find($order_product_color->order_id);
                        $order_product_color_ids = OrderProductColor::where(['order_id' => $order->id])->get();
                        $order->qty = $order_product_color->quantity_delivered;

                        //  $user = User::findorfail($order->user_id);
                        //  $order->user_name = $user->name;
                        //$user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
                        //dd($user_city);

                        foreach ($order_product_color_ids as $id) {

                            foreach ($product_color_ids as $color_id) {

                                if ($id->product_color_id == $color_id) {

                                    $product['net_order'] = $product['net_order'] + $order->qty;
                                    // if ($order->status == "processing")
                                    $product['pending'] = $product['pending'] + $order_product_color->quantity_pending;
                                    //}
                                    $product['quantity_in_hand'] = $product['quantity_in_hand'];

                                }
                            }

                        }

                    }
                    $data [] = $product_color;
                }
            }

            // dd($data);

            //dd($product_color);
            return view('reports.sale_stock_color_detail', ['product_color' => $data]);

        } else {
            return Redirect::back()->with("error", "Please select colors from the given list!!");
        }


    }


    public function saleStock_Dates_View()
    {
        $orders = Order::all('created_at');

        $array_date = [];

        foreach ($orders as $order) {
            //  $array_date['original_format'] = $order->created_at;
            $orders_dates = (new Carbon($order->created_at))->format('Y-m-d');
            $array_date[] = $orders_dates;
        }

        return view('reports.sales_stock_dates_report', ['order_dates' => array_unique($array_date)]);
    }

    public function get_saleStock_Dates_records(Request $request)
    {
        $date = $request->date;

        $starting = (new Carbon($request->starting_date))->format('Y-m-d');
        $ending = (new Carbon($request->ending_date))->format('Y-m-d');
        $starting = $starting . " 00:00:00";
        $ending = $ending . " 23:59:59";

        $current_date = (new Carbon($request->starting_date))->format('Y-m-d');

        $product_color = ProductColors::all();
        $pruchases = 0;

        foreach ($product_color as $product) {
            $purcahses = 0;
            $orders = [];
            $order_product_colors = OrderProductColor::where('product_color_id', $product->id)->get();
            $color = Color::findorfail($product->color_id);

            // Check stock according between selected dates
            $stocks = ProductColorPurcahses::where('product_color_id', $product->id)->where('created_at', '>=', $starting)->where('created_at', '<=', $ending)->get();
            //$stocks = ProductColorPurcahses::where('product_color_id', $product->id)->get();

            $product_details = Product::find($product->product_id);


            foreach ($stocks as $stock) {
                $purcahses = $purcahses + (int)$stock->quantity;


            }

            $product['purchase'] = $purcahses;
            $product->code = $color->code;
            $product->name = $product_details->name;
            $product['pending'] = 0;
            $product['net_order'] = 0;
            // $product['quantity_in_hand'] = $product->quantity_in_hand;
            $product['quantity_in_hand'] = $purcahses + $product->quantity;

            //  dd($order_product_colors);
            foreach ($order_product_colors as $order_product_color) {

                $orderID = Order::find($order_product_color->order_id);

                $order = Order::where('id', $orderID->id)->where('created_at', '>=', $starting)->where('created_at', '<=', $ending)->get();
                // $orderDates = (new Carbon($order->created_at))->format('Y-m-d');

                foreach ($order as $order) {
                    $order->qty = $order_product_color->quantity_delivered;
                    $user = User::findorfail($order->user_id);
                    $order->user_name = $user->name;
                    $user_city = UserMeta::where('user_id', $user->id)->where('key', 'city')->first();
                    //  dd($user_city);

                    if ($order) {
                        $product['net_order'] = $product['net_order'] + $order->qty;
                        //  if ($order->status == "processing")
                        $product['pending'] = $product['pending'] + $order_product_color->quantity_pending;
                        //                }
                        //$product['quantity_in_hand'] = $product['quantity_in_hand'];
                        //$product['quantity_in_hand'] = ($product->quantity + (int)$stock->quantity) - $product['net_order'];
                    }
                }
            }
        }


        //dd($product_color);
        return view('reports.sale_stock_detail', ['product_color' => $product_color]);

    }


    public function get_productLedger()
    {
        $products = Product::all();

        return view('reports.productLedger', ['products' => $products]);
    }

    public function getColorCode($id)
    {
//        dd($id);
        $product_colors = ProductColors::where('product_id', $id)->get();
//        dd($product_colors);
        foreach ($product_colors as $product_color) {
            $color = Color::find($product_color->color_id);
            $product_color['color_code'] = $color->code;
        }
//        $product_colors;
        $product_color = json_encode($product_color);
        return response()->json($product_colors);
    }

    public function get_productLedgerReport(Request $request)
    {
//        dd($request);
        $ReportData = [];
        $data = [];
        $product = Product::find($request->product);
        $data[$product->name] = [];
        foreach ($request->color as $color) {
            $order_product_colors = OrderProductColor::where('product_color_id', $color)->get();
            $purchases = ProductColorPurcahses::where('product_color_id', $color)->get();
            $product_color = ProductColors::find($color);
            $color = Color::find($product_color->color_id);
            $data[$product->name][$color->code] = [];
            $data[$product->name][$color->code]['order'] = 0;
            $data[$product->name][$color->code]['issued'] = 0;
            $data[$product->name][$color->code]['pending_order'] = 0;
            $data[$product->name][$color->code]['purchase'] = 0;
            $data[$product->name][$color->code]['initial'] = (int)$product_color->quantity;
            $data[$product->name][$color->code]['stock'] = (int)$product_color->quantity;
            $data[$product->name][$color->code]['balanace'] = $product_color->quantity_in_hand;

            foreach ($order_product_colors as $order_product_color) {
                $data[$product->name][$color->code]['order'] += $order_product_color->quantity;
                $data[$product->name][$color->code]['issued'] += $order_product_color->quantity_delivered;
                $data[$product->name][$color->code]['pending_order'] += $order_product_color->quantity_pending;
            }
            foreach ($purchases as $purchase) {
                $data[$product->name][$color->code]['purchase'] += $purchase->quantity;
                $data[$product->name][$color->code]['stock'] += (int)$purchase->quantity;
            }
//            dd($data);
//            $data;
        }

//        dd($purchases);
//        dd($data);
        $ReportData['products'] = [];
        $ReportData['products'] = $data;
//        dd($ReportData);

        return view('reports.product_ledger_detail', ['data' => $ReportData]);
    }

    public function get_deliveryChallen()
    {
        $orders = Order::all();
        $data = array();
        foreach ($orders as $order) {
            if ($order->status == 'approved' || $order->status == 'balance' || $order->status == 'completed') {
                array_push($data, $order);
            }
        }
        return view('reports.deliveryChallen', ['orders' => $data]);
    }


    //-------------
    public function createView_deliveryChallan($id)
    {
        $order_details = Order::find($id);
        $users = User::role('warehouse_manager')->get();
        $data = array();
        //        foreach ($order_details as $order) {
        //            if ($order->status == 'approved' || $order->status == 'balance') {
        //                array_push($data, $order);
        //            }
        //        }
        return view('reports.CreateDevliveryChallan', ['order' => $order_details, 'users' => $users]);

    }

    public function create_deliveryChallan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //            regex:/^([a-zA-Z0-9 ]*)$/
            //            regex:/^([\+0-9\s\-\(\)]*)$/
            'order' => 'required',
            'vehicle_no' => 'required|max:8',
            'driver_name' => 'required|max:255',
            'driver_nic' => 'required|regex:/^([0-9]){5}-([0-9]){7}-([0-9]){1}$/',
            'created_by' => 'required|max:255',
            'driver_number' => 'required|max:15',

        ], [
            'driver_nic.regex' => 'Enter CNIC no in this format XXXXX-XXXXXXX-X',
//            'driver_name.regex' => 'Driver name only accept alphabets.'

        ]);
        if ($validator->fails()) {
            // return redirect('clients.add_client')->withErrors($validator)->withInput();
            return Redirect::back()->withErrors($validator)->withInput();
        }


        $challan = new deliveryChallan();
        $challan->order_id = $request->order;
        $challan->vehicle_no = $request->vehicle_no;
        $challan->driver_name = $request->driver_name;
        $challan->driver_cnic = $request->driver_nic;
        $challan->driver_number = $request->driver_number;

        $created_by = User::where(['id' => $request->created_by])->first();
        $challan->created_by = $created_by->name;

        $order = Order::find($challan->order_id);
        $orderProductColors = OrderProductColor::where('order_id', $challan->order_id)->first();

        $qty_pending = $orderProductColors->quantity_pending;
        $qty_delivered = $orderProductColors->quantity_delivered;
        $qty_completed = $orderProductColors->quantity_completed;

        //for delivery report


        // if ($qty_pending != 0) {

        DB::transaction(function () use ($challan, $qty_delivered, $orderProductColors) {

            if ($challan->save()) {
                $data = [];

                $order = Order::find($challan->order_id);
                $is_completed = true;
                // $order_product_colors_challan = OrderProductColor::where('order_id',$order->id)->where('quantity_completed',false)->get();
                $order_product_colors = OrderProductColor::where('order_id', $order->id)->get();

                foreach ($order_product_colors as $order_product_color) {

                    $delivered_qty = 0;


                    if ($order_product_color->quantity_delivered > 0) {

                        $challans = deliveryChallan::where(['order_id' => $order->id])->get();
                        foreach ($challans as $challan) {
                            $delivered_challans = deliveryChallanDetails::where('challan_id', $challan->id)->get();
                            foreach ($delivered_challans as $delivered_challan) {
                                $delivered_qty += $delivered_challan->quantity;
                            }
                        }

                        $remaining_quantity_in_challan_details = ($qty_delivered - $delivered_qty);
                        $order_product_color->quantity_completed = $order_product_color->quantity_delivered;

                        $delivery_challan_detail = new deliveryChallanDetails();
                        $delivery_challan_detail->challan_id = $challan->id;
                        $delivery_challan_detail->order_product_colors_id = $order_product_color->id;
                        $delivery_challan_detail->quantity = $remaining_quantity_in_challan_details;


                        //Delivery Challan Report
                        $order_no = Order::where(['id' => $order->id])->first();
                        $client = User::where(['id' => $order->user_id])->first();
                        $distributor_id = UserMeta::where('user_id', $client->id)->where('key', 'distributor_id')->first();
                        $client_address = UserMeta::where('user_id', $client->id)->where('key', 'address')->first();
                        if ($distributor_id->value != "") {
                            $distributor = User::find($distributor_id->value);
                            $distributor_name = $distributor->name;
                        } else {
                            $distributor_name = "";
                        }

                        $delivery_report = deliveryChallanReport::create([
                            'challan_id' => $challan->id,
                            'order_id' => $order_no->id,
                            'order_no' => $order_no->order_no,
                            'date' => (new Carbon($challan->created_at))->format('Y-m-d'),
                            'client_name' => $client->name,
                            'distributor_name' => $distributor_name,
                            'address' => $client_address->value,
                            'order_qty' => $orderProductColors->quantity,
                            'delivered_qty' => $orderProductColors->quantity_delivered,
                            'balance_qty' => $orderProductColors->quantity - $orderProductColors->quantity_delivered
                        ]);


                        $product_color = ProductColors::find($order_product_color->product_color_id);

                        if ($order->status == 'approved' || $order->status == 'balance') {
                            $product_color->quantity_in_hand = $product_color->quantity_in_hand - $delivery_challan_detail->quantity;
                        }

                        $product_color->save();
                        $delivery_challan_detail->save();
                        // $order_product_color->quantity_completed = $order_product_color->quantity_completed + $remaining_quantity_in_challan_details;
                        $order_product_color->save();
                    }
                }


                if ($order_product_color->quantity_completed == $order_product_color->quantity_approved_by_distributor) {
                    $order->status = "completed";

                } else {
                    $order->status = "balance";
                }
                $order->save();


            }

        });

        return redirect()->route('report.getChallanDetail', ['id' => $challan->id]);

        //   } else {
        //       return Redirect::back()->with('error', 'Report already generated!');
        //   }


    }

    public function get_Challans()
    {
        $challans = deliveryChallan::all();
        foreach ($challans as $challan) {
            $order = Order::find($challan->order_id);
            $user = User::find($order->user_id);
            $challan->user_name = $user->name;

        }

        return view('reports.challan', ['challans' => $challans]);
    }

    public function get_Challan_detail($id)
    {
        $challan = deliveryChallan::find($id);

        $data = [];
        $order = Order::find($challan->order_id);

        $data['challan_no'] = $challan->id;
        $data['date'] = (new Carbon($challan->created_at))->format('d/m/y');


        $client = User::find($order->user_id);
        $distributor_id = UserMeta::where('user_id', $client->id)->where('key', 'distributor_id')->first();
        if ($distributor_id->value != "") {
            $distributor = User::find($distributor_id->value);
            $distributor_name = $distributor->name;
        } else {
            $distributor_name = "";
        }
        $client_city = UserMeta::where('user_id', $client->id)->where('key', 'city')->first();
        $order_product_colors = OrderProductColor::where('order_id', $order->id)->get();

        $data['party_name'] = $client->name;
        $data['order_no'] = $order->order_no;
        $data['city'] = $client_city->value;
        $data['distributor'] = $distributor_name;
        $data['vehicle_no'] = $challan->vehicle_no;
        $data['driver_name'] = $challan->driver_name;
        $data['driver_cnic'] = $challan->driver_cnic;
        $data['driver_number'] = $challan->driver_number;
        $data['products'] = [];
        $total = 0;
        $total_delivered = 0;
        $delivery_challan_details = deliveryChallanDetails::where('challan_id', $challan->id)->get();
        foreach ($delivery_challan_details as $key => $delivery_challan_detail) {
//                    dd($order_product_color);
            $order_product_color = OrderProductColor::find($delivery_challan_detail->order_product_colors_id);
            $product_color = ProductColors::find($order_product_color->product_color_id);
            $product = Product::find($product_color->product_id);
            $color = Color::find($product_color->color_id);
            $data['products'][$key]['product_name'] = $product->name;
            $data['products'][$key]['product_code'] = $color->code;
            $data['products'][$key]['quantity'] = $order_product_color->quantity;
            $data['products'][$key]['quantity_delivered'] = $delivery_challan_detail->quantity;

            $total += $order_product_color->quantity;
            $total_delivered += $delivery_challan_detail->quantity;
        }

        $data['total'] = $total;
        $data['total_delivered'] = $total_delivered;

        return view('reports.delivery_challan_detail', ['data' => $data]);
    }

    public function get_delivery_Challan_report()
    {
        $client = User::role('client')->get();
        $distributor = User::role('distributor')->get();
        foreach ($distributor as $value) {
            $client[] = $value;
        }
        //dd($client);
        return view('reports.delivery_challan_report', ['clients' => $client]);
    }

    public function getClientOrder(Request $request)
    {
        $users = $request->ids;
        $item = [];
        foreach ($users as $user) {
            $orders = Order::where("user_id", $user)->get();
            foreach ($orders as $order) {
                $challans = deliveryChallan::where('order_id', $order->id)->get();
                foreach ($challans as $challan) {
                    $order = Order::find($challan->order_id);
                    $order_Product_colors = OrderProductColor::where('order_id', $challan->order_id)->get();
                    foreach ($order_Product_colors as $order_Product_color) {
                        $product_color = ProductColors::find($order_Product_color->product_color_id);
                        $color = Color::find($product_color->color_id);
                        $item[$color->id] = $color->code;
                    }
                }
            }
        }
        return response()->json($item);
    }

    public function get_Challan_report_detail(Request $request)
    {
        //        dd($request);
        $clients = $request->client;
        //        dd($clients);
        $items = $request->item;
        //        dd($items);
        $starting = (new Carbon($request->starting_date))->format('Y-m-d');
        $ending = (new Carbon($request->ending_date))->format('Y-m-d');
        $starting = $starting . " 00:00:00";
        $ending = $ending . " 23:59:59";
        $data = [];

        $data['starting'] = $request->starting_date;
        $data['ending'] = $request->ending_date;
        $data['data'] = [];

        foreach ($clients as $client) {
            $orders = Order::where('user_id', $client)->get();

            foreach ($orders as $order) {
                $order_product_colors = OrderProductColor::where('order_id', $order->id)->get();


                $challans = deliveryChallanReport::where(['order_id' => $order->id])->where('created_at', '>=', $starting)->where('created_at', '<=', $ending)->get();


                foreach ($order_product_colors as $order_product_color) {
                    $product_color = ProductColors::find($order_product_color->product_color_id);
                    if (in_array($product_color->color_id, $items)) {
                        //  var_dump($product_color->color_id);
                        // $challans = deliveryChallan::where('order_id', $order->id)->where('created_at', '>=', $starting)->where('created_at', '<=', $ending)->get();
                        // dd($challans);
                        if (isset($challans)) {
                            foreach ($challans as $challan) {

                                $client = User::find($order->user_id);

                                $user_details = UserMeta::where('user_id', $client->id)->where('key', 'address')->first();
                                $distributor_id = UserMeta::where('user_id', $client->id)->where('key', 'distributor_id')->first();

                                // custom_printR($distributor_id->value);

                                if ($distributor_id->value != "") {
                                    $distributor = User::find($distributor_id->value);
                                    $challan['agent'] = $distributor->name;
                                }
                                $challan['address'] = $user_details->value;


                                $item_code = Color::find($product_color->color_id);
                                $challan['order_no'] = $order->order_no;
                                $challan['order_id'] = $order->id;
                                $challan['company_name'] = $client->name;
                                $challan['driver_name'] = $challan->driver_name;
                                $challan['driver_cnic'] = $challan->driver_cnic;
                                $challan['vehicle_no'] = $challan->vehicle_no;
                                $challan['party_name'] = $client->name;
                                $challan['item'] = $item_code->code;
                                $challan['order_quantity'] = $challan->order_qty;
                                $challan['delivered_quantity'] = $challan->delivered_qty;
                                $challan['balance'] = $challan->balance_qty;
                                $challan['created_at'] = (new Carbon($challan->created_at))->format('Y-m-d');
                                $data['data'][] = $challan;
                                // break;
                            }

                        }
                    }
                }


            }
        }
        return view('reports.delivery_challan_report_detail', ['data' => $data, 'date' => $data]);
    }

    public function orderReport()
    {

        $users = User::role('client')->get();
        $distributor = User::role('distributor')->get();
        foreach ($distributor as $value) {
            $users[] = $value;
        }
        $clients = array();

        $orders = Order::all();
//        dd($orders);

        $groupby_user_id = Order::all()->groupBy('user_id')->toArray();

        $order_wise_section = [];
        $totalOrder_per_customer = [];

        $qty = 0;
        $total_balance_qty = 0;
        $total_declined_qty = 0;
        $rr = array();
        $client['total_order_qty_new'] = 0;


        foreach ($groupby_user_id as $key => $order_wise) {
            $order_wise_section [$key] = count($order_wise);
            $totalOrder_per_customer['totalOrder'] = count($order_wise);
        }

        //custom_printR($order_wise_section[8]);

        foreach ($order_wise_section as $user_id_key => $orderWise) {
            $orderss = Order::where('user_id', $user_id_key)->get();

            foreach ($orderss as $key => $ord) {

                //echo $key."=>".$ord->id." , ".$ord->order_no."<br>";
                $user = User::find($user_id_key);

                $client = $user;

                $distributor_id = UserMeta::where('user_id', $user_id_key)->where('key', 'distributor_id')->first();
                if ($distributor_id->value != "") {
                    $distributor = User::find($distributor_id->value);
                    $client['agent'] = $distributor->name;
                }

                $client['total_order'] = 0;
                $client['total_ords_qty'] = 0;
                $client['total_delivered'] = 0;
                $client['total_pending'] = 0;
                $client['user_id'] = $user_id_key;

                $client['order_no'] = $ord->order_no;
                $client['order_id'] = $ord->id;
                $client['order_date'] = (new Carbon($ord->created_at))->format('d-m-Y');
                $client['codes'] = [];
                $item = [];

                $order_product_colors = OrderProductColor::where('order_id', $ord->id)->get();

                foreach ($order_product_colors as $order_product_color) {

                    //custom_printR($order_product_color);

                    $color_code = ProductColors::find($order_product_color->product_color_id);
                    $color = Color::find($color_code->color_id);
                    $item[$color_code->id]["color_code"] = $color->code;
                    $item[$color_code->id]['deliver_quantity'] = (int)$order_product_color->quantity_delivered;
                    $item[$color_code->id]['order_quantity'] = (int)$order_product_color->quantity;
                    $item[$color_code->id]['pending_quantity'] = (int)$order_product_color->quantiy_pending;

                    $item[$color_code->id]['balance_quantity'] = $item[$color_code->id]['order_quantity'] - $item[$color_code->id]['deliver_quantity'];
                    $item[$color_code->id]['approved_quantity'] = (int)$order_product_color->quantity_approved_by_distributor;
                    $item[$color_code->id]['declined_quantity'] = $item[$color_code->id]['order_quantity'] - $item[$color_code->id]['approved_quantity'];


                    $totals = DB::table('orders')
                        ->select('orders.user_id',
                            DB::raw('sum(order_product_colors.quantity) as total_order_qty_sum'),
                            DB::raw('sum(order_product_colors.quantity_approved_by_distributor) as total_approved_qty_sum'),
                            DB::raw('sum(order_product_colors.quantity_pending) as total_pending_qty_sum'),
                            DB::raw('sum(order_product_colors.quantity_delivered) as total_delivered_qty_sum'),
                            DB::raw('sum(order_product_colors.quantity_completed) as total_completed_qty_sum')
                        )
                        ->join('order_product_colors', 'orders.id', '=', 'order_product_colors.order_id')
                         ->where(['orders.user_id' => $user_id_key])
                        ->groupBy('orders.user_id')
                        ->get();

                    //$client['total_order'] = $client['total_order'] + $order_product_color->quantity;
                    //$client['total_delivered'] = $client['total_delivered'] + $order_product_color->quantity_delivered;
                    //$client['total_pending'] = $client['total_pending'] + $order_product_color->quantiy_pending;
                    //$client['total_balance'] = $client['total_balance'] + $item[$color_code->id]['balance_quantity'];
                    //$client['total_approved'] = $client['total_approved'] + $item[$color_code->id]['approved_quantity'];
                    //$client['total_declined'] = $item[$color_code->id]['declined_quantity'];
                }
                $client['codes'] = $item;
                $clients[] = $client;

                $total_balance_qty += $item[$color_code->id]['balance_quantity'];
                $total_declined_qty += $item[$color_code->id]['declined_quantity'];
            }
            //$client['total_order'] = $client['total_order'] +  $client['total_ords_qty'];


            foreach ($totals as $total) {
                $client['total_order'] = $total->total_order_qty_sum;
                $client['total_approved'] = $total->total_approved_qty_sum;
                $client['total_pending'] = $total->total_pending_qty_sum;
                $client['total_delivered'] = $total->total_delivered_qty_sum;
                $client['total_balance'] = $client['total_order'] - $client['total_delivered'];
                $client['total_declined'] = $client['total_order'] - $client['total_approved'];
            }
            //echo "<br>";
        }
       // custom_printR($totals[0]->total_order_qty_sum);
        return view('reports.order_report', ['data' => $clients , 'totals'=>$totals]);

    }
}

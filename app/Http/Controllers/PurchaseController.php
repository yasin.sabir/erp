<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Purchases;
use App\ProductColorPurcahses;
use App\ProductColors;
use App\Color;
use Illuminate\Support\Facades\Validator;
use App\ProductStocks;

class PurchaseController extends Controller
{
    public function index()
    {
        $purchases = Purchases::all();

        return view('purchases.index',['purchases'=>$purchases]);
    }
    public function add()
    {
        $product_colors = ProductColors::all();
        foreach ($product_colors as $item) {
            $color = Color::find($item->color_id);
            $item->color_code = $color->code;
        }

        return view('purchases.add', ['product_colors' => $product_colors]);
    }
    public function create(Request $request)
    {

        //dd($request->quantity);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:purchases,batch_name',
            'date' => 'required|max:255',
            'color' => 'required|max:255',
            'quantity' => 'required|array'
            // 'image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ],[
            'required' => 'The :attribute field is required.',
            'size'    => 'The :attribute must be exactly :size.',

        ]);

        if($validator->fails()){
            return redirect('purchases/add')->withErrors($validator)->withInput();
        }

        //return custom_printR($request->all());
//        if($validator->fails()){
//            return redirect()->route('purchase.add')->withErrors($validator)->withInput();
//        }


        $purchase = new Purchases();
        $purchase->batch_name = $request->name;
        $purchase->date = $request->date;
        if($purchase->save()){
            foreach ($request->color as $key =>$value){
                $quantity =$request->quantity[$key];
                $productColorPurchase = new ProductColorPurcahses();
                $productColorPurchase->purchases_id = $purchase->id;
                $productColorPurchase->product_color_id = $value;
                $productColorPurchase->quantity = $quantity;
                $productColorPurchase->save();
                $product_color = ProductColors::find($value);
                $product_color->quantity_in_hand += $quantity;
                $product_color->save();
            }
        }

        return redirect(route('purchase.all'))->with("msg","Purchase added");
    }
    public function view($id){

        $purchase = Purchases::with('ProductColorPurchases')->where('id',$id)->first();
//         dd($purchase);
        foreach ($purchase->ProductColorPurchases as $productColorPurchase) {
                $product_color = ProductColors::find($productColorPurchase->product_color_id);
                $color = Color::find($product_color->color_id);
                $product = Product::find($product_color->product_id);
                $productColorPurchase->color_code = $color->code;
                $productColorPurchase->product_name = $product->name;
        }

        return view('purchases.view',['purchase'=>$purchase]);
    }
    public function delete($id){

        $purchase = Purchases::with('ProductColorPurchases')->where('id',$id)->first();
//         dd($purchase);
        foreach ($purchase->ProductColorPurchases as $productColorPurchase) {
            $product_color = ProductColors::find($productColorPurchase->product_color_id);
            $product_color->quantity_in_hand = $product_color->quantity_in_hand - $productColorPurchase->quantity;
            $product_color->save();
        }
        $purchase->delete();
//
        return redirect(route('purchase.all'))->with("error","Purchase successfully deleted!");
    }

}

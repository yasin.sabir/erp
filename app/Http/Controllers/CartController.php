<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductColors;
use App\Color;
use Session;

class CartController extends Controller
{
    public function cart()
    {
        return view('frontend.cart.cart');
    }
    //
    public function addToCart(Request $request)
    {
//        dd($request);
        $cart = Session::get('cart');
        $product = Product::find($request->product_id);

        $product_color = ProductColors::find($request->color);
        $color = Color::find($product_color->color_id);
        $quantity = 0;
        if(isset($cart[$request->color]['qty']))
        {
            $quantity = $cart[$request->color]['qty'];
        }
        $cart[$request->color] = array(
            "id" => $request->product_id,
            "product_name" => $product->name,
            "product_color_id" => $request->color,
            "color_name" => $color->name,
            "color_code" => $color->code,
            "image" => $color->image,
            "qty" => $quantity + $request->qty,
        );

        Session::put('cart', $cart);
        Session::flash('success','Product Added To Cart!');
//        dd(Session::get('cart'));
        return redirect()->back();
    }

    public function removeFromCart($id)
    {
//        dd($request);
        $cart = Session::get('cart');
//        $cart =[];
        unset($cart[$id]);

//        dd($cart);

        Session::put('cart', $cart);
        Session::flash('success','Product Remove From Cart!');
//        dd(Session::get('cart'));
        return redirect()->back();
    }

    public function updateCart(Request $cartdata)
    {
//        dd($cartdata);
        $cart = Session::get('cart');

        foreach ($cartdata->product_color_id as $key => $val)
        {
            if ($cartdata->quantity[$key] > 0) {
                $cart[$val]['qty'] = $cartdata->quantity[$key];
            } else {
                unset($cart[$val]);
            }
        }
        Session::put('cart', $cart);
        Session::flash('success','Cart Updated!');
        return redirect()->back();
    }
}

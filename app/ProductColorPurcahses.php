<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColorPurcahses extends Model
{
    //
    public function Purchases(){
        return $this->belongsTo('App\Purchases');
    }
    public function productColor(){
        return $this->belongsTo('App\ProductColors');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductColor extends Model
{
    //
    public $timestamps = false;
}

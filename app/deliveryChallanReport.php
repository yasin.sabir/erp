<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deliveryChallanReport extends Model
{
    //
    protected $table = "delivery_challan_report";

    public $timestamps = true;
    protected $fillable = [
        'challan_id','order_id', 'order_no', 'date', 'client_name', 'distributor_name', 'address', 'order_qty', 'delivered_qty', 'balance_qty'
    ];

}
